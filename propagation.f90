module matvec_mod
     use conf
     implicit none
     type(Upointer) :: W_mat
     type(vmat_container) :: V_mat
     real(kind=db) :: UW_Uprefac
contains
     subroutine associate_W(W)
          use conf
          implicit none
          type(Upointer) :: W
          W_mat = W
     end subroutine associate_W
     subroutine associate_vmats(v)
          use conf
          implicit none
          type(vmat_container) :: v
          V_mat = v
     end subroutine associate_vmats

     subroutine matvec_Wmat(a)
          use conf
          implicit none
          complex(kind=db), intent(inout) :: a(NR,num_jJ,0:LMAX)
          call apply_cos2mat_fast(W_mat%p,W_mat%Unexti,a,W_mat%Ublock_offset)
     end subroutine matvec_Wmat
     subroutine matvec_Wmat_block(a,l)
          use conf
          implicit none
          complex(kind=db), intent(inout) :: a(NR,num_jJ)
          integer, intent(in) :: l
          call apply_cos2mat_fast_block(W_mat%p,W_mat%Unexti,a,W_mat%Ublock_offset,l)
     end subroutine matvec_Wmat_block

     subroutine matvec_Vmat(a)
          use conf
          implicit none
          complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
          call apply_vmat(V_mat%v, V_mat%diag, V_mat%rotmat, V_mat%nexti, a, V_mat%block_offset)
     end subroutine matvec_Vmat
     subroutine matvec_Vmat_block(a,jt)
          use conf
          implicit none
          complex(kind=db), intent(inout) :: a(NR,num_N)
          integer, intent(in) :: jt
          call apply_vmat_block(V_mat%v, V_mat%diag, V_mat%rotmat, V_mat%nexti, a, V_mat%block_offset,jt)
     end subroutine matvec_Vmat_block
     subroutine matvec_UW(a)
          ! Perform the operation (t*W+V)*a
          ! with t = UW_Uprefac
          complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
          complex(kind=db) :: a2(NR,num_N,0:JTOTMAX)
          complex(kind=db) :: ap(NR,num_jJ,0:LMAX)

          call prepare_statevec_for_laser_matrices(ap,a)
          call apply_vmat(V_mat%v, V_mat%diag, V_mat%rotmat, V_mat%nexti, a, V_mat%block_offset)
          call apply_cos2mat_fast(W_mat%p,W_mat%Unexti,ap,W_mat%Ublock_offset)
          call unprepare_statevec_for_laser_matrices(a2,ap)
          a = a + UW_Uprefac * a2
     end subroutine matvec_UW
end module matvec_mod



subroutine timeblock(a,num_steps,expkin,expkinHalf,expvmat,expdiag,expind, expblock,blockmap)
     ! Trotter approximation to propagator, exploiting 
     ! the fact that two V/2 matrices can be replaced by one V matrix.
     use conf
     use matvec_mod
     implicit none
     
     integer, intent(in) :: num_steps
     complex(kind=db), intent(in) :: expkin(NR), expkinHalf(NR)
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=exp_k), intent(in) :: expvmat(num_expvmat)
     complex(kind=db),intent(in) :: expdiag(NR,num_N,0:JTOTMAX)
     integer(kind=idx_k), intent(in) :: expind(num_expvmat)
     integer(kind=ll), intent(in) :: expblock(2,0:JTOTMAX)
     logical, intent(in) :: blockmap(0:JTOTMAX)

     integer :: it
     complex(kind=db), parameter :: dtC = DT*cmplx(0,1_db,kind=db)
     
     complex(kind=db), external :: dot_subspace,dot
     real(kind=db), external :: norm_subspace,norm

     ! apply exp(T/2) part of propagator
     call apply_expT(expkinHalf,a)

     do it=1,num_steps
          ! Apply two exp(T/2) parts in one matrix multiplication
          if(it>1) then
               call apply_expT(expkin,a)
          endif

          call apply_expvmat(expvmat, expind, expdiag, expblock, blockmap, a)
          if (do_absorption) call apply_absorbing_boundary(a)
          !call exp_a_krylov_block(num_N,JTOTMAX,indOrder,-dtC,a,&
          !     matvec_Vmat_block,norm_subspace,dot_subspace,&
          !     krylov_subspace_size_V_after_pulse,vmat_num_krylov_steps_after_pulse)
          !call exp_a_krylov(num_N,JTOTMAX,-dtC,a,matvec_Vmat,norm,dot,&
          !     krylov_subspace_size_V_after_pulse,vmat_num_krylov_steps_after_pulse)
     enddo
     ! Apply last T/2 matrix in the chain
     call apply_expT(expkinHalf,a)


end subroutine timeblock


subroutine imag_timeblock_singleJ(jt,num_steps,a,expvmat,expkin,expkinHalf)
     ! Trotter approximation to propagator, exploiting 
     ! the fact that two V/2 matrices can be replaced by one V matrix.
     !
     ! Propagate only J = jt part of the wave function.
     ! Different J parts do not mix when there is no external field.
     use conf
     implicit none
     
     integer, intent(in) :: jt,num_steps
     real(kind=db), intent(in) :: expvmat(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)
     complex(kind=db), intent(in) :: expkin(NR), expkinHalf(NR)
     complex(kind=db), intent(inout) :: a(NR,num_N)
     integer :: ir,it,order

     order = indOrder(jt) ! size of Lj matrix
     ! apply exp(T/2) part of propagator
     call apply_expT_singleJ(expkinHalf,a(1:NR,1:order),order)

     do it=1,num_steps
          ! Apply two exp(T/2) parts in one matrix multiplication
          if(it>1) then
               call apply_expT_singleJ(expkin,a(1:NR,1:order),order)
          endif
     
          ! Apply exp(V)
          forall(ir=1:NR) a(ir,1:order) = matmul(expvmat(1:order,1:order,jt,ir),a(ir,1:order))
     enddo
     ! Apply last T/2 matrix in the chain
     call apply_expT_singleJ(expkinHalf,a(1:NR,1:order),order)

end subroutine imag_timeblock_singleJ


function pulse_strength(t,I_max)
     use conf
     implicit none

     real(kind=db) :: t, I_max, pulse_strength, peak_field_amplitude_squared
     real(kind=db), parameter :: FWHM_p = pulse_sigma*sqrt(2*pi)

     ! Note: A factor \epsilon_0 is missing from this.
     ! It is included in the polarizability volume instead
     peak_field_amplitude_squared = 2*I_max/speed_of_light
     ! Gaussian beam:
     pulse_strength = peak_field_amplitude_squared * exp(-t**2/(2*pulse_sigma**2));
     !return
     ! Rectangular beam with same fluence (modifying fwhm)
     !if (abs(t) > 0.5*FWHM_p) then
     !     pulse_strength = 0
     !else
     !     pulse_strength = peak_field_amplitude_squared
     !end if

end function pulse_strength

integer(kind=idx_k) pure function num_Unonzero(U)
     use conf
     implicit none
     type(Upointer), intent(in) :: U
     
     integer(kind=idx_k) :: Ui

     Ui = 1
     ! Linear search for the index of Unexti that gives 0,
     ! marking the end of the U matrix
     do while(U%Unexti(Ui) .ne. 0)
          Ui = Ui+1;
     enddo
     
     num_Unonzero = Ui-1;
     
     return
end function num_Unonzero

subroutine fieldfree_propagation(cos2,a,U,vmats,expkin,expkinHalf)
     use conf
     use matvec_mod
     implicit none
     real(kind=db), intent(out) :: cos2(num_fieldfree_samples)
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db) :: a2(NR,num_N,0:JTOTMAX)
     complex(kind=db) :: ap(NR,num_jJ,0:LMAX)
     type(Upointer), intent(in) :: U
     type(vmat_container) :: vmats
     complex(kind=db), intent(in) :: expkin(NR), expkinHalf(NR)
     integer(kind=idx_k) :: num_Unonzero, probability_current_length
     real(kind=db), allocatable :: probability_current_time_integral(:)
     complex(kind=db) :: dot
     real(kind=db) :: overlap, overlap_naive
     logical :: blockmap(0:JTOTMAX)
     !real(kind=db) :: norm
     real(kind=db) :: t, tsam1,tsam2
     integer :: it, sample

     if (DEBUG) print *,'entering field free'
     probability_current_length = num_Unonzero(U)
     print *, probability_current_length
     allocate(probability_current_time_integral(probability_current_length))
     probability_current_time_integral = 0

     call associate_vmats(vmats) ! prepare matrix multiplication routine
                                 ! for use in the Krylov subspace algorithm.

     call setblockmap(blockmap,a) ! Mask out jt subspaces where the total
                                  ! probability is neglicible


     t = 1.5*pulse_FWHM
     sample = 1
     do it =1,NT2,NTblock
          call prepare_statevec_for_laser_matrices(ap,a)
          if (He .eq. 1) then
               if (DEBUG) call cpu_time(tsam1)
               call accumulate_absorbed_probability(overlap,overlap_naive,U%p, &
                probability_current_time_integral, probability_current_length, &
                ap, probability_current_wall, U%Unexti,t)
               if (DEBUG) call cpu_time(tsam2)
               if (DEBUG) print *, 'accumulate time taken: ', tsam2-tsam1
          else
               call apply_cos2mat_fast(U%p,U%Unexti,ap,U%Ublock_offset)
               call unprepare_statevec_for_laser_matrices(a2,ap)
               overlap = real(dot(a,a2))
               overlap_naive = overlap
          endif
          cos2(sample) = real(overlap)
          !print *, t/picosecond,real(overlap)
          sample=sample+1
!          print *, t, norm(a)
!          print *, 'imag should be zero: ', tmp

          call dump_distributions(t/picosecond,a,overlap,overlap_naive)
          call timeblock(a,NTblock,expkin,expkinHalf, &
               vmats%expV,vmats%expdiag,vmats%expind, &
               vmats%expblock, blockmap)

          !call cpu_time(tsam2)
          !print *, 'time taken2: ', tsam2-tsam1
          !a = a/norm(a)
          t = t + NTblock*DT
     enddo


end subroutine fieldfree_propagation

subroutine apply_pulse(cos2,a,I_max,U,W,vmats,expkinHalf)
     use conf
     use matvec_mod
     implicit none
     
     real(kind=db), intent(out) :: cos2(num_pulse_samples)
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db) :: a2(NR,num_N,0:JTOTMAX)
     complex(kind=db) :: ap(NR,num_jJ,0:LMAX)
     real(kind=db), intent(in) :: I_max
     !real(kind=db), intent(in), dimension(-1:1,0:jMAX,0:JTOTMAX,0:JTOTMAX,0:LMAX) :: U, W
     type(Upointer), intent(in) :: U, W
     type(vmat_container), intent(in) :: vmats
     real :: tV, tW
     
     complex(kind=db), intent(in) :: expkinHalf(NR)
     real(kind=db)::norm_subspace
     
     complex(kind=db) :: dot, overlap
     real(kind=db) :: norm

     real(kind=db) :: t
     integer :: it,sample!, outr
     
     tW = 1
     tV = 0

     call associate_W(W) ! Prepare external matrix multiplication routines 
     call associate_vmats(vmats) ! for use in the Krylov subspace
                                 ! exponentiation algorithm.

     t = -1.5*pulse_FWHM
     sample = 1
     do it =1,NT_pulse,NTblock_pulse
!          print *, t
!         ! Sample cos^2(theta)
          call prepare_statevec_for_laser_matrices(ap,a)
          call apply_cos2mat_fast(U%p,U%Unexti,ap,U%Ublock_offset)
          call unprepare_statevec_for_laser_matrices(a2,ap)
          overlap = dot(a,a2)
          cos2(sample) = real(overlap) !pulse_strength(t,1.0_db) ! real(overlap)
          sample = sample + 1
!          print *, 'imag should be zero: ', overlap
          call dump_distributions(t/picosecond,a,real(overlap),real(overlap))
          !call dump_energy(t/picosecond,vmats,a) ! For Hamiltonian readout
          ! Then propagate wave function in time
          call timeblock_pulse(a,I_max,t,NTblock_pulse,expkinHalf,&
               vmats%expV_pulse,vmats%expdiag_pulse,vmats%expind_pulse,&
               vmats%expblock_pulse)
          !call timeblock_pulse_UW(a,I_max,t,NTblock_pulse,expkinHalf)
          !if (tV .lt. tW) then
          !     call timeblock_pulse_W(a,I_max,t,NTblock_pulse,expkinHalf,tV,tW)
          !else
          !     call timeblock_pulse_V(a,I_max,t,NTblock_pulse,expkinHalf,tV,tW)
          !endif
          ! call dump_statevector(t/picosecond,a)
          ! print *, t/picosecond, norm(a), norm_subspace(a(:,:,JTOTMAX),JTOTMAX)**2
          !t = t + NTblock_pulse*DT_pulse
     enddo
!     close(25)
end subroutine apply_pulse

subroutine dump_energy(t,vmats,a)
     use conf
     implicit none
     real(kind=db), intent(in) :: t
     type(vmat_container), intent(in) :: vmats
     complex(kind=db), intent(in) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db)  :: b(NR,num_N,0:JTOTMAX)
     complex(kind=db)  :: c(NR,num_N,0:JTOTMAX)

     complex(kind=db) :: dot

     real(kind=db) :: Hrot, Hrot2
     real(kind=db) :: H, H2

     call copy_statevector(b,a)
     call apply_Hrot(b)
     Hrot = real(dot(a,b))
     call apply_Hrot(b)
     Hrot2 = real(dot(a,b))

     call copy_statevector(b,a)
     call copy_statevector(c,a)
     call apply_T(b)
     call apply_vmat(vmats%v, vmats%diag, vmats%rotmat, vmats%nexti, c, vmats%block_offset)
     H = real(dot(a,b)) + real(dot(a,c))
     call apply_T(b)
     call apply_vmat(vmats%v, vmats%diag, vmats%rotmat, vmats%nexti, c, vmats%block_offset)
     H2 = real(dot(a,b)) + real(dot(a,c))

     open(unit=29,file='hamilton.dat',status='unknown',action='write',position='append')

     write(29,*) t, H, H2, Hrot, Hrot2
     close(29)

end subroutine dump_energy


subroutine dump_statevector(t,a)
     use conf
     use shm
     use iso_c_binding
     implicit none
     
     real(kind=db) :: t
     complex(kind=db) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db), contiguous, pointer :: ap(:,:,:)
     type(c_ptr) :: handle,ptr
     character(len=1024) :: filename

     write (filename,*) 'statevectors/',t,'.dat'

     call allocate_shared(trim(adjustl(filename)),16*NR*int(num_N,kind=ll)*(JTOTMAX+1),handle,ptr)
     call C_F_POINTER(ptr,ap,(/NR,num_N,JTOTMAX+1/))
     ap(1:NR,1:num_N,0:JTOTMAX) => ap
     
     ap = a

     call deallocate_shared(handle)

end subroutine dump_statevector

subroutine dump_distributions(t,a,cos2,cos2_naive)
     use conf
     implicit none
     complex(kind=db), dimension(NR,num_N,0:JTOTMAX) :: a
     integer :: j,l,jt,ir
     real(kind=db), intent(in) :: t,cos2,cos2_naive
     real(kind=db) :: rvec(NR)
     real(kind=db) :: norm_subspace,norm_squared_jsubspace
     real(kind=db) :: norm_squared_lsubspace, norm

     if (.not. emit_distributions) then
          return
     endif

     open(unit=24,file='norm.dat',status='unknown',action='write',position='append')
     open(unit=25,file='rpart.dat',status='unknown',action='write',position='append')
     open(unit=26,file='jpart.dat',status='unknown',action='write',position='append')
     open(unit=27,file='lpart.dat',status='unknown',action='write',position='append')
     open(unit=28,file='jtpart.dat',status='unknown',action='write',position='append')

     write(24,*) t, norm(a), cos2, cos2_naive

     call norm_squared_rsubspace(a,rvec)
     do ir=1,NR
          write(25,*) t,ir*DR,rvec(ir)
     enddo
     write(25,*) ''

     do j=0,jMAX,jSTEP
          write(26, *) t,j,norm_squared_jsubspace(a,j)
     enddo
     write(26,*) ''

     do l=0,LMAX,LSTEP
          write(27, *) t,l,norm_squared_lsubspace(a,l)
     enddo
     write(27,*) ''

     do jt=0,JTOTMAX,JTOTSTEP
          write(28, *) t,jt,norm_subspace(a(:,:,jt),jt)**2
     enddo
     write(28,*) ''

     close(28)
     close(27)
     close(26)
     close(25)
     close(24)

end subroutine dump_distributions




subroutine timeblock_pulse_V(a,I_max,t,num_steps,expkinHalf,tV,tW)
     use conf
     use potentials
     use matvec_mod
     implicit none

     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     real(kind=db), intent(in) :: I_max
     real(kind=db), intent(inout) :: t
     integer, intent(in) :: num_steps
     complex(kind=db), intent(in) :: expkinHalf(NR)
     real(kind=db) :: pulse_strength
     integer :: it
     complex(kind=db), parameter :: dtC = DT_pulse*cmplx(0,1_db,kind=db)
     complex(kind=db) :: fac,prefac
     real(kind=db) :: p1, p2
     real, intent(out) :: tW, tV
     real :: t0,t1,t2
     
     complex(kind=db), external :: dot_subspace, dot_laserblock
     real(kind=db), external :: norm_subspace, norm_laserblock
     complex(kind=db), external :: dot
     real(kind=db), external :: norm


     ! 4*pi converts from polarizability volume to polarizability
     ! (sans \epsilon_0)
     !pulse_strength(t,I_max) * 4*pi* eig*dt
     prefac = -4*pi*dtC/2
     p1 = pulse_strength(t+DT_pulse/2,I_max)

     if (DEBUG) call cpu_time(t0)
     if (DEBUG) t1 = t0

     if (DEBUG) print *, 'Entering timeblock_pulse ', t0

     fac = prefac*p1
     call apply_expWmat_fast_block(matvec_Wmat_block,fac,a)
     !call apply_expWmat_fast(matvec_Wmat_block,fac,a)
    
     
     if (DEBUG) call cpu_time(t2)
     if (DEBUG) print *, 'Applied W(t)/2 ', t2-t1
     if (DEBUG) t1=t2

     do it=1,num_steps
          if (it > 1) then ! Apply W(t)/2 * W(t)/2 = W(t) matrix
          p2 = p1 
          p1 = pulse_strength(t+DT_pulse/2,I_max)
          fac = prefac*(p1+p2)
          call apply_expWmat_fast_block(matvec_Wmat_block,fac,a)
          !call apply_expWmat_fast(matvec_Wmat_block,fac,a)
          endif
          ! Apply T/2 operator 
          call apply_expT(expkinHalf,a)

          call cpu_time(t2) ! Not debug, needed by caller
          if (DEBUG) print *, 'Applied T/2 ', t2-t1
          t1 = t2

          call apply_expvmat_krylov_block(matvec_Vmat_block,-dtC,a,vmat_num_krylov_steps_pulse)
          !call apply_expvmat_krylov(matvec_Vmat,-dtC,a,vmat_num_krylov_steps_pulse)
          call cpu_time(t2)
          if (DEBUG) print *, 'Applied V', t2-t1
          tV = t2-t1
          if (DEBUG) t1 = t2

          call apply_expT(expkinHalf,a)
          call cpu_time(t2)
          if (DEBUG) print *, 'Applied T/2 again', t2-t1
          t1 = t2
          t = t+DT_pulse
     enddo

     ! Apply final W(t)/2 matrix in the chain
     fac = prefac*p1
     call apply_expWmat_fast_block(matvec_Wmat_block,fac,a)
     !call apply_expWmat_fast(matvec_Wmat_block,fac,a)
     call cpu_time(t2)
     tW = t2-t1
     if (DEBUG) then 
          print *, 'Applied W(t)/2 again', tW
          print *, 'Elapsed time: ', t2-t0
     endif

end subroutine timeblock_pulse_V

! same as above with W as the inner matrix
subroutine timeblock_pulse_W(a,I_max,t,num_steps,expkinHalf,tV,tW)
     use conf
     use potentials
     use matvec_mod
     implicit none

     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     real(kind=db), intent(in) :: I_max
     real(kind=db), intent(inout) :: t
     integer, intent(in) :: num_steps
     complex(kind=db), intent(in) :: expkinHalf(NR)
     real(kind=db) :: pulse_strength
     integer :: it
     complex(kind=db), parameter :: dtC = DT_pulse*cmplx(0,1_db,kind=db)
     complex(kind=db) :: fac,prefac
     real(kind=db) :: p1
     real, intent(out) :: tW, tV
     real :: t0,t1,t2
     
     complex(kind=db), external :: dot_subspace, dot_laserblock
     real(kind=db), external :: norm_subspace, norm_laserblock
     complex(kind=db), external :: dot
     real(kind=db), external :: norm


     ! 4*pi converts from polarizability volume to polarizability
     ! (sans \epsilon_0)
     !pulse_strength(t,I_max) * 4*pi* eig*dt
     prefac = -4*pi*dtC

     call cpu_time(t0)
     t1 = t0

     print *, 'Entering timeblock_pulse_W ', t0
     call apply_expvmat_krylov_block(matvec_Vmat_block,-dtC/2,a,vmat_num_krylov_steps_pulse)
     !call apply_expvmat_krylov(matvec_Vmat,-dtC/2,a,vmat_num_krylov_steps_pulse)
     
     call cpu_time(t2)
     print *, 'Applied V/2 ', t2-t1
     t1=t2

     do it=1,num_steps
          if (it > 1) then ! Apply W(t)/2 * W(t)/2 = W(t) matrix
          call apply_expvmat_krylov_block(matvec_Vmat_block,-dtC,a,vmat_num_krylov_steps_pulse)
          !call apply_expvmat_krylov(matvec_Vmat,-dtC,a,vmat_num_krylov_steps_pulse)
     
          endif
          ! Apply T/2 operator 
          call apply_expT(expkinHalf,a)

          call cpu_time(t2)
          print *, 'Applied T/2 ', t2-t1
          t1 = t2

          p1 = pulse_strength(t+DT_pulse/2,I_max)
          fac = prefac*p1
          call apply_expWmat_fast_block(matvec_Wmat_block,fac,a)
          !call apply_expWmat_fast(matvec_Wmat_block,fac,a)
 
          call cpu_time(t2)
          print *, 'Applied W(t)', t2-t1
          tW = t2-t1
          t1 = t2

          call apply_expT(expkinHalf,a)
          call cpu_time(t2)
          print *, 'Applied T/2 again', t2-t1
          t1 = t2
          t = t+DT_pulse
     enddo

     ! Apply final V/2 matrix in the chain
     call apply_expvmat_krylov_block(matvec_Vmat_block,-dtC/2,a,vmat_num_krylov_steps_pulse)
     !call apply_expvmat_krylov(matvec_Vmat,-dtC/2,a,vmat_num_krylov_steps_pulse)
     
     call cpu_time(t2)
     print *, 'Applied V/2 again', t2-t1
     tV = t2-t1
     print *, 'Elapsed time: ', t2-t0

end subroutine timeblock_pulse_W


subroutine timeblock_pulse_UW(a,I_max,t,num_steps,expkinHalf)
     use conf
     use potentials
     use matvec_mod
     implicit none

     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     real(kind=db), intent(in) :: I_max
     real(kind=db), intent(inout) :: t
     integer, intent(in) :: num_steps
     complex(kind=db), intent(in) :: expkinHalf(NR)
     real(kind=db) :: pulse_strength
     integer :: it
     complex(kind=db), parameter :: dtC = DT_pulse*cmplx(0,1_db,kind=db)
     real :: t0,t1,t2
     complex(kind=db), external :: dot
     real(kind=db), external :: norm
     integer, parameter :: siz = krylov_subspace_size_UW
     integer, parameter :: krylov_steps = krylov_UW_num_steps

     call cpu_time(t0)

     print *, 'Entering timeblock_pulse_UW ', t0

     t1 = t0

     do it=1,num_steps
          ! Apply T/2 operator 
          call apply_expT(expkinHalf,a)

          call cpu_time(t2)
          print *, 'Applied T/2 ', t2-t1
          t1 = t2

          ! 4*pi converts from polarizability volume to polarizability
          ! (sans \epsilon_0)

          UW_Uprefac = 4*pi*pulse_strength(t+DT_pulse/2,I_max)
          
          call exp_a_krylov(num_N,JTOTMAX,-dtC,a,matvec_UW,norm,dot,siz,krylov_steps)

          call cpu_time(t2)
          print *, 'Applied V+W(t)', t2-t1
          t1 = t2

          call apply_expT(expkinHalf,a)
          call cpu_time(t2)
          print *, 'Applied T/2 again', t2-t1
          t1 = t2
          t = t+DT_pulse
     enddo

     call cpu_time(t2)
     print *, 'Elapsed time: ', t2-t0

end subroutine timeblock_pulse_UW

subroutine timeblock_pulse(a,I_max,t,num_steps,expkinHalf,expvmat,expdiag,expind,expblock)
     use conf
     use potentials
     use matvec_mod
     implicit none

     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     real(kind=db), intent(in) :: I_max
     real(kind=db), intent(inout) :: t
     integer, intent(in) :: num_steps
     complex(kind=db), intent(in) :: expkinHalf(NR)
     complex(kind=exp_k), intent(in) :: expvmat(num_expvmat)
     complex(kind=db),intent(in) :: expdiag(NR,num_N,0:JTOTMAX)
     integer(kind=idx_k), intent(in) :: expind(num_expvmat)
     integer(kind=ll), intent(in) :: expblock(2,0:JTOTMAX)

     real(kind=db) :: pulse_strength
     integer :: it
     logical :: blockmap(0:JTOTMAX)
     complex(kind=db), parameter :: dtC = DT_pulse*cmplx(0,1_db,kind=db)
     complex(kind=db) :: fac,prefac
     real(kind=db) :: p1
     real :: t0,t1,t2
     
     complex(kind=db), external :: dot_subspace, dot_laserblock
     real(kind=db), external :: norm_subspace, norm_laserblock
     complex(kind=db), external :: dot
     real(kind=db), external :: norm


     ! 4*pi converts from polarizability volume to polarizability
     ! (sans \epsilon_0)
     !pulse_strength(t,I_max) * 4*pi* eig*dt
     prefac = -4*pi*dtC

     if (DEBUG) then
          call cpu_time(t0)
          t1 = t0
          print *, 'Entering timeblock_pulse ', t0
     endif 

     do it=1,num_steps
          
          call setblockmap(blockmap,a) ! Which entries are non-neglicible
          call apply_expvmat(expvmat, expind, expdiag, expblock, blockmap, a)
          if (do_absorption) call apply_absorbing_boundary(a)

          if (DEBUG) then
               call cpu_time(t2)
               print *, 'Applied V/2 ', t2-t1
               t1=t2
          endif

          ! Apply T/2 operator 
          call apply_expT(expkinHalf,a)

          if (DEBUG) then
               call cpu_time(t2)
               print *, 'Applied T/2 ', t2-t1
               t1 = t2
          endif

          p1 = pulse_strength(t+DT_pulse/2,I_max)
          fac = prefac*p1
          call apply_expWmat_fast_block(matvec_Wmat_block,fac,a)
          !call apply_expWmat_fast(matvec_Wmat_block,fac,a)
 
          if (DEBUG) then
               call cpu_time(t2)
               print *, 'Applied W(t)', t2-t1
               t1 = t2
          endif

          call apply_expT(expkinHalf,a)
          if (DEBUG) then
               call cpu_time(t2)
               print *, 'Applied T/2 again', t2-t1
               t1 = t2
          endif

          ! Apply final V/2 matrix in the chain
          call setblockmap(blockmap,a)
          call apply_expvmat(expvmat, expind, expdiag, expblock, blockmap, a)
          if (do_absorption) call apply_absorbing_boundary(a)
          if (DEBUG) then
               call cpu_time(t2)
               print *, 'Applied V/2 again', t2-t1
          endif
          
          t = t+DT_pulse

     enddo

     if (DEBUG) print *, 'Elapsed time: ', t2-t0

end subroutine timeblock_pulse



