
! Using mergesort because it's not terrible and it's fast to implement.
recursive subroutine mergesort(full,l,idx,array)
     ! full: size of array.
     ! l: size of index array
     use conf
     implicit none
     integer :: full,l,idx(l),idx_tmp(l)
     real(kind=db) :: array(full)
     
     integer :: split

     if (l .le. 1) return ! A list with 1 or 0 elements is trivially sorted.

     split = l/2

     ! Sort the two halves of the array
     call mergesort(full,split,idx(1:split),array)
     call mergesort(full,l-split,idx(split+1:l),array)
     
     ! It's easy to merge two sorted arrays into a single sorted array..
     call mergesort_merge(full,l,split,idx_tmp,idx(1:split), idx(split+1:l), array)

     idx = idx_tmp

     return

end subroutine mergesort

subroutine mergesort_merge(full,l,split,idxout,idx1,idx2,array)
     use conf
     implicit none
     
     integer :: a,b,o, full, l, split
     integer :: idxout(l),idx1(split),idx2(l-split)
     real(kind=db) :: array(full)

     
     a = 1
     b = 1

     do o = 1, l
          if (a > split) then
               idxout(o:l) = idx2(b:l-split)
               exit
          endif
          if (b > l-split) then
               idxout(o:l) = idx1(a:split)
               exit
          endif

          if (array(idx1(a)) > array(idx2(b))) then
               idxout(o) = idx1(a)
               a = a + 1
          else 
               idxout(o) = idx2(b)
               b = b + 1
          end if
     enddo

end subroutine mergesort_merge


