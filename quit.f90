! Must be in separate file to work in all programs
subroutine quit(str)
     character(*) :: str
     integer :: ierr
     logical :: flag

     include 'mpif.h'

     write(*,*) str
     call MPI_INITIALIZED(flag,ierr)
     if (flag) then
          rc=0
          call MPI_ABORT(MPI_COMM_WORLD, rc, ierr)
          call MPI_FINALIZE (ierr)
     end if 
     stop
end subroutine quit
