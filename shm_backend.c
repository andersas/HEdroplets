#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/file.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>


typedef struct {
     int fd;
     size_t size;
     void *mmap_area;
} shm_handle_t;


void *shm_get_pointer(shm_handle_t *handle) {
     return handle->mmap_area;
}

shm_handle_t *shm_get_handle(size_t size) {

     shm_handle_t *handle;

     handle = (shm_handle_t *) malloc (sizeof(shm_handle_t));
     handle->mmap_area = NULL;
     handle->size = size;

     return handle;
}


static size_t filesize(int fd) {

     struct stat buf;
     if (fstat(fd,&buf)) {
          perror("fstat");
          return 0;
     }

     return buf.st_size;

}

void *shm_allocate(char *filename, shm_handle_t *handle) {

     int fd, prot, flags;
     size_t offset;

     fd = open(filename,O_CREAT|O_RDWR,00660);

     if (fd < 0) {
          perror("open");
          return NULL;
     }

     handle->fd = fd;

     if (filesize(fd) < handle->size) {
          if (ftruncate(fd,handle->size)) {
               perror("ftruncate");
               return NULL;
          }
     }

     prot = PROT_READ|PROT_WRITE;
     flags = MAP_SHARED;//|MAP_LOCKED; // MAP_LOCKED prevents swapping to disk, but is privileged
     offset = 0;

     handle->mmap_area = mmap(NULL,handle->size,prot,flags,fd,offset);

     if (handle->mmap_area == MAP_FAILED) {
          handle->mmap_area = NULL;
          perror("mmap");
          return NULL;
     }

     return handle->mmap_area;

}

void shm_deallocate(shm_handle_t *handle) {

     if (handle == NULL) return;

     if (handle->mmap_area != NULL) {
          munmap(handle->mmap_area,handle->size);
     }
     handle->mmap_area = NULL;
     handle->size = 0;
     
     close(handle->fd);

     free(handle);
}


int shm_lock_shared(shm_handle_t *handle) {
     if (handle == NULL) return -1;

     if (flock(handle->fd, LOCK_SH)) {
          perror("flock");
          return -1;
     }
     return 0;
}

int shm_lock_exclusive(shm_handle_t *handle) {
     if (handle == NULL) return -1;

     if (flock(handle->fd, LOCK_EX)) {
          perror("flock");
          return -1;
     }
     return 0;
}

int shm_unlock(shm_handle_t *handle) {
     if (handle == NULL) return -1;

     if (flock(handle->fd, LOCK_UN)) {
          perror("flock");
          return -1;
     }
     return 0;
}


