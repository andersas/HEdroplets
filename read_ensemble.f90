program init
     use conf
     call conf_init()
     call read_ensemble()
end program init

subroutine read_ensemble
     use conf
     implicit none

     real(kind=db), allocatable :: statistical_weight(:), E(:)
     integer, allocatable :: jtots(:)
     integer :: ensemble_size, i
     complex(kind=db), allocatable :: a_initial(:,:,:)


     open(77,file=ensemble_file,status='old',form='UNFORMATTED')
     read(77) ensemble_size
     allocate(E(ensemble_size), jtots(ensemble_size), statistical_weight(ensemble_size), a_initial(NR,num_N,ensemble_size))
     read(77) E, jtots, statistical_weight,a_initial
     close(77)

     print *, 'E [K],                           Jtot,  weight,       degeneracy'
     do i = 1, ensemble_size
          print *, E(i), jtots(i), statistical_weight(i), 2*jtots(i)+1
     enddo




end subroutine read_ensemble


