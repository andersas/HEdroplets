#!/bin/bash

# Set the environment variables for calling coupchan.
# Ensures a big enough stack size for openmp,
# and disables multiple threads in openblas.

# For this script to work, you must run it with "source".
# I.e:
# source env.sh
#
# Otherwise, these environment variables will only be set in
# the shell script process, and not the calling process (i.e. your shell)

ulimit -s unlimited
export OMP_STACKSIZE=400M
export OPENBLAS_NUM_THREADS=1

