subroutine focal_volume_average(weights, shell_intensities)
     use conf
     implicit none

     integer, parameter :: num_shells = focal_volume_averaging_shells
     real(kind=db), intent(out), dimension(num_shells) :: weights, shell_intensities
     real(kind=db), parameter :: rmax = 1.7*probe_waist
     real(kind=db) :: drr
     real(kind=db), dimension(num_shells) :: rr, volume, Pdiss
     integer :: n

     drr = rmax/num_shells
     do n=1,num_shells
          rr(n) = (2.0*n-1.0)*drr/2.0
     enddo
     
     volume = 2*rr*drr !Shell volume (apart from pi, but we normalize anyway)

     Pdiss = exp(-2.0*rr**2/(probe_waist**2)) ! detection probability
     ! = cross section * number density * intensity ** 1
     ! (other models might have ** 2 or ** 3 dependency on I

     weights = volume*Pdiss
     weights(1) = drr*drr
     weights = weights/sum(weights)

     shell_intensities(1) = pulse_Imax
     shell_intensities(2:) = pulse_Imax*exp(-2.0*rr(2:)**2/(kick_waist**2))


end subroutine focal_volume_average
