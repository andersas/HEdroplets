c$$$Date: Tue, 20 May 2003 15:06:45 +0200 (MEST)
c$$$From: Ad van der Avoird <avda@theochem.kun.nl>
c$$$To: Robert Zillich <zil@holmium.cchem.berkeley.edu>
c$$$Cc: whaley@socrates.berkeley.edu
c$$$Subject: Re: He-C2H2
c$$$Parts/Attachments:
c$$$   1 Shown     17 lines  Text
c$$$   2   OK    ~9.7 KB     Text, ""
c$$$----------------------------------------
c$$$
c$$$Dear Robert,
c$$$
c$$$Attached I send you the Fortran code with the He-acetylene potential that
c$$$you requested. Please keep me informed about your results. 
c$$$
c$$$With kind regards,   .............      Ad
c$$$
c$$$----------------------------------
c$$$Ad van der Avoird 
c$$$Institute of Theoretical Chemistry 
c$$$University of Nijmegen 
c$$$Nijmegen, The Netherlands 
c$$$Tel:    +31-24-3653037 
c$$$Fax:    +31-24-3653041 
c$$$E-mail: avda@theochem.kun.nl
c$$$----------------------------------
c$$$
c$$$
c$$$    [ Part 2, ""  Text/PLAIN (Name: "heacetpot.f")  209 lines. ]
c$$$    [ Not Shown. Use the "V" command to view or save this part. ]
c$$$
*  He-acetylene potential from ab initio SAPT calculations,
*  see R. Moszynski, P.E.S. Wormer, and A. van der Avoird,
*      J. Chem. Phys. 102 (1995) 8385.
*
*  For applications, see:
*      T. G. A. Heijmen, R. Moszynski, P. E. S. Wormer, A. van der Avoird,
*      U. Buck, I. Ettischer, and R. Krohne,
*      J. Chem. Phys. 107 (1997) 7260.
*  and:
*      T. G. A. Heijmen, R. Moszynski, P. E. S. Wormer, A. van der Avoird,
*      A. D. Rudert, J. B. Halpern, J. Martin, W. B. Gao, and H. Zacharias,
*      J. Chem. Phys. 111 (1999) 2519.
*
*  The configuration of the system is specified by the coordinates
*  R [distance between atom and molecular c.o.m.],
*  and XCOS [= cos(THETA)].
*  Energies are in Hartree, distances in bohr.
*
       SUBROUTINE POTXC(R,XCOS,V)
       IMPLICIT REAL*8 (A-H,O-Z)
       DIMENSION PLA(0:16)
       CALL vpleg_heacet(16,XCOS,PLA(0))
       CALL DISP(R,EDISP,PLA)
       CALL XIND(R,EIND,PLA)
       CALL REP(R,EREP,PLA)
       V=EREP+EIND+EDISP
       RETURN
       END

       SUBROUTINE XIND(R,EIND,PLA)
       IMPLICIT REAL*8 (A-H,O-Z)
       DIMENSION AA(0:16),BET(0:16),BETT(0:16),PLA(0:16)
       AA(0)=1.54177054591745621D00
       AA(2)=1.59912440700066782D00
       AA(4)=0.349676880920980149D00
       AA(6)=-0.249491099568268059D-01
       AA(8)=-0.380976695621587161D-01
       AA(10)=0.975560454620914841D-02
       AA(12)=0.733070412255666659D-02
       AA(14)=-0.673591559982077495D-04
       BET(0)=2.00813889836353221D00
       BET(2)=0.367695951523989040d00
       BET(4)=0.110467151754973431D00
       BET(6)=-0.141635012785763063d-01
       BET(8)=-0.216957501867133147D-01
       BET(10)=0.221209050809010200D-02
       BET(12)=0.485444557876708697D-02
       BET(14)=0.702755453198513659D-03
       BETT(0)=2.00792831056897070D00
       BETT(2)=0.368026658337549217D00
       BETT(4)=0.110517355900627556D00
       BETT(6)=-0.147687056605053085D-01
       BETT(8)=-0.207531964463634651D-01
       BETT(10)=0.103877655295501591D-02
       BETT(12)=0.658930237001839440D-02
       BETT(14)=-0.147579148954246446D-02
       A=0.D00
       BETA=0.D00
       BETAT=0.D00
       DO 5 L=0,14,2
          A = A + AA(L) * PLA(L)
          BETA = BETA + BET(L) * PLA(L)
          BETAT = BETAT + BETT(L) * PLA(L)
   5   CONTINUE
       C80I=        -49.4365349D00
       C82I=        -56.4988971D00
       C84I=        -42.3741728D00
       C100I=      -377.1355809D00
       C102I=     -3124.0211179D00
       C104I=     -2000.7330404D00
       C106I=     -1017.3732158D00
       C120I=    -11129.2819239D00
       C122I=    -17377.2125745D00
       C124I=   -106459.1923162D00
       C126I=    -79686.6910826D00
       C128I=    -23990.0576849D00
       C8I=C80I+C82I*PLA(2)+C84I*PLA(4)
       C10I=C100I+C102I*PLA(2)+C104I*PLA(4)
     $     +C106I*PLA(6)
       C12I=C120I+C122I*PLA(2)+C124I*PLA(4)
     $     +C126I*PLA(6)
     $     +C128I*PLA(8)
!       XIMULT=FD(8,BETAT,R)*C8I/R**8+
!     $       FD(10,BETAT,R)*C10I/R**10+FD(12,BETAT,R)*C12I/R**12
       BETATR = BETAT*R
       R2 = R*R
       XIMULT=(dnOPT2(8,BETATR)*C8I+
     $         dnOPT2(10,BETATR)*C10I/R2 + dnOPT2(12,BETATR)*C12I/R2**2)
     $         /R**8
       EIND=-DEXP(-BETA*(R-A))+XIMULT
       RETURN
       END
       SUBROUTINE REP(R,EREP,PLA)
       IMPLICIT REAL*8 (A-H,O-Z)
       DIMENSION AA(0:18),BB(0:18),ALPH(0:16),PLA(0:16)
       AA(0)=2.49651414645867797D00
       AA(2)=1.36487624411546449D00
       AA(4)=0.230212053721340099D00
       AA(6)=-0.153465146487973200D-01
       AA(8)=0.115937989481625873D-01
       AA(10)=-0.575705004402370449D-02
       AA(12)=0.354226943305890538D-02
       AA(14)=-0.596460684420615714D-02
       AA(16)=0.453658789152738308D-02
       AA(18)=-0.272204503741310406E-02
       BB(0)=0.920989997349769718D00
       BB(2)=1.65429896099134255D00
       BB(4)=0.276741498865257340D00
       BB(6)=-0.858358702442607729D-01
       BB(8)=-0.585130544017367377D-02
       BB(10)=0.690352342082658776D-01
       BB(12)=-0.455446845252151164D-01
       BB(14)=0.469617614048305643D-02
       BB(16)=0.677203472443357359D-03
       BB(18)=0.771662929351580977D-02
       ALPH(0)=1.70741452047050801D00
       ALPH(2)=0.123050934032571196D00
       ALPH(4)=-0.202583447561236890D-01
       ALPH(6)=0.252591382778491434D-01
       ALPH(8)=0.305755831413249135D-01
       ALPH(10)=-0.288990523252774384D-01
       ALPH(12)=0.128508409625628008D-01
       ALPH(14)=-0.448627932236489967D-02
       ALPH(16)=0.733753512316038907D-02
       A=0.D00
       B=0.D00
       ALPHA=0.D00
       DO 5 L=0,16,2
          A = A + AA(L) * PLA(L)
          B = B + BB(L) * PLA(L)
          ALPHA = ALPHA + ALPH(L) * PLA(L)
   5   CONTINUE
       EREP = DEXP(-ALPHA*(R-A)) - R * DEXP(-ALPHA*(R-B))
       RETURN
       END
       SUBROUTINE DISP(R,EDISP,PLA)
       IMPLICIT REAL*8 (A-H,O-Z)
       DIMENSION AA(0:24),BET(0:16),BETT(0:16),PLA(0:16)
       DIMENSION CC(0:16)
       AA(0)=0.528598017011038923D00
       AA(2)=1.25294090478270692D00
       AA(4)=0.239674579302971785D00
       AA(6)=-0.283258973090457569D-01
       AA(8)=0.129697567138882339D-01
       AA(10)=0.229791750803129899D-01
       AA(12)=0.107955617576875745D-01
       AA(14)=-0.453748411733807984D-02
       AA(16)=-0.645776645283614812D-02
       AA(18)=-0.447381664585907828D-02
       AA(20)=0.494848160109860689D-03
       AA(22)=0.107082368242969635D-02
       AA(24)=0.872135726538730231D-03
       CC(0)=0.752069065103413203D00
       CC(2)=1.01610043203094280D00
       CC(4)=0.389654091657134882D00
       CC(6)=0.113811695185820419D00
       CC(8)=0.175212874854004212D-01
       CC(10)=-0.539246769373521572D-02
       CC(12)=-0.267466495527758814D-02
       CC(14)=0.147578366864296439D-02
       CC(16)=0.152203381434781043D-02
       BET(0)=1.36843040293633278D00
       BET(2)=0.144797643167711737D00
       BET(4)=0.200375691565646552D-01
       BET(6)=-0.367874458211933653D-01
       BET(8)=-0.835546893059466167D-02
       BET(10)=0.169066799075748044D-01
       BET(12)=0.176409292775520539D-01
       BET(14)=-0.968854704175413622D-03
       BET(16)=-0.952961500558694233D-02
       BETT(0)=1.09761244986821938D00
       BETT(2)=0.441249733120338272D00
       BETT(4)=0.102180423135972198D00
       BETT(6)=-0.116539066455536619D00
       BETT(8)=-0.902870717480219742D-01
       BETT(10)=0.330738232071706370D-01
       BETT(12)=0.752228092504989149D-01
       BETT(14)=0.571617827767613267D-02
       BETT(16)=-0.377882328169538359D-01
       BETT(0)=1.08944330112993826
       BETT(2)=0.419354870908879906
       BETT(4)=0.111436113451997970
       BETT(6)=-0.727758838948523445E-01
       BETT(8)=-0.663251749808492774E-01
       BETT(10)=-0.570398735854630006E-03
       BETT(12)=0.265430916918432749E-01
       BETT(14)=0.632418056161352347E-02
       BETT(16)=-0.772108745671576911E-02
       A=0.D00
       BETA=0.D00
       BETAT=0.D00
       C=0.D00
       DO 5 L=0,16,2
          A = A + AA(L) * PLA(L)
          C = C + CC(L) * PLA(L)
          BETA = BETA + BET(L) * PLA(L)
          BETAT = BETAT + BETT(L) * PLA(L)
   5   CONTINUE
       C60=   -.1872843172D+02
       C62=    .2692733787D+01
       C80=   -.1596276760D+04
       C82=    .2082336788D+04
       C84=   -.2343924992D+03
       C100=   -.1089918423D+06
       C102=    .1861332708D+06
       C104=   -.1025460388D+06
       C106=    .1179168423D+05
       C120=   -.7505846879D+07
       C122=    .1443967709D+08
       C124=   -.1072062681D+08
       C126=    .4871096937D+07
       C128=   -.5954119996D+06
       C60=C60   -.2383457401D+00
       C62=C62   -.3757028415D+00
       C80=C80   -.1177994106D+03
       C82=C82   +.7713654976D+02
       C84=C84   +.1768383222D+02
       C100=C100   -.1788571922D+05
       C102=C102   +.2845820655D+05
       C104=C104   -.1560454801D+05
       C106=C106   +.2622051984D+04
       C120=C120   -.1725860110D+07
       C122=C122   +.3325436339D+07
       C124=C124   -.2407923525D+07
       C126=C126   +.1241164930D+07
       C128=C128   -.2625813063D+06
       C6=C60+C62*PLA(2)
       C8=C80+C82*PLA(2)+C84*PLA(4)
       C10=C100+C102*PLA(2)+C104*PLA(4)+C106*PLA(6)
       C12=C120+C122*PLA(2)+C124*PLA(4)+C126*PLA(6)
     $    +C128*PLA(8)
!       DMULT=FD(6,BETAT,R)*C6/R**6+FD(8,BETAT,R)*C8/R**8+
!     $       FD(10,BETAT,R)*C10/R**10+FD(12,BETAT,R)*C12/R**12
       BETATR = BETAT*R
       R2 = R*R
       DMULT=(dnOPT2(6,BETATR)*C6 + dnOPT2(8,BETATR)*C8/R2+
     $        dnOPT2(10,BETATR)*C10/R2**2 + dnOPT2(12,BETATR)*C12/R2**3)
     $       /R**6
       EDISP=-DEXP(-BETA*(R-A))+C*DMULT
       RETURN
       END

       DOUBLE PRECISION FUNCTION PL(L,X)
       IMPLICIT REAL*8 (A-H,O-Z)
       P0=1.D00
       P1=X
       IF(L.EQ.0) THEN
       PL=P0
       RETURN
       END IF
       IF(L.EQ.1) THEN
       PL=P1
       RETURN
       END IF
       DO 1 I=1,L-1
       PIP1=(DFLOAT(2*I+1)*X*P1-DFLOAT(I)*P0)/DFLOAT(I+1)
       P0=P1
       P1=PIP1
    1  CONTINUE
       PL=PIP1
       RETURN
       END

       DOUBLE PRECISION FUNCTION FD(N,A,R)
       implicit real*8 (a-h,o-z)
       fd=1.d00
       rr=a*r
       do 1 i=1,n
       fd = fd + rr**i/fact(i)
    1  continue
       fd=1.d00-dexp(-rr)*fd
       return
       end

       DOUBLE PRECISION FUNCTION FACT(I)
       if(i.eq.0) then
       fact=1.d00
       return
       end if
       if(i.eq.1) then
       fact=1.d00
       return
       end if
       fact = 1.d00
       do 1 j=2,i
       fact = fact*dfloat(j)
    1  continue
       return
       end

      subroutine vpleg_heacet(lmax,y,p)
      implicit real*8 (a-h,o-z)
      integer lmax,l,lp,lm
      
      dimension p(lmax+1)
!
          p(1)=1.d0
          p(2)=y
        if(lmax.gt.1) then  
          do 10 l=2,lmax
            lp=l+1
            lm=l-1
            xlml=dble(lm+l)
            xlm=dble(lm)
            xli=1.d0/dble(l)
              p(lp)=(xlml*y*p(l)-xlm*p(lm))*xli
 10       continue
        end if
      return
      end
