\documentclass[10pt,epsf]{article}
\usepackage{amsmath}
\usepackage{mathtools}

%\documentstyle[12pt,german,epsf]{report}
%\parskip1.0ex
%\usepackage{exscale}
\frenchspacing
\sloppy
\topmargin-2.0cm
%links Platz fuers Lochen:
\oddsidemargin-0.cm
\textwidth17cm
%die Seite von links bis rechts ausfuellend:
%\oddsidemargin-1.0cm
%\textwidth18cm
\textheight24cm

% if variablen
\newif\ifpic
\pictrue

\newif\ifcomment
\commentfalse

%\evensidemargin1.5cm

% alle makros beginnen mit "q"
\def\qrvec{({\bf r}_1,\ldots,{\bf r}_N)}           % vectors (r1,...,rN)
\def\qJ{{\bf J}}                                   % fettes r
\def\qr{{\bf r}}                                   % fettes r
\def\qk{{\bf k}}                                   % fettes k
\def\qK{{\bf K}}                                   % fettes k
\def\qp{{\bf p}}                                   % fettes p
\def\qq{{\bf q}}                                   % fettes q
\def\qa{{\bf a}}                                   % fettes q
\def\qj{{\bf j}}                                   % fettes q
\def\qJ{{\bf J}}                                   % fettes q
\def\qb{{\bf b}}                                   % fettes q
\def\qd{{\bf d}}                                   % fettes q
\def\qo{{\bf o}}                                   % fettes q
\def\qm{{\bf m}}                                   % fettes q
\def\qy{{\bf y}}                                   % fettes q
\def\qx{{\bf x}}                                   % fettes q
\def\qlim#1#2{ \stackrel{{#1}\to{#2}}{\longrightarrow} }

\begin{document}

\noindent{\large\bf Non-equilibrium many-body problems}\medskip

\noindent{Robert E. Zillich}\hfill{\it \today}

\noindent\hrulefill
%\vspace*{1cm}

\baselineskip3.5ex
\unitlength1cm

%----------------------------------------------------------------------

\section{(Non)adiabatic Alignment in $^4$He}

\subsection{\dots with single $^4$He atom}

We split the problem into three steps:
\begin{enumerate}
\item finding the ground state (at finite temperature: the equilibrium) of the molecule, done by
  imaginary time propagation, where the Schr\"odinger equation becomes a diffusion equation
  leading to the ground state.
\item applying the pulse.  For the time being, we assume a very short pulse, such that
  all other interactions are negligible during the pulse.  This generates a superposition of excited states.
  It is easy to generalize this,
  by using the technique of step 3 also for the evolution of the complex during the pulse.
\item evolution after the pulse, i.e. evolution of a superposition of excited states, using
  the same time propagation technique as in step 1, but for real time.
\end{enumerate}


\subsubsection{Schr\"odinger equation in lab frame basis}

We start with the Schr\"odinger equation for two particles
and derive the coupled equations governing the expansions coefficients of the two-body wave function
in a lab frame basis, as given by Hutson and Thornley, JCP 100, 2505 (1993).  This is a rather
technical and unexciting exercise in angular momentum algebra.

The relative distance is $\qr_1$ which we often
abbreviate simply by 1, and reduced mass is $m$.  One of the particles (the molecule) has an internal
degree of freedom $x$ (such as rotation or vibration of the molecule).  The kinetic energy
operator for the internal degree of freedom shall be $T_x$, the interaction potential between
the two particles $V(x,1)$, and $W(x,t)$ is a time-dependent external potential describing an
alignment pulse.  Hence we got
$$
  H\Psi(x,1;t)\equiv
  T_x\Psi(x,1;t) - {\hbar^2\over 2m}\nabla_1^2\Psi(x,1;t) + V(x,1)\Psi(x,1;t) + W(x,t)\Psi(x,1;t) = 
  i\hbar{\partial\over\partial t}\Psi(x,1;t)
$$
For a strongly bound atom-molecule complex, the molecule frame is usually the preferrable.
But the lab frame is easier to work with, so for the time being we use the lab frame.
We expand the wave function in the free molecule states
$$
\Psi(x,1;t) = \sum_j \phi_j(x)a_j(1;t)\qquad\mbox{where}\quad T_x\phi_i(x) = t_i\phi_i(x)
$$
We plug this into the Schr\"odinger equation and project onto $\langle\phi_i(t)|$ and obtain a
coupled set of equations for $a_i(1;t)$
\begin{equation}
- {\hbar^2\over 2m}\nabla_1^2 a_i(1;t) + t_i\phi_i(x) + \sum_j V_{ij}(1) a_j(1;t) + \sum_j W_{ij}(t) a_j(1;t) =
  i\hbar{\partial\over\partial t}a_i(1;t)
\label{eq:coupledgeneral}
\end{equation}
with
\begin{align*}
V_{ij}(1) &= \int dx\phi_j^*(x) V(x,1) \phi_i(x) \\
W_{ij}(t) &= \int dx\phi_j^*(x) W(x,t) \phi_i(x)
\end{align*}

We are specifically interested in rotations, so $x=\Omega$, $T_x=BL^2$, and $\phi_=Y_{\ell m}$.
The interaction is a function of $r$ and $\theta$, the angle of $\qr$ in the molecule frame.
We expand $V$,
$$
  V(r,\cos\theta) = \sum_\ell P_\ell(\cos\theta) V_\ell(r)
  = \sum_{\ell m} {4\pi\over 2\ell+1} Y_{\ell m}(\Omega) Y^*_{\ell m}(\Omega_r) V_\ell(r)
$$
where $\Omega_r$ is the orientation of $\qr$ in the lab frame.
\begin{align*}
  V_{ij}(1) &= \sum_{\ell m}{4\pi\over 2\ell+1}
  \int d\Omega\ Y^*_{j_i m_i}(\Omega)Y_{\ell m}(\Omega)Y_{j_j m_j}(\Omega)\ Y^*_{\ell m}(\Omega_r) V_\ell(r)\\
  &= \sum_{\ell m}{4\pi\over 2\ell+1}
  (-1)^{m_i}\left[{j_i\atop -m_i}{\ell\atop m}{j_j\atop m_j}\right] Y^*_{\ell m}(\Omega_r) V_\ell(r)
\end{align*}
with the abbreviation
$$
  \left[{a\atop u}{b\atop v}{c\atop w}\right] \equiv 
  \Big( {(2a+1)(2b+1)(2c+1)\over 4\pi} \Big)^{1/2}
  \left({a\atop u}{b\atop v}{c\atop w}\right)
  \left({a\atop 0}{b\atop 0}{c\atop 0}\right)
$$
The pulse potential is expanded too
$$
  W(\Omega,t) = \sum_{\ell m}Y_{\ell m}(\Omega)w_{\ell m}(t)
$$
Following Seideman's work, we see that for a non-resonant pulse we have $\lambda=0,2$ and
particularly $\mu=0$.  The projection of $W(\Omega,t)\Psi(\omega,\qr)$ onto $Y^*_{\ell_i m_i}(\Omega)$
yields
$$
\sum_{\ell,j_j,m_j} w_\ell(t)(-1)^{m_i} \left({j_i\atop -m_i}{\ell\atop 0}{j_j\atop m_j}\right)
a_{j_j,m_j}(\qr)
\equiv 
\sum_{j_j} w_{j_i,j_j}(t) a_{j_j,m_j}(\qr)
$$
with
$$
  w_{j_i,j_j}(t)
= \sum_{\ell} w_\ell(t)(-1)^{m_i} \left({j_i\atop -m_i}{\ell\atop 0}{j_j\atop m_i}\right)
$$
$W(t)$ acts only during the pulse, which is usually short.  Below we will therefore use the
impulsive limit, where the pulse time is much shorter than all other time scales.

Next we switch to spherical coordinates for $\qr$
As usual, we have the kinetic energy of the relative translational motion
$$
  -{\hbar^2\over 2m}\nabla_1^2
  = -{1\over r}{\hbar^2\over 2m}{\partial^2\over\partial^2 r}r + {1\over 2m r^2}\hat L_r^2
  \equiv -{\hbar^2\over 2m}\hat R^2 + {1\over 2m r^2}\hat L_r^2
$$
$a_{j_j,m_j}(\qr)$ is thus further expanded in spherical harmonics and we get
$$
  \Psi(\Omega,\qr) = \sum_{jm}Y_{jm}(\Omega)a_{j m}(\qr)
= \sum_{jm}\sum_{LM}Y_{jm}(\Omega)Y_{LM}(\Omega_r)a_{jm}^{LM}(r)
$$
where
$$
  a_{jm}^{LM}(r) = \int d\Omega\int d\Omega_r Y^*_{jm}(\Omega)Y^*_{LM}(\Omega_r) \Psi(\Omega,\qr)
$$
We project the interaction term
$$
\sum_{j_jm_j}V_{ij}(\qr) a_{j_j,m_j}(\qr) = 
\sum_{j_jm_j} \sum_{\ell m}{4\pi\over 2\ell+1}
  (-1)^{m_i}\left({j_i\atop -m_i}{\ell\atop m}{j_j\atop m_j}\right) Y^*_{\ell m}(\Omega_r) V_\ell(r)
  \sum_{L'M'}Y_{L'M'}(\Omega_r) a_{j_j m_j}^{L'M'}(r)
$$
onto $Y_{LM}^*(\Omega_r)$
$$
\sum_{j_jm_j,\ell m,L'M'} (-1)^{m_i+M'}
  \left[{j_i\atop -m_i}{\ell\atop m}{j_j\atop m_j}\right]
  \left[{L\atop -M}{\ell\atop -m}{L'\atop M'}\right]
  {4\pi V_\ell(r)\over 2\ell+1}\ a_{j_j m_j}^{L'M'}(r)
$$
The above coupled equations for $a_i(1;t)$ become
\begin{align*}
& -{\hbar^2\over 2m}\hat R^2 a_{j_i m_i}^{LM}(r) + {\hbar^2 L(L+1)\over 2m r^2} a_{j_i m_i}^{LM}(r)
  + Bj_i(j_i+1) a_{j_i m_i}^{LM}(r)\ + \\
&\qquad +
  \sum_{j_jm_j,\ell m,L'M'} (-1)^{m_i+M'}
    \left[{j_i\atop -m_i}{\ell\atop m}{j_j\atop m_j}\right]
    \left[{L\atop -M}{\ell\atop -m}{L'\atop M'}\right]
  {4\pi V_\ell(r)\over 2\ell+1}\ a_{j_j m_j}^{L'M'}(r)
= i\hbar \dot a_{j_i m_i}^{LM}(r)
\end{align*}
apart from the pulse term that we discuss separately later.

The $(j,m;L,M)$ basis does not know about total angular moment conservation before and after
the pulse, so we combine the molecule rotation and $^4$He orbital angular momenta to the total angular moment
\begin{equation}
  \bar a_{LjJN}(r) \equiv
  (-1)^{j+L+N}\sqrt{2J+1}\ a_{LjJN}(r) =
  (-1)^{j+L+N}\sqrt{2J+1}\sum_{Mm} \left({j\atop m}{L\atop M}{J\atop -N}\right)
  a_{jm}^{LM}(r)
\label{eq:totJ}
\end{equation}
The other way round
$$
  \sum_{JN} (2J+1) \left({j\atop m}{L\atop M}{J\atop -N}\right) a_{LjJN}(r)
= \dots =   a_{jm}^{LM}(r)
$$
which we plug into the coupled equations
\begin{align*}
& -{\hbar^2\over 2m}\hat R^2    \sum_{JN} (2J+1) \left({j_i\atop m_i}{L\atop M}{J\atop -N}\right) a_{Lj_i JN}(r)
  + {\hbar^2 L(L+1)\over 2m r^2} \sum_{JN} (2J+1) \left({j_i\atop m_i}{L\atop M}{J\atop -N}\right) a_{Lj_i JN}(r) \\
& + Bj_i(j_i+1) \sum_{JN} (2J+1) \left({j_i\atop m_i}{L\atop M}{J\atop -N}\right) a_{Lj_i JN}(r)\ + \\
&\qquad +
  \sum_{JN} \sum_{j_jm_j,\ell m,L'M'} (2J+1) (-1)^{m_i+M'}
    \left[{j_i\atop -m_i}{\ell\atop m}{j_j\atop m_j}\right]
    \left[{L\atop -M}{\ell\atop -m}{L'\atop M'}\right]
  {4\pi V_\ell(r)\over 2\ell+1}\ \left({j_j\atop m_j}{L'\atop M'}{J\atop -N}\right) a_{L'j_j JN}(r) \\
&= i\hbar \sum_{JN} (2J+1) \left({j_i\atop m_i}{L\atop M}{J\atop -N}\right) \dot a_{L j_i JN}(r)
\end{align*}
We multiply with $\left({j_i\atop m_i}{L\atop M}{J'\atop -N'}\right)$ and sum over $M$ and $m_i$ and
use
$$
\sum_{Mm_i}\left({j_i\atop m_i}{L\atop M}{J'\atop -N'}\right)\left({j_i\atop m_i}{L\atop M}{J\atop -N}\right)
={1\over 2J'+1)}\delta_{J'J}\delta_{N'N}\delta(j_i,L,J')
$$
(where $\delta(j_i,L,J')$ is zero if the three $\ell$'s do not form a triangle) to obtain
new coupled equations
\begin{align*}
& -{\hbar^2\over 2m}\hat R^2    a_{Lj_i J'N'}(r)
 + {\hbar^2 L(L+1)\over 2m r^2} a_{Lj_i J'N'}(r)
 + Bj_i(j_i+1) a_{Lj_i J'N'}(r)\ + \\
& +
  \sum_{Mm_i}\sum_{JN} \sum_{j_jm_j,\ell m,L'M'} (2J+1) (-1)^{m_i+M'}
    \left({j_i\atop m_i}{L\atop M}{J'\atop -N'}\right)
    \left[{j_i\atop -m_i}{\ell\atop m}{j_j\atop m_j}\right]
    \left[{L\atop -M}{\ell\atop -m}{L'\atop M'}\right]
    \left({j_j\atop m_j}{L'\atop M'}{J\atop -N}\right)
    {4\pi V_\ell(r)\over 2\ell+1}\ a_{L'j_j JN}(r) \\
&= i\hbar \dot a_{Lj_i J'N'}(r)
\end{align*}
Both $J$ and $N$ are conserved, so we get (dropping the primes)
\begin{align*}
& -{\hbar^2\over 2m}\hat R^2    a_{Lj_i JN}(r)
 + {\hbar^2 L(L+1)\over 2m r^2} a_{Lj_i JN}(r)
 + Bj_i(j_i+1) a_{Lj_i JN}(r)\ + \\
& +
  \sum_{Mm_i}\sum_{j_jm_j,\ell m,L'M'} (2J+1) (-1)^{m_i+M'}
    \left({j_i\atop m_i}{L\atop M}{J\atop -N}\right)
    \left[{j_i\atop -m_i}{\ell\atop m}{j_j\atop m_j}\right]
    \left[{L\atop -M}{\ell\atop -m}{L'\atop M'}\right]
    \left({j_j\atop m_j}{L'\atop M'}{J\atop -N}\right)
    {4\pi V_\ell(r)\over 2\ell+1}\ a_{L'j_j JN}(r) \\
&= -{\hbar^2\over 2m}\hat R^2 a_{Lj_i JN}(r) + {\hbar^2 L(L+1)\over 2m r^2} a_{Lj_i JN}(r) + Bj_i(j_i+1) a_{Lj_i JN}(r)\ +\ 
    \sum_{L'l_j} V_{LL'j_ij_j JN}(r)\ a_{L'j_j JN}(r) \\
&= i\hbar \dot a_{Lj_i JN}(r)
\end{align*}
where
$$
  V_{LL'j_ij_j JN}(r) = 
  \sum_{Mm_i}\sum_{m_j,\ell m,M'} (2J+1) (-1)^{m_i+M'}
    \left({j_i\atop m_i}{L\atop M}{J\atop -N}\right)
    \left[{j_i\atop -m_i}{\ell\atop m}{j_j\atop m_j}\right]
    \left[{L\atop -M}{\ell\atop -m}{L'\atop M'}\right]
    \left({j_j\atop m_j}{L'\atop M'}{J\atop -N}\right)
    {4\pi V_\ell(r)\over 2\ell+1}
$$
The combination of four 3-j symbols can be summed up to a 6-j symbol, but
numerically, we will calculate the latter by the combination of four 3-j symbols anyway.
We have now arrived at eq.(12) of Hutson and Thornley, JCP 100, 2505 (1993),
specialized to a linear molecule.


\subsubsection{short pulse}

Assuming a short pulse of high strength, we proceed as proposed by Seideman for isolated molecules:
the laser-molecule interaction is so strong that all other terms in the Hamiltonian can be
neglected.  In the case of an isolated molecule it is just the rotational kinetic energy of
the molecule that is neglected.  Here we additionally neglect the translational kinetic energy
of the molecule-atom motion and the interaction potential between molecule and atom.
Since our method can be easily generalized to arbitrary pulses,
it might be interesting to investigate numerically how appropriate these approximations
are for a molecule-atom complex.

Hence the coupled equations eq.(\ref{eq:coupledgeneral}) are approxmated by
\begin{equation}
\sum_j W_{ij}(t) a_j(1;t) = i\hbar{\partial\over\partial t}a_i(1;t)
\label{eq:a}
\end{equation}
Since we neglected all terms but the interaction with the laser pulse in the
Hamiltonian, we have the same equations as for isolated molecules, see
Seideman, J. Chem. Phys. 115, 5965 (2001), section II.B and III.  The only difference
is that the coefficients depend on the molecule-atom distance, which does not
enter eq.(\ref{eq:a}).

We continue where we dropped the pulse term 
Specifically for our case of a linear molecule with an atom, we got above
$$
\sum_{j_j} w_{j_i,j_j}(t) a_{j_j,m_i}(\qr,t) = i\hbar\dot a_{j_i,m_i}(\qr,t)
$$
which we project onto $Y_{LM}(\Omega_r)$ as above ($W$ and thus $w_{jj;}$ does 
not depend on $\qr$)
$$
\sum_{j_j} w_{j_i,j_j}(t) a_{j_j,m_i}^{LM}(r,t) = i\hbar\dot a_{j_i,m_i}^{LM}(r,t)
$$
This is a system of ordinary first-order differential equations, that we can write
in a compact form
$$
W(t)\qa(r,t) = i\hbar\dot\qa(r,t)
$$
where $\qa$ is a vector with elements $a_{j_i,m_i}^{LM}(r,t)$ for given $m_i$, $L$, $M$, and $r$
and $W$ is the matrix built from $w_{j_i,j_j}$.  Compare with eq.(18) of Seideman for isolated molecules.
Our equation for molecule-atom complexes depends on $r$, $L$, $M$, but only as a parameter.
We use the same time-transformation $\xi=\int_0^t dt' g(t')$, where $g(t)$ is the shape of the pulse
including all kinds of prefactors.  The differential equation with time-dependent coefficients $w_{j_i,j_j}(t)$
(which have all the same time dependence $g(t)$) then becomes one with time-independent coefficients
$w^0_{j_i,j_j}$, in matrix form
$$
W^0\qa(r,\xi) = i\hbar{\partial\over\partial\xi}\qa(r,\xi)
$$
We solve this equation be diagonalizing $W^0$ and then calculate
$\qa(r,\xi_T)=e^{-iW^0\xi_T/\hbar}\qa(r,0)$, where $\xi_T=\int_0^T dt' g(t')$ and $T$ is the
pulse length; i.e. $\xi_T$ is the pulsestrength.  Conversely one could do it more
elegantly using the analytic solution by Seideman.

Note that if we are in the representation where the orbital and molecule angular momenta
are coupled $a_{LjJN}(r)$, we have to transform to the uncoupled representation, i.e.
undo eq.(\ref{eq:totJ}) to get back $a_{jm}^{LM}(r)$.

For the specific case of a off-resonant laser pulse, $w_{j,j'}$ is non-zero only for
$|j-j'|=0,2$. Obviously, the total $J$ is not conserved by the pulse, but the total $N$ is.



\subsubsection{time evolution}

$$
  -{\hbar^2\over 2m}\hat R^2 a_{Lj_i JN}(r)
+ {\hbar^2 L(L+1)\over 2m r^2} a_{Lj_i JN}(r) + Bj_i(j_i+1) a_{Lj_i JN}(r)\ +\ 
    \sum_{L'l_j} V_{LL'j_ij_j JN}(r)\ a_{L'j_j JN}(r) \\
= i\hbar \dot a_{Lj_i JN}(r)
$$
which we abbreviate
\begin{equation}
  H\qa(r,t) \equiv T_R \qa(r,t) + V(r) \qa(r) = i\hbar\dot \qa(r,t)
  \label{eq:Ha}
\end{equation}
Now $\qa$ is the vector containing as elements $a_{Lj_i JN}(r)$ for all $L$ and $j_i$ ($J$ and $N$
are conserved).  $T_R$ is the translational kinetic energy, and $V$ is the matrix containing all the other
terms.  We are interested in the time evolution, after a (short) pulse has resulted
in an initial $\qa(r,0)$.  This can be either obtained by first calculating the eigenstates
of the stationary Schr\"odinger equation $H\qa(r,t)=E\qa(r,t)$, and then expanding the
state resulting from the pulse in these eigenstates.  This would be the quite efficient, at least
after the eigenstates have been calculated.  However, this cannot be generalized to pulsed of
finite length.  Secondly, our final goal is the nonlinear
dynamics of a molecule coupled to the many $^4$He atoms.  We anticipate that any
approximation to this many-body problem will look like a nonlinear generalization of
eq.(\ref{eq:Ha}), which could not be solved via eigenstate expansion.  Therefore we
generated the time evolution directly from the time-evolution operator
$$
  \qa(r,t) = G \qa(r,0) \equiv e^{-iHt/\hbar} \qa(r,0) = e^{-i(T_R+V)t/\hbar} \qa(r,0)
$$
The operator $G$ or its application to $\qa$ cannot be done exactly.  We split the full
time evolution in to small steps, and use the simplest
approximation to $G$, the Trotter approximation
$$
  G = e^{-iVt/2\hbar} e^{-iT_Rt/\hbar} e^{-iVt/2\hbar} + {\rm O}(t^3)
$$
The part containing the kinetic energy is best formulated in Fourier space, where
the kinetic energy is already diagonal.  The part containing the potential matrix is
calculated by diagonalizing $V$ (must be done only once), $\Lambda=M V M^{-1}$, where
$\Lambda$ is the diagonal matrix of eigenvalues $\lambda_i$.
$$
  e^{-iV\tau} = e^{-iM^{-1} \Lambda M\tau} = M^{-1}e^{-i\Lambda \tau}M
$$
The diagonal matrix $\Lambda$ can be exponentiated like a number.  So in order to
apply $e^{-iV\tau}$ to $\qa(r,t)$ we need to first transform into the eigenbasis
of $V$, $M\qa$, multiply each element of the resulting vector with $\lambda_i$
and transform back, $M^{-1}\qa$.  Since $V$ depends on $r$, all this has to be
done for each $r$.

For each time step $\Delta t$ we thus perform the following sequence of operations to the
vector of coefficients, $\qa(r)$ at the current time $t$ to evolve it to time $t+\Delta t$
\begin{enumerate}
\item  $M\qa(r)$ gives us $a_i(r)$ 
\item  multiply $e^{-i\Delta t\lambda_i(r)/2\hbar} a_i(r)$
\item  $M^{-1}\qa(r)$ 
\item  FFT[$\qa(r)$]
\item  multiply $e^{-i\Delta tk^2} a_i(k)$
\item  FFT$^{-1}$[$\qa(k)$]
\item  $M\qa(r)$ gives us $a_i$ 
\item  multiply $e^{-i\Delta t\lambda_i/2\hbar} a_i(r)$
\item  $M^{-1}\qa(r)$ 
\end{enumerate}
Note that the two half-steps 1-3 and 7-9 can be combined into a
single full-step, if $\qa$ is just propagated, but no expectation values are
calculated.  We calculate expectation values only after e.g. 10 $\Delta t$
(= 1 time block).

As mentioned above, we use the same method to calculate the ground state of the complex from which we excite
with a pulse, by simply changing to imaginary time.  In imaginary time, the Schr\"ordinger
equation is a diffusion equation and any starting wave function with finite overlap
with the ground state will diffuse to the ground state.  By orthogonalization
this could later be generalized to obtain excited states as starting state.


\newpage

\end{document}


