\documentclass[a4paper,11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{amsmath,amssymb}

\usepackage{color,graphicx}
\usepackage[rel]{overpic}

\newcommand{\bra}[1]{\left\langle #1 \right\rvert}
\newcommand{\ket}[1]{\left\lvert #1 \right\rangle}
\newcommand{\braket}[1]{\left\langle #1 \right\rangle}

\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\green}[1]{{\color{green} #1}}
\newcommand{\blue}[1]{{\color{blue} #1}}

\author{Anders A. Søndergaard}
\date{April 28, 2015}
\title{Notes on keeping track of probability otherwise lost to absorbing
boundary}
\begin{document}
\maketitle

\subsection*{Inner product and expectation value}

The standard inner product is defined as
\[ \braket{\psi_1\vert \psi_2} = \int \psi_1^*\psi_2\ d\Omega\ r^2 dr\ d\Omega_\mathbf{r} , \]
i.e. just the integral of $\psi_1^*\psi_2$ over all coordinates.

We expand the wave function in spherical harmonics
\begin{align*}
     \psi(\mathbf{r},\theta,\phi) &= \sum_{jmLM} a_{jmLM}(r)\ket{jmLM} \\
                                  & = \sum_{jmLM}\sum_{JN} a_{jmLM}(r)\ket{jLJN}\braket{jLJN\vert jmLM} \\
                                  & = \sum_{jLJN}\sum_{mM} a_{jmLM}(r)\ket{jLJN}\braket{jLJN\vert jmLM} \\
                                  & = \sum_{jLJN} a_{jLJN}(r)\ket{jLJN},
\end{align*}
where
\[ a_{jLJN}(r) = \sum_{mM} a_{jmJM}(r)\braket{jLJN\vert jmLM}, \]
hence
\begin{align*}
     \braket{\psi_b\vert\psi_a} & = \int r^2 dr \sum_{jj'LL'JJ'NN'} \bra{j'L'J'N'} b_{j'L'J'N'}^*(r)a_{jLJN}(r)\ket{jLJN} \\
     & = \sum_{jj'LL'JJ'NN'} \braket{j'L'J'N' \vert jLJN} \int b_{j'L'J'N'}^*(r)a_{jLJN}(r)\ r^2 dr \\
     & = \sum_{jLJN} \int b_{jLJN}^*(r)a_{jLJN}(r)\ r^2 dr.
\end{align*}
The norm can now be calculated as
\[ |\psi|^2 = \braket{\psi\vert\psi} = \sum_{jLJN} \int a_{jLJN}^*(r)a_{jLJN}(r)\ r^2 dr. \]
For a molecular ($r,L,M$-independent) operator $A$,
with
\[ A_{jm}^{j'm'} = \bra{j'm'}A\ket{jm}, \]
\begin{align*}
     A_{jLJN}^{j'J'N'} &= \bra{j'L'J'N'}A\ket{jLJN} \\
                     & = \sum_{MM'mm'} \braket{j'L'J'N' \vert j'm'L'M'}\bra{j'm'L'M'}A\ket{jmLM}\braket{jmLM \vert jLJN} \\
                     & = \sum_{MM'mm'} \braket{j'L'J'N' \vert j'm'L'M'}A_{jm}^{j'm'}\delta_{LL'}\delta_{MM'}\braket{jmLM \vert jLJN} \\
                     & = \sum_{M=N-m,mm'} A_{jm}^{j'm'}\braket{j'LJ'N' \vert j'm'LM}\braket{jmLM \vert jLJN}\delta_{LL'}.
\end{align*}
It is seen that $A_{jLJN}^{j'J'N'}$ can only be nonzero if
$L = L'$, $N-N' = m-m'$, $M = N-m = N'-m'$
and if the selection rules
\[ |j'-L| \leq J \leq j'+ L \]
\[ |j-L| \leq J \leq j+ L \]
are obeyed.

The expectation value of $A$ can now be calculated:
\begin{align*}
     \bra{\psi}A\ket{\psi} &= \braket{\psi\vert A\psi} \\
                           &= \int r^2\ dr\sum_{jj'LJJ'NN'} \bra{j'LJ'N'}a_{j'LJ'N'}^*(r)\ A\  a_{jLJN}(r)\ket{jLJN} \\ 
                           &= \sum_{jj'LJJ'NN'} A_{jLJN}^{j'J'N'} \int r^2\ dr\ a_{j'LJ'N'}^*(r)\ a_{jLJN}(r) \\ 
\end{align*}


\subsection*{Dealing with probabilities lost to absorbing boundaries}

The Hamiltonian of the molecule-He system is
\begin{align*}
\mathcal{H} &= \mathcal{H_\text{rot}} - \frac{\hbar^2 \nabla^2_\mathbf{r}}{2m} + V(\mathbf{r},\theta,\phi) + W(\theta,t) \\
            &= \mathcal{H_\text{rot}} - \frac{\hbar^2}{2mr}\frac{\partial^2 r}{\partial r^2}r + \frac{\hat{L}^2}{2mr^2} + V(\mathbf{r},\theta,\phi) + W(\theta,t).
\end{align*}
Here, $\theta$ and $\phi$ are the polar and azimuthal angle of the molecular axis wrt. the lab reference frame. $\mathbf{r}$ is the vector from origo
to the He atom.

Expanding the Schr\"odinger equation in the coupled angular momentum
basis \[ \ket{jLJN} = \sum C_{jmLM}^{JN}\ket{jm}\ket{LM} = \sum C_{jmLM}^{JN}Y_{jm}(\theta,\phi)Y_{LM}(\theta_\mathbf{r},\phi_\mathbf{r}) \]
and projecting it onto the $\ket{j'L'J'N'}$ state yields a system
of coupled ordinary differential equations, called the coupled channel equations, for the expansion coefficients $a_{jLJN}(r)$:
\begin{align*}
     i\hbar\dot{a}_{jLJN}(r) & = -\frac{\hbar^2}{2m}\hat{\mathcal{R}}^2a_{jLJN}(r) + \frac{\hbar^2L(L+1)}{2mr^2}a_{jLJN}(r) + Bj(j+1)a_{jLJN}(r) \\
 & + \sum_{j'L'}V_{jLJN}^{j'L'}(r)a_{j'L'JN}(r) + \sum_{j'J'} W_{jLJN}^{j'J'}a_{j'LJ'N}(r),
\end{align*}
where $\hat{\mathcal{R}}^2 = \tfrac{1}{r}\tfrac{\partial^2}{\partial r^2}r$.
In the simulation, we work with the coefficients \mbox{$u_{jLJN} = r \cdot a_{jLJN}$},
because then the $\hat{\mathcal{R}}^2$ operator becomes easy to calculate.
Multiplying the coupled channel equations with r gives
\begin{align*}
     i\hbar\dot{u}_{jLJN}(r) & = -\frac{\hbar^2}{2m}\frac{\partial^2}{\partial r^2}u_{jLJN}(r) + \frac{\hbar^2L(L+1)}{2mr^2}u_{jLJN}(r) + Bj(j+1)u_{jLJN}(r) \\
 & + \sum_{j'L'}V_{jLJN}^{j'L'}(r)u_{j'L'JN}(r) + \sum_{j'J'} W_{jLJN}^{j'J'}u_{j'LJ'N}(r),
\end{align*}
Note how $\hat{\mathcal{R}}^2$ turned into the $\tfrac{\partial^2}{\partial r^2}$ operator, which is diagonal in Fourier space.
Note that $u(r) = a(r) = 0$ because $V(r=0)$ is infinite.

Because the pulse is strong enough to produce scattering states,
i.e. states where the atom is no longer bound to the molecule,
we can get population at any $r$.

This is a problem because the state vector containing all the expansion
coefficients in principle would need to be infinitely large.
Or it would at least have to be large enough to contain
the coefficients for large enough $r$ during the finite simulation time.
Anyway, this is infeasible, and at some point we have to
neglect state far away. This is not a problem for the simulation
itself, but it is for the readout of results.
To measure the expectation value of some \textbf{molecular} observable\footnote{one that's diagonal in $r$, $L$ and $N$. If it is not diagonal in $r$, we must integrate twice over $r$.} $A$ (for example $\cos^2\theta$)
\[ \braket{A} = \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\int_0^\infty dr\ u^*_{jLJN}u_{j'LJ'N}.  \]
Throwing away components at large $r$ can be seen to remove 
weight in the summation, giving
a wrong expectation value.

To get around this, we first pick some $R$ large enough that the interaction with the molecule
can be neglected, i.e. $V(r>R) \approx 0$. Further, $R$
must be large enough that no probability amplitude
has reached $r=R$ before the pulse is over. I.e. we can set $W = 0$

We then split $\braket{A}$ up in a known part, and a part
we need to find:
\begin{equation}
\braket{A} = \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\int_0^R dr\ u^*_{jLJN}u_{j'LJ'N} + \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\int_R^\infty dr\ u^*_{jLJN}u_{j'LJ'N}.
\label{eq:expectation}
\end{equation}
The idea now is to use the continuity equation
to replace the last integral over space with an integration in time.

More concretely, if we define the weight density $\rho_w$ as 
\[ \rho_w = u^*_{jLJN}(r)u_{j'LJ'N}(r), \]
we can use the (1D) continuity equation
\begin{equation}
     \frac{\partial\rho_w}{\partial t} = -\frac{\partial j_w}{\partial r} + \sigma(r,t),  
\label{eq:continuity}
\end{equation}
where $j_w$ is the weight current and $\sigma$ is a source term, to rewrite the integral as follows. Assuming $\sigma=0$, we can write
\begin{align*}
     \int_R^\infty dr\ u^*_{jLJN}u_{j'LJ'N} & = \int_R^\infty dr\ \rho_w \\
                                            & = \int_R^\infty dr\int_0^tdt\ \frac{\partial\rho_w}{\partial t} \\
                                            & = -\int_0^tdt\int_R^\infty dr\ \frac{\partial}{\partial r} j_w \\
                                            & = \int_0^tdt\ j_w(R)
\end{align*}
The problem is now to find an expression for $j_w(R)$.
Further, if the source term is nonzero, it will have to be dealt with somehow.

Following the standard way of deriving the continuity equation,
we look at the time derivative of $\rho_w$
(note the simplifying index notation $n = jLJN$, $n' = j'LJ'N$):
\begin{equation}
     \frac{\partial}{\partial t} u^*_{n}(r)u_{n'}(r) = \dot{u}^*_{n}(r)u_{n'}(r) + u^*_{n}(r)\dot{u}_{n'}(r)
\label{eq:dtweight}
\end{equation}

For $r > R$ the coupled\footnote{Note that they are now not so coupled anymore. The only coupling is in the radial direction through $\tfrac{\partial^2}{\partial r^2}$} channel equations and their complex conjugate simplify to
\begin{align*}
i\hbar\dot{u}_{n'}(r) &= -\frac{\hbar^2}{2m}\frac{\partial^2}{\partial r^2}u_{n'}(r) + E^\text{ang}_{j'L}(r) u_{n'}(r) \\
-i\hbar\dot{u}^*_{n}(r) &= -\frac{\hbar^2}{2m}\frac{\partial^2}{\partial r^2}u^*_{n}(r) + E^\text{ang}_{jL}(r) u^*_{n}(r)
\end{align*}
where
\[ E^\text{ang}_{jL}(r) = \frac{\hbar^2L(L+1)}{2mr^2} + Bj(j+1) \] 
Since $r>R$ is large, we also neglect the first term in $E^\text{ang}$:
\[ E^\text{ang}_{jL}(r>R) \approx E^\text{rot}_{j} = Bj(j+1) \] 
In this way, it is constant, and can be taken outside an integration.
To obtain the right hand side terms in \eqref{eq:dtweight}, multiply
the coupled channel equations with $\tfrac{-i}{\hbar}u^*_{n}(r)$
and the complex conjugate equations with $\tfrac{i}{\hbar}u_{n'}(r)$
\begin{align*}
     u^*_{n}(r)\dot{u}_{n'}(r) &= \frac{i\hbar}{2m}u^*_{n}(r)\frac{\partial^2}{\partial r^2}u_{n'}(r) - \frac{iE^\text{rot}_{j'}}{\hbar} u^*_{n}(r)u_{n'}(r) \\
     \dot{u}^*_{n}(r)u_{n'}(r) &= -\frac{i\hbar}{2m}u_{n'}(r)\frac{\partial^2}{\partial r^2}u^*_{n}(r) + \frac{iE^\text{rot}_{j}}{\hbar} u^*_{n}(r)u_{n'}(r) \\ 
\end{align*}
Plugging these expressions into \eqref{eq:dtweight} gives
\begin{align*}
     \dot{\rho}_w &= (\dot{u}^*_{n}(r)u_{n'}(r) + u^*_{n}(r)\dot{u}_{n'}(r)) \\
             &= \frac{i\hbar}{2m}\left[u^*_{n}(r)\frac{\partial^2}{\partial r^2} u_{n'}(r) - u_{n'}(r)\frac{\partial^2}{\partial r^2}u^*_{n}(r)\right] \\
             & + \frac{i(E^\text{rot}_{j} - E^\text{rot}_{j'})}{\hbar} \rho_w(r)
\end{align*}
Taking the right hand side to be $-\tfrac{\partial j_w}{\partial r} + \sigma$ (see \eqref{eq:continuity}),
it seems natural to pick
\begin{equation}
\frac{\partial j_w}{\partial r}  = \frac{i\hbar}{2m}\left[u_{n'}(r)\frac{\partial^2}{\partial r^2}u^*_{n}(r) - u^*_{n}(r)\frac{\partial^2}{\partial r^2} u_{n'}(r)\right] 
\label{eq:diffcurrent}
\end{equation}
and
\begin{equation}
\sigma = \frac{i(E^\text{rot}_{j} - E^\text{rot}_{j'})}{\hbar} \rho_w(r).
\label{eq:source}
\end{equation}
Note that when $n = n'$, $\sigma = 0$ as you could expect,
because $\rho_w$ is then just the (radial) probability density.

Integrating equation \eqref{eq:diffcurrent} by parts, we see
\begin{align*}
     j_w(R) & = -\int_R^\infty \frac{\partial j_w}{\partial r}  dr \\
            & = -\int_R^\infty \frac{i\hbar}{2m}\left[u_{n'}(r)\frac{\partial^2}{\partial r^2}u^*_{n}(r) - u^*_{n}(r)\frac{\partial^2}{\partial r^2} u_{n'}(r)\right] dr \\
            & = -\frac{i\hbar}{2m}\left[u_{n'}(r)\frac{\partial}{\partial r}u^*_{n}(r) - u^*_{n}(r)\frac{\partial}{\partial r} u_{n'}(r)\right]_R^\infty \\
            & = \frac{i\hbar}{2m}\left[u_{n'}(r)\frac{\partial}{\partial r}u^*_{n}(r) - u^*_{n}(r)\frac{\partial}{\partial r} u_{n'}(r)\right]_{r=R}
\end{align*}
which looks like the normal expression for the current.

If we ignored $\sigma$, we would have
\begin{align}
     \nonumber
     \braket{A} &= \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\int_0^R dr\ u^*_{jLJN}u_{j'LJ'N} \\
                & + \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\ \frac{i\hbar}{2m}\int_0^tdt\ \left[u_{n'}(r)\frac{\partial}{\partial r}u^*_{n}(r) - u^*_{n}(r)\frac{\partial}{\partial r}u_{n'}(r)\right]_{r=R}.
\end{align}

Because $\sigma \neq 0$, this is not the full solution to the problem.
But note that $\sigma \propto \rho_w$ (see \eqref{eq:source}). 
This inspires the rescaling of variables
\begin{align*}
     w_{n}^* & = \exp\left(-\frac{iE^\text{rot}_j}{\hbar} t\right)u_{n}^* \\
     w_{n'} & = \exp\left(\frac{iE^\text{rot}_{j'}}{\hbar} t\right)u_{n'}.
\end{align*}
The weight density for these variables is defined as
\[ \tau_w = w_{n}^*w_{n'} \]
and
\begin{align*}
     \dot{\tau}_w &= w_{n'}\dot{w}_{n}^* + w_{n}^*\dot{w}_{n'} \\
     &= \exp\left(\frac{iE^\text{rot}_{j'}}{\hbar} t\right)u_{n'}\dot{w}_{n}^* + \exp\left(-\frac{iE^\text{rot}_j}{\hbar} t\right)u_{n}^*\dot{w}_{n'} \\
     &= \exp\left(\frac{iE^\text{rot}_{j'}}{\hbar} t\right)u_{n'}\left\{-\frac{iE^\text{rot}_{j}}{\hbar}\exp\left(-\frac{iE^\text{rot}_j}{\hbar} t\right)u_{n}^* + \exp\left(-\frac{iE^\text{rot}_j}{\hbar} t\right)\dot{u}_{n}^*\right\} \\
     & + \exp\left(-\frac{iE^\text{rot}_j}{\hbar} t\right)u_{n}^*\left\{\frac{iE^\text{rot}_{j'}}{\hbar}\exp\left(\frac{iE^\text{rot}_{j'}}{\hbar} t\right)u_{n'} + \exp\left(\frac{iE^\text{rot}_{j'}}{\hbar} t\right)\dot{u}_{n'}\right\} \\
     &= \exp\left(\frac{i(E^\text{rot}_{j'}-E^\text{rot}_{j})}{\hbar} t\right)\left[u_{n'}\left\{-\frac{iE^\text{rot}_{j}}{\hbar}u_{n}^* + \dot{u}_{n}^*\right\} + u_{n}^*\left\{\frac{iE^\text{rot}_{j'}}{\hbar}u_{n'} + \dot{u}_{n'}\right\}\right] \\
     &= \exp\left(\frac{i(E^\text{rot}_{j'}-E^\text{rot}_{j})}{\hbar} t\right)\left[\frac{i(E^\text{rot}_{j'}-E^\text{rot}_{j})}{\hbar}u_{n'}u_{n}^* + u_{n'}\dot{u}_{n}^* + u_{n}^*\dot{u}_{n'} \right] \\
     &= \exp\left(\frac{i(E^\text{rot}_{j'}-E^\text{rot}_{j})}{\hbar} t\right)\left[-\sigma - \frac{\partial j_w}{\partial r} + \sigma \right] \\
     &= -\exp\left(\frac{i(E^\text{rot}_{j'}-E^\text{rot}_{j})}{\hbar} t\right)\ \frac{\partial j_w}{\partial r}.
\end{align*}
We see that working with the $w$ variables, we have a completely source free continuity equation with the same current, at the cost of a phase factor. Note that this phase factor is independent on $r$ (which would not be the case if we didn't ignore the orbital angular momentum contribution). The phase factor has the nice property of being 1 in the diagonal case.

The integral can now be evaluated as
\begin{align}
     \nonumber
     \int_R^\infty dr\ u^*_{n}u_{n'} & = \exp\left(\frac{i(E_{j}-E_{j'})}{\hbar}t\right)\int_R^\infty dr\ w^*_{n}w_{n'} \\  
     \nonumber
                                     & = \exp\left(\frac{i(E_{j}-E_{j'})}{\hbar}t\right) \frac{i\hbar}{2m} \times \\
     \label{eq:theone}
                                     & \int_0^tdt\ \exp\left(\frac{i(E^\text{rot}_{j'}-E^\text{rot}_{j})}{\hbar} t\right)\left[u_{n'}(r)\frac{\partial}{\partial r}u^*_{n}(r) - u^*_{n}(r)\frac{\partial}{\partial r}u_{n'}(r)\right]_{r=R} \\
                                     & = \exp\left(\frac{i(E_{j}-E_{j'})}{\hbar}t\right) \frac{i\hbar}{2m} \int_0^tdt\ \left[w_{n'}(r)\frac{\partial}{\partial r}w^*_{n}(r) - w^*_{n}(r)\frac{\partial}{\partial r}w_{n'}(r)\right]_{r=R} 
     \label{eq:thetwo}
\end{align}
In practice, the second to last equation \eqref{eq:theone}
should probably be used, as it is the $u$ coefficients that are available
in the simulation. Equation \eqref{eq:thetwo} just shows
that the added phase simply translates the $u$ variables into $w$
variables in the expression for the current.

Denoting the phase
\[ P_{jj'}(t) = \exp\left(\frac{i(E_{j}-E_{j'})}{\hbar}t\right) \]
we arrive at the final expression for $\braket{A}$:
\begin{align}
     \nonumber
     \braket{A} &= \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\int_0^R dr\ u^*_{jLJN}u_{j'LJ'N} \\
                & + \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\ P_{jj'}(t)\frac{i\hbar}{2m}\int_0^tdt\ P_{j'j}(t)\left[u_{n'}(r)\frac{\partial}{\partial r}u^*_{n}(r) - u^*_{n}(r)\frac{\partial}{\partial r}u_{n'}(r)\right]_{r=R}.
\end{align}

Note that if $A = \cos^2\theta$, $A$ is a tridiagonal matrix and can be
stored very efficiently. We only need to calculate and integrate
the currents $j_w$ corresponding to the nonzero $jj'$ combinations in $A$.

Therefore, it is not going to be a difficult problem to implement this
rewrite in the program code.

In summary, we have used the continuity equation to rewrite
an integral over a very long radial range (corresponding to a 
huge amount of computer memory) to an integral in time
for a fixed radial point. This allows us to probe the real
dynamics of the molecule-atom system, and we have overcome the problem of
reflecting probability at the walls.


\end{document}

