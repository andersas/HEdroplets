\documentclass[a4paper,11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{amsmath,amssymb}

\usepackage{color,graphicx}
\usepackage[rel]{overpic}

\newcommand{\bra}[1]{\left\langle #1 \right\rvert}
\newcommand{\ket}[1]{\left\lvert #1 \right\rangle}
\newcommand{\braket}[1]{\left\langle #1 \right\rangle}

\begin{document}

\subsection*{Dealing with probabilities lost to absorbing boundaries}

The Hamiltonian of the molecule-He system is
\begin{align*}
\mathcal{H} &= \mathcal{H_\text{rot}} - \frac{\hbar^2 \nabla^2_\mathbf{r}}{2m} + V(\mathbf{r},\theta,\phi) + W(\theta,t) \\
            &= \mathcal{H_\text{rot}} - \frac{\hbar^2}{2mr}\frac{\partial^2 r}{\partial r^2}r + \frac{\hat{L}^2}{2mr^2} + V(\mathbf{r},\theta,\phi) + W(\theta,t).
\end{align*}
Here, $\theta$ and $\phi$ are the polar and azimuthal angle of the molecular axis wrt. the lab reference frame. $\mathbf{r}$ is the vector from origo
to the He atom.

Expanding the Schr\"odinger equation in the coupled angular momentum
basis \[ \ket{jLJN} = \sum C_{jmLM}^{JN}\ket{jm}\ket{LM} = \sum C_{jmLM}^{JN}Y_{jm}(\theta,\phi)Y_{LM}(\theta_\mathbf{r},\phi_\mathbf{r}) \]
and projecting it onto the $\ket{j'L'J'N'}$ state yields a system
of coupled ordinary differential equations, called the coupled channel equations, for the expansion coefficients $a_{jLJN}(r)$:
\begin{align*}
     i\hbar\dot{a}_{jLJN}(r) & = -\frac{\hbar^2}{2m}\hat{\mathcal{R}}^2a_{jLJN}(r) + \frac{\hbar^2L(L+1)}{2mr^2}a_{jLJN}(r) + Bj(j+1)a_{jLJN}(r) \\
 & + \sum_{j'L'}V_{jLJN}^{j'L'}(r)a_{j'L'JN}(r) + \sum_{j'J'} W_{jLJN}^{j'J'}a_{j'LJ'N}(r),
\end{align*}
where $\hat{\mathcal{R}}^2 = \tfrac{1}{r}\tfrac{\partial^2}{\partial r^2}r$.

Because the pulse is strong enough to produce scattering states,
i.e. states where the atom is no longer bound to the molecule,
we can get population at any $r$.

This is a problem because the state vector containing all the expansion
coefficients in principle would need to be infinitely large.
Or it would at least have to be large enough to contain
the coefficients for large enough $r$ during the finite simulation time.
Anyway, this is infeasible, and at some point we have to
neglect state far away. This is not a problem for the simulation
itself, but it is for the readout of results.
To measure the expectation value of some \textbf{molecular} observable\footnote{one that's diagonal in $r$, $L$ and $N$. If it is not diagonal in $r$, we must integrate twice over $r$.} $A$ (for example $\cos^2\theta$)
\[ \braket{A} = \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\int_0^\infty dr\ a^*_{jLJN}a_{j'LJ'N}.  \]
Throwing away components at large $r$ can be seen to remove 
weight in the summation, giving
a wrong expectation value.

To get around this, we first pick some $R$ large enough that the interaction with the molecule
can be neglected, i.e. $V(r>R) \approx 0$. Further, $R$
must be large enough that no probability amplitude
has reached $r=R$ before the pulse is over. I.e. we can set $W = 0$

We then split $\braket{A}$ up in a known part, and a part
we need to find:
\begin{equation}
\braket{A} = \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\int_0^R dr\ a^*_{jLJN}a_{j'LJ'N} + \sum_{jj'JJ'LN}A_{jLJN}^{j'J'}\int_R^\infty dr\ a^*_{jLJN}a_{j'LJ'N}.
\label{eq:expectation}
\end{equation}
The idea now is to use the continuity equation
to replace the last integral over space with an integration in time.

More concretely, if we define the weight density $\rho_w$ as the integrand
\[ \rho_w = a^*_{jLJN}(r)a_{j'LJ'N}(r), \]
the continuity equation
\begin{equation}
\frac{\partial\rho_w}{\partial t} = -\frac{\partial j_w}{\partial r}, 
\label{eq:continuity}
\end{equation}
where $j_w$ is the weight current,
allows us to rewrite the integral as
\begin{align*}
     \int_R^\infty dr\ a^*_{jLJN}a_{j'LJ'N} & = \int_R^\infty dr\ \rho_w \\
                                            & = \int_R^\infty dr\int_0^tdt\ \frac{\partial\rho_w}{\partial t} \\
                                            & = -\int_R^\infty dr\int_0^tdt\ \frac{\partial j_w}{\partial r} \\
                                            & = -\int_0^tdt\int_R^\infty dr\ \frac{\partial j_w}{\partial r} \\
                                            & = \int_0^tdt\ j_w(R)
\end{align*}
The problem is now to find an expression for $j_w(R)$.

Following the standard way of deriving the continuity equation,
we look at the time derivative of $\rho_w$
(note the simplifying index notation $n = jLJN$, $n' = j'LJ'N$):
\begin{equation}
\frac{\partial}{\partial t} a^*_{n}(r)a_{n'}(r) = \dot{a}^*_{n}(r)a_{n'}(r) + a^*_{n}(r)\dot{a}_{n'}(r)
\label{eq:dtweight}
\end{equation}

For $r > R$ the coupled\footnote{Note that they are now not so coupled anymore. The only coupling is in the radial direction through $\tfrac{\partial^2}{\partial r^2}$} channel equations and their complex conjugate simplify to
\begin{align*}
i\hbar\dot{a}_{n'}(r) &= -\frac{\hbar^2}{2m}\frac{1}{r}\frac{\partial^2}{\partial r^2}ra_{n'}(r) + E^\text{ang}_{j'L}(r) a_{n'}(r) \\
-i\hbar\dot{a}^*_{n}(r) &= -\frac{\hbar^2}{2m}\frac{1}{r}\frac{\partial^2}{\partial r^2}ra^*_{n}(r) + E^\text{ang}_{jL}(r) a^*_{n}(r)
\end{align*}
where
\[ E^\text{ang}_{jL}(r) = \frac{\hbar^2L(L+1)}{2mr^2} + Bj(j+1) \] 
Since $r>R$ is large, we also neglect the first term in $E^\text{ang}$:
\[ E^\text{ang}_{jL}(r>R) \approx E^\text{rot}_{j} = Bj(j+1) \] 
In this way, it is constant, and can be taken outside an integration.
To obtain the right hand side terms in \eqref{eq:dtweight}, multiply
the coupled channel equations with $\tfrac{-i}{\hbar}a^*_{n}(r)$
and the complex conjugate equations with $\tfrac{i}{\hbar}a_{n'}(r)$
\begin{align*}
     a^*_{n}(r)\dot{a}_{n'}(r) &= \frac{i\hbar}{2m}\frac{1}{r}a^*_{n}(r)\frac{\partial^2}{\partial r^2}ra_{n'}(r) - \frac{iE^\text{rot}_{j'}}{\hbar} a^*_{n}(r)a_{n'}(r) \\
     \dot{a}^*_{n}(r)a_{n'}(r) &= -\frac{i\hbar}{2m}\frac{1}{r}a_{n'}(r)\frac{\partial^2}{\partial r^2}ra^*_{n}(r) + \frac{iE^\text{rot}_{j}}{\hbar} a^*_{n}(r)a_{n'}(r) \\ 
\end{align*}
Plugging these expressions into \eqref{eq:dtweight} gives
\begin{align*}
\dot{\rho}_w &= \dot{a}^*_{n}(r)a_{n'}(r) + a^*_{n}(r)\dot{a}_{n'}(r) \\
             &= \frac{i\hbar}{2m}\left[\frac{a^*_{n}(r)}{r}\frac{\partial^2}{\partial r^2} (ra_{n'}(r)) - \frac{a_{n'}(r)}{r}\frac{\partial^2}{\partial r^2}(ra^*_{n}(r))\right] \\
             & + \frac{i(E^\text{rot}_{j} - E^\text{rot}_{j'})}{\hbar} \rho_w(r)
\end{align*}
Taking the right hand side to be $-\tfrac{\partial j_w}{\partial r}$ (see \eqref{eq:continuity})
and demanding that \mbox{$j_w(\infty) = 0$} we get
\begin{align*}
     j_w(R) & = \int_\infty^R \frac{\partial j_w}{\partial r} dr = \int_R^\infty -\frac{\partial j_w}{\partial r} dr \\
            & = \frac{i\hbar}{2m}\int_R^\infty dr \left[\frac{a^*_{n}(r)}{r}\frac{\partial^2}{\partial r^2} (ra_{n'}(r)) - \frac{a_{n'}(r)}{r}\frac{\partial^2}{\partial r^2}(ra^*_{n}(r))\right] \\
            & + \frac{i(E^\text{rot}_{j} - E^\text{rot}_{j'})}{\hbar} \int_R^\infty \rho_w(r) dr \\
     & = \frac{i\hbar}{2m}\left[\frac{a^*_{n}(r)}{r}\frac{\partial}{\partial r} (ra_{n'}(r)) - \frac{a_{n'}(r)}{r}\frac{\partial}{\partial r}(ra^*_{n}(r))\right]_R^\infty \\
     & - \frac{i\hbar}{2m}\int_R^\infty dr \left[\frac{\partial}{\partial r}\frac{a^*_{n}(r)}{r}\frac{\partial}{\partial r} (ra_{n'}(r)) - \frac{\partial}{\partial r}\frac{a_{n'}(r)}{r}\frac{\partial}{\partial r}(ra^*_{n}(r))\right] \\
            & + \frac{i(E^\text{rot}_{j} - E^\text{rot}_{j'})}{\hbar} \int_R^\infty \rho_w(r) dr \\
\end{align*}

In the 1D note, the left over integral from integration by parts vanish,
because the two terms are identical. 
This is not the case here, however, because of the extra factors of $r$.
We get
\begin{align*}
     \text{integral} & = \int_R^\infty dr \left[\frac{\partial}{\partial r}\frac{a^*_{n}(r)}{r}\frac{\partial}{\partial r} (ra_{n'}(r)) - \frac{\partial}{\partial r}\frac{a_{n'}(r)}{r}\frac{\partial}{\partial r}(ra^*_{n}(r))\right] \\
                     & = \int_R^\infty dr \left[\left(\frac{\partial_r a_{n}^*(r)}{r} - \frac{a_{n}^*}{r^2}\right)\left(a_{n'}(r) + r\partial_r a_{n'}(r)\right) - \left(\frac{\partial_r a_{n'}(r)}{r} - \frac{a_{n'}}{r^2}\right)\left(a_{n}^*(r) + r\partial_r a_{n}^*(r)\right)\right] \\
                     & = 2\int_R^\infty \frac{a_{n'}(r)\partial_r a_{n}^*(r) - a_{n}^*(r)\partial_r a_{n'}(r)}{r}\ dr
\end{align*}
You can further integrate one of the terms by parts
in the hope that the remaining integral will vanish.
However, when you do that, you end up with
\[ \text{integral} = 2\left(\left[\frac{a_{n'}(r)a_{n}^*(r)}{r}\right]_R^\infty  + \int_R^\infty \frac{a_{n'}(r)a_{n}^*(r)}{r^2}\ dr\right) \]

The first term is easy to evaluate, but the second term
is still annoying. It is tempting to discard it because of the $1/r^2$
dependence, starting at high $r$, but I'm not positive this is a good approximation.

You could further do the same trick with the continuity equation for that
kind of term, and get successively higher order corrections, I guess.
\end{document}

