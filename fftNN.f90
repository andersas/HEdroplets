!       sin transform, non-normalized, therefore x2y does not matter
subroutine fftNN(x2y,N,a,b)
  use conf
  use omp_lib
  implicit none
  include 'fftw3.f'
  integer, parameter :: max_thread = 100
  integer :: N ! actually, since r(0) = 0, there is an implied extra position
  complex(kind=db) :: a(N),b(N)
  real(kind=db), save :: c(NR+1,0:max_thread),cr(NR+1,0:max_thread)
  integer*8, save :: ISET(0:max_thread) = 0, plan(0:max_thread)
  character(len=3) x2y
  integer :: id
  
  id = omp_get_thread_num()
  if (id > max_thread) call quit("fftNN: Not enough plans allocated.");
  if( ISET(id) ==0 ) then
    ISET(id) = 1
    if (mod(loc(c(:,id)),16) .ne. 0 .or. mod(loc(cr(:,id)),16) .ne. 0) then
         print *, 'Warning: FFT memory not aligned!', mod(loc(c(:,id)),16), mod(loc(cr(:,id)),16)
    endif
    !$OMP CRITICAL
    call dfftw_plan_r2r_1d(plan(id),N, c(:,id), c(:,id), FFTW_RODFT00, FFTW_MEASURE)
    !$OMP END CRITICAL
  endif
  c(1:N,id) = real(a,kind=db)
!  call dfftw_execute_r2r(plan,c,c) ! fftw: sin-trafo with factor 2 !!!
  call dfftw_execute(plan(id)) ! fftw: sin-trafo with factor 2 !!!
  cr(:,id) = c(:,id)
  c(1:N,id) = imag(a)
!  call dfftw_execute_r2r(plan,c,c)
  call dfftw_execute(plan(id))
  b = cmplx(cr(1:N,id),c(1:N,id),kind=db) / sqrt(2d0*dble(N+1))
end subroutine fftNN

