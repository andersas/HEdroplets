module potentials
contains

subroutine exponentiate_imag_vmat(dtR,vmat,expvmat)
  use conf
  integer :: i,ir,info,jt,matsize
  real(kind=db), intent(in) :: vmat(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)
  real(kind=db) :: expw(imag_num_N,imag_num_N)
  real(kind=db), intent(in) :: dtR
  real(kind=db), intent(out) :: expvmat(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)
  real(kind=db) :: mat(imag_num_N,imag_num_N),w(imag_num_N),work(imag_num_N**2+10)
  expw = 0d0
  !$OMP PARALLEL DO SCHEDULE(GUIDED,4) DEFAULT(shared) PRIVATE(ir,jt,matsize,mat,i,w,work,info) &
  !$OMP& FIRSTPRIVATE(expw) COLLAPSE(2)
  do ir=1,NR
     do jt=0,imag_JTOTMAX,JTOTSTEP
        matsize = indOrder(jt)
        mat(1:matsize,1:matsize) = vmat(1:matsize,1:matsize,jt,ir)
        call DSYEV('V','U',matsize,mat,imag_num_N,w,work,imag_num_N**2+10,info)
        if(info/=0) call quit('DSYEV failed.')
        ! mat now contains orthonormal eigenvectors of vmat.
        ! w contains the eigenvalues
!do i=1,N
!write(*,'(2I5,99f15.7)') ir,i,w(i)
!enddo
        forall(i=1:matsize) expw(i,i) = exp(-dtR*w(i))
        expvmat(1:matsize,1:matsize,jt,ir) = &
        matmul(matmul( mat(1:matsize,1:matsize), expw(1:matsize,1:matsize)), transpose(mat(1:matsize,1:matsize)))
     enddo
  enddo
  !$OMP END PARALLEL DO
end subroutine exponentiate_imag_vmat
!
   
subroutine setpotmat(system,v,diag,rotmat,nexti,block_offset)
  use conf
  implicit none
  integer :: ir,j1,j2,l1,l2,la,jt
  integer(kind=ll) :: i1,i2
  real(kind=db) :: THREE0, sixJsymbol
  real(kind=pot_k) :: rotmat(num_rotmat)
  integer(kind=idx_k) :: nexti(num_rotmat)
  integer(kind=ll) :: block_offset(2,0:JTOTMAX)
  integer(kind=ll) :: i, ii
  real(kind=db) :: fac
  real(kind=db) :: diag(NR,num_N,0:JTOTMAX)
  real(kind=pot_k) :: v(NR_pot,0:LAMAX)
  real(kind=db) :: vp(NR_pot,0:LAMAX)
  real(kind=db) :: r(NR)
  character(len=*) :: system

  integer(kind=idx_k), parameter :: idx_warn = huge(int(0,kind=idx_k))-20

  if (He .eq. 1) then
     call radialpot(system,vp) ! Calculate v(r,l)
     v = real(vp,kind=pot_k)
  endif
  call dump_potential(v);

  ! Then calculate the interaction matrix in coupled basis
  r = (/( ir*DR, ir=1,NR )/)

  do jt=0,JTOTMAX,JTOTSTEP
  do j1=0,jMAX,jSTEP
  do l1=0,LMAX,LSTEP
  i1 = ind(l1,j1,jt);  if(i1<0) cycle
  forall(ir=1:NR) diag(ir,i1,jt) = H2Mred*l1*(l1+1)/r(ir)**2 + Bconst*j1*(j1+1)
  enddo
  enddo
  enddo

  if (He .eq. 0) return
  
  !  !$OMP PARALLEL DO DEFAULT(PRIVATE) SHARED(ind,diag,rotmat,v,r) COLLAPSE(3)
  i = 1
  ii = 0
  do jt=0,JTOTMAX,JTOTSTEP
  block_offset(1,jt) = i
  block_offset(2,jt) = ii
  do la = 0, LAMAX, LASTEP
  do j1=0,jMAX,jSTEP
  do l1=0,LMAX,LSTEP
  i1 = ind(l1,j1,jt);  if(i1<0) cycle
  do j2=0,jMAX,jSTEP
  do l2=0,LMAX,LSTEP
  i2 = ind(l2,j2,jt);  if(i2<0) cycle
  if (i2 .lt. i1) cycle

  ii = ii + 1
  if(la<abs(l1-l2).or.la>l1+l2.or.la<abs(j1-j2).or.la>j1+j2) cycle

     fac = (-1)**(l1+l2+jt) * &
     sqrt(dble((2*j1+1)*(2*j2+1)*(2*l1+1)*(2*l2+1) )) * &
     THREE0(l1,la,l2)*THREE0(j1,la,j2)*sixJsymbol(l1,la,l2,j2,jt,j1)

     if (abs(fac) .gt. 1e-10) then
          nexti(i) = ii
          rotmat(i) = real(fac,kind=pot_k)
          i = i + 1
          if (ii > idx_warn) call quit('You need a bigger indexing kind.')
     endif

  enddo
  enddo
  enddo
  enddo
  enddo
  enddo
  nexti(i) = 0
!  !$OMP END PARALLEL DO
end subroutine setpotmat

subroutine setpotmat_simple(system,jtmax,jtstep,maxOrd,vmat)
  use conf
  implicit none
  integer :: ir,j1,j2,l1,l2,la,jt,jtstep
  integer(kind=ll) :: i1,i2
  real(kind=db) :: THREE0, sixJsymbol
  integer :: jtmax,maxOrd
  real(kind=db) :: vmat(maxOrd,maxOrd,0:jtmax,NR)
  real(kind=db) :: fac
  real(kind=db) :: v(NR_pot,0:LAMAX)
  real(kind=db) :: vp(NR_pot,0:LAMAX)
  real(kind=db) :: r(NR)
  character(len=*) :: system

  vmat = 0

  if (He .eq. 1) then
     call radialpot(system,vp) ! Calculate v(r,l)
     v = real(vp,kind=pot_k)
  endif
  ! Then calculate the interaction matrix in coupled basis
  r = (/( ir*DR, ir=1,NR )/)

  do jt=0,jtmax,jtstep
  do j1=0,jMAX,jSTEP
  do l1=0,LMAX,LSTEP
  i1 = ind(l1,j1,jt);  if(i1<0) cycle
  forall(ir=1:NR) vmat(i1,i1,jt,ir) = H2Mred*l1*(l1+1)/r(ir)**2 + Bconst*j1*(j1+1)
  enddo
  enddo
  enddo

  if (He .eq. 0) return
  
  !$OMP PARALLEL DO DEFAULT(PRIVATE) SHARED(ind,vmat,v,r) COLLAPSE(3)
  do jt=0,jtmax,jtstep
  do la = 0, LAMAX, LASTEP
  do j1=0,jMAX,jSTEP
  do l1=0,LMAX,LSTEP
  i1 = ind(l1,j1,jt);  if(i1<0) cycle
  do j2=0,jMAX,jSTEP
  do l2=0,LMAX,LSTEP
  i2 = ind(l2,j2,jt);  if(i2<0) cycle
  if (i2 .lt. i1) cycle
  if(la<abs(l1-l2).or.la>l1+l2.or.la<abs(j1-j2).or.la>j1+j2) cycle

     fac = (-1)**(l1+l2+jt) * &
     sqrt(dble((2*j1+1)*(2*j2+1)*(2*l1+1)*(2*l2+1) )) * &
     THREE0(l1,la,l2)*THREE0(j1,la,j2)*sixJsymbol(l1,la,l2,j2,jt,j1)

     if (abs(fac) .gt. 1e-10) then
          vmat(i1,i2,jt,1:NR_pot) = vmat(i1,i2,jt,1:NR_pot) + fac*v(:,la)
          if (i1 .ne. i2) &
               vmat(i2,i1,jt,1:NR_pot) = vmat(i2,i1,jt,1:NR_pot) + fac*v(:,la)

     endif

  enddo
  enddo
  enddo
  enddo
  enddo
  enddo
  !$OMP END PARALLEL DO
end subroutine setpotmat_simple


subroutine dump_potential(v)
     use conf
     implicit none
     
     real(kind=pot_k), intent(in) :: v(NR_pot,0:LAMAX)
     integer, parameter :: num_theta = 100
     integer :: ir,ell,it
     real(kind=db) :: vr,r,theta,pl(0:LAMAX,num_theta)
          
     real(kind=db) :: plgndr

     open(97,file='potential.dat',status='replace',action='write')

     do it = 1,num_theta
     do ell = 0, LAMAX, LASTEP
               pl(ell,it) = plgndr(ell,0,cos(it*PI/num_theta))
     enddo
     enddo

     do ir = 1,NR_pot
     r = ir*DR
     do it = 1,num_theta
     theta = it*PI/num_theta
     vr = 0
          do ell = 0,LAMAX,LASTEP
               vr = vr + pl(ell,it)*v(ir,ell)
          enddo
     write(97,*) r,theta*180/PI,vr
     enddo
     write(97,*) ''
     enddo
     
     close(97)

end subroutine dump_potential

subroutine exponentiate_vmat(v,diag,rotmat,nexti,dtC1,dtC2,exp1,exp2,expdiag1,expdiag2,expind1,expind2,block1,block2)
     use conf
     implicit none
     real(kind=pot_k),intent(in) :: v(NR_pot,0:LAMAX)
     real(kind=db),intent(in) :: diag(NR,num_N,0:JTOTMAX)
     real(kind=pot_k),intent(in) :: rotmat(num_rotmat)
     integer(kind=idx_k),intent(in) :: nexti(num_rotmat)
     complex(kind=db), intent(in) :: dtC1, dtC2
     complex(kind=exp_k), intent(out) :: exp1(num_expvmat),exp2(num_expvmat)
     complex(kind=db),intent(out),dimension(NR,num_N,0:JTOTMAX):: expdiag1,expdiag2
     integer(kind=idx_k), intent(out) :: expind1(num_expvmat), expind2(num_expvmat)
     integer(kind=ll), dimension(2,0:JTOTMAX), intent(out) :: block1,block2
     real(kind=db) :: vmat_jt(num_N,num_N,NR_pot)
     complex(kind=db), dimension(num_N,num_N,NR_pot) :: expvmat1,expvmat2
     integer(kind=idx_k), parameter :: idx_warn = huge(int(0,kind=idx_k))-20
     integer(kind=ll) :: i, ii, ei1, eii1, ei2, eii2
     integer :: ir, jt, la
     integer(kind=ll) :: i1,i2

     expdiag1 = exp(-dtC1*diag)
     expdiag2 = exp(-dtC2*diag)
     
     if (He .eq. 0) return

     ei1 = 1
     ei2 = 1
     eii1 = 0
     eii2 = 0

     i = 1
     ii = 0
     do jt=0,JTOTMAX,JTOTSTEP
     block1(1,jt) = ei1
     block1(2,jt) = eii1
     block2(1,jt) = ei2
     block2(2,jt) = eii2
     vmat_jt = 0
laloop:do la = 0, LAMAX, LASTEP
     do i1 = 1,indOrder(jt)
     do i2 = i1,indOrder(jt)
     ii = ii+1
     if (nexti(i) .ne. ii) cycle
          vmat_jt(i2,i1,:) = vmat_jt(i2,i1,:) + rotmat(i)*v(:,la)
          if (i1.ne.i2) &
               vmat_jt(i1,i2,:) = vmat_jt(i1,i2,:) + rotmat(i)*v(:,la)
          i = i + 1
          if (nexti(i) .eq. 0) exit laloop
     enddo
     enddo
     end do laloop
     
     ! must be outside previous loop. The diagonal is not guaranteed
     ! to be populated.
     do i1 = 1,indOrder(jt)
          vmat_jt(i1,i1,:) = vmat_jt(i1,i1,:) + diag(1:NR_pot,i1,jt)
     enddo

     call exponentiate_vmat_jt(indOrder(jt), vmat_jt, dtC1,dtC2, expvmat1, expvmat2)
     do i1 = 1,indOrder(jt)
     do i2 = i1,indOrder(jt)
     do ir = 1,NR_pot
     eii1 = eii1+1
     eii2 = eii2+1
     
          if (abs(expvmat1(i2,i1,ir)) .gt. 1e-10) then
               expind1(ei1) = eii1
               exp1(ei1) = expvmat1(i2,i1,ir)
               ei1 = ei1 + 1
               if (eii1 > idx_warn) call quit('You need a bigger indexing kind.')
          endif
          if (abs(expvmat2(i2,i1,ir)) .gt. 1e-10) then
               expind2(ei2) = eii2
               exp2(ei2) = expvmat2(i2,i1,ir)
               ei2 = ei2 + 1
               if (eii2 > idx_warn) call quit('You need a bigger indexing kind.')
          endif
     enddo
     enddo
     enddo

     enddo

     expind1(ei1) = 0
     expind2(ei2) = 0

end subroutine exponentiate_vmat

subroutine exponentiate_vmat_jt(matsize, vmat_jt,dtC1,dtC2,expvmat1,expvmat2)
     use conf
     implicit none
     integer, intent(in) :: matsize
     complex(kind=db),intent(in)  :: dtC1, dtC2
     real(kind=db),intent(in) :: vmat_jt(num_N,num_N,NR_pot)
     complex(kind=db), dimension(num_N,num_N,NR_pot) :: expvmat1,expvmat2

     complex(kind=db) :: expw(num_N,num_N)
     real(kind=db) :: mat(num_N,num_N),w(num_N),work(num_N**2+10)
     integer :: ir, i, info

     expw = 0d0
     !$OMP PARALLEL DO DEFAULT(shared) PRIVATE(ir,mat,i,w,work,info) &
     !$OMP& FIRSTPRIVATE(expw)
     do ir=1,NR_pot
          mat(1:matsize,1:matsize) = vmat_jt(1:matsize,1:matsize,ir)
          call DSYEV('V','U',matsize,mat,num_N,w,work,num_N**2+10,info)
          if(info/=0) call quit('DSYEV failed.')
          ! mat now contains orthonormal eigenvectors of vmat.
          ! w contains the eigenvalues
          
        forall(i=1:matsize) expw(i,i) = exp(-dtC1*w(i))
        expvmat1(1:matsize,1:matsize,ir) = &
        matmul(matmul( mat(1:matsize,1:matsize), expw(1:matsize,1:matsize)), transpose(mat(1:matsize,1:matsize)))

        forall(i=1:matsize) expw(i,i) = exp(-dtC2*w(i))
        expvmat2(1:matsize,1:matsize,ir) = &
        matmul(matmul( mat(1:matsize,1:matsize), expw(1:matsize,1:matsize)), transpose(mat(1:matsize,1:matsize)))
     enddo
     !$OMP END PARALLEL DO    

end subroutine exponentiate_vmat_jt



subroutine radialpot(system,v)
  use conf
  implicit none
  real(kind=db) :: v(NR_pot,0:LAMAX)
  character(len=*) :: system
  integer :: la,r 

     ! LASTEP and LAMAX must be compatible with the vlam files
  if (system .eq. "CS2-He") then
     call get_vlam_potential(v,'cs2he.vlam') 
  else if (system .eq. "HCCH-He") then
     call get_vlam_potential(v,'hcch_he.vlam')
  else if (system .eq. "I2-He") then
     call get_vlam_potential(v,'I2_he.vlam')
  else if (system .eq. "HCCH-He-conv-I2") then
     call get_vlam_potential(v,'hcch_he.vlam')
     ! Scale HCCH potential
     v = v*43.52/25.1 ! max(v) = 25.1, max(v_I2-he) = 43.52 cm^-1
     ! and translate it ~1 Å (roughly 10 grid cells)
     do la = 0,LAMAX
     do r = 10,NR_pot
          v(NR_pot-r+10,la) = v(NR_pot-r+1,la)
     enddo
     enddo
     do la = 0,LAMAX
     do r = 1,10
          v(r,la) = v(11,la)
     enddo
     enddo
      
  else if (system .eq. "morse") then
       write(*,*) 'Potential not recognized:', system 
       stop
  else
       write(*,*) 'Potential not recognized:', system 
       stop
  end if 

end subroutine radialpot

subroutine get_vlam_potential(v,filename)
     use conf
     implicit none
     real(kind=db), intent(out) :: v(NR_pot,0:LAMAX)
     character(len=*), intent(in) :: filename
     real(kind=db) :: r(NR_pot)
     ! primed (p) variables refer to parameters in the cs2he.vlam file
     integer :: NRp, LAMAXp, LASTEPp,nlp
     real(kind=db), allocatable :: rp(:),ya(:,:),y2a(:,:),yvec(:),p(:)
     integer :: i,ir,l,ii
     real(kind=db) :: rr,rrr
     r = (/( ir*DR, ir=1,NR_pot )/)
     
     open(4210,file=filename,status='old',err=99)
     read(4210,*) NRp,LAMAXp,LASTEPp,nlp
     if(LAMAXp/=LAMAX.or.LASTEPp/=LASTEP) goto 98
     allocate(rp(NRp),ya(NRp,nlp),y2a(NRp,nlp),yvec(nlp),p(LAMAX+1))
     do i=1,nlp
        do ir=1,NRp
          read(4210,*)ii,rp(ir),ya(ir,i),y2a(ir,i)
        enddo
     enddo
     close(4210)

     do ir=1,NR_pot
          rr=r(ir)
          rrr=min(max(rp(1),rr),rp(NRp))
          call splintvec(rp,ya,y2a,NRp,rrr,yvec,nlp)
          i=0
          do l=0,LAMAX,LASTEP
               i=i+1
               v(ir,l)=yvec(i)
          enddo
     enddo
     return
!
99 write(*,*)'get_vlam_potential()::file ',filename,' not found.';stop
98 write(*,*)'get_vlam_potential()::LMAX/=LAMAX.or.LSTEP/=LASTEP';stop
end subroutine get_vlam_potential


subroutine splintvec(xa,ya,y2a,n,x,y,nlam)
      use conf
      implicit none
      integer n,nlam,k,klo,khi,j
      real(kind=db) xa(n),ya(n,nlam),y2a(n,nlam),y(nlam),x,h,a,b
      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.d0) call quit('bad xa input.')
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      do j=1,nlam
         y(j) = a*ya(klo,j)+b*ya(khi,j)+ &
               ((a**3-a)*y2a(klo,j)+(b**3-b)*y2a(khi,j))*(h**2)/6.d0
      enddo
      return
end

subroutine setUmat(U,Unexti,Ublock_offset,N)
     ! Calculate the matrix representation of cos^2 theta
     ! in the coupled basis
     use conf
     implicit none

     real(kind=lpot_k), intent(out) :: U(num_Umat) ! the matrix
     integer(kind=idx_k) :: Unexti(num_Umat) ! 
     integer(kind=ll) :: Ublock_offset(2,0:LMAX) ! 
     integer(kind=ll) :: Ui, Uii
     integer, intent(in) :: N
     integer :: j1, jt1, j2, jt2, l, jmin
     integer(kind=ll) :: Ui1, Ui2
     integer :: m
     real(kind=db) :: THREE0, THREE
     integer :: delta
     real(kind=db) :: prefac, fac, j1j22000, sqrtsqrt

     integer(kind=idx_k), parameter :: idx_warn = huge(int(0,kind=idx_k)) - 20

!     !$OMP PARALLEL DO SCHEDULE(GUIDED,4) DEFAULT(PRIVATE) SHARED(U,N,Uind) COLLAPSE(2)
     Ui = 1
     Uii = 0
     do l = 0, LMAX, LSTEP
     Ublock_offset(1,l) = Ui
     Ublock_offset(2,l) = Uii
     do jt2 = 0, JTOTMAX, JTOTSTEP
     do j2 = 0, jMAX, jSTEP
     Ui2 = Uind(j2,jt2,l); if (Ui2<0) cycle
     do jt1 = 0, JTOTMAX, JTOTSTEP
     !do j1 = max(j2-2,0),min(j2+2,jMAX),2
     do j1 = 0, jMAX, jSTEP
     Ui1 = Uind(j1,jt1,l); if (Ui1 < 0) cycle
     
     if (Ui1 .lt. Ui2) cycle

          Uii = Uii+1
                                    ! check for j1+j2 even
          if (abs(j2-j1) .gt. 2 .or. IAND(j1+j2,1) .ne. 0) cycle

          jmin = min(j1,j2)

          prefac = sqrt(dble((2*jt1+1)*(2*jt2+1)))
          j1j22000 = THREE0(j1,j2,2) ! j1+j2 must be even here
          sqrtsqrt = sqrt(dble((2*j1+1)*(2*j2+1)))

          fac = 0
          do m = -jmin,jmin
               if (abs(N-m) .gt. l) cycle
               fac = fac + (2*(-1)**m*sqrtsqrt*j1j22000*three(j1,j2,2,m,-m,0) +&
               delta(j1,j2))*three(j1,l,jt1,m,N-m,-N)*three(j2,l,jt2,m,N-m,-N)
          enddo

          fac = prefac*fac/3

          if (abs(fac) .gt. 1e-10 .or. Ui1 .eq. Ui2) then
               Unexti(Ui) = Uii
               U(Ui) = real(fac,kind=lpot_k)
               Ui = Ui + 1
               if (Ui > idx_warn) call quit('You need a bigger indexing kind.')
          endif
     enddo
     enddo
     enddo
     enddo
     enddo

     Unexti(Ui) = 0

!     !$OMP END PARALLEL DO
     print *, Ui

end subroutine setUmat

subroutine setWmat(W,U,Unexti,N)
     use conf
     implicit none
     real(kind=lpot_k), dimension(num_Umat)  :: W,U
     integer(kind=idx_k) :: Unexti(num_Umat)
     integer(kind=ll) :: Ui, Uii
     integer, intent(in) :: N
     intent(in) :: U
     intent(out) :: W
     real(kind=db) :: THREE
     integer :: j1, jt1, j2, jt2, l, jmin
     integer :: m
     integer(kind=ll) :: Ui1, Ui2 
     real(kind=db) :: prefac, fac

!     W = -(polarizability_anisotropy*U+polarizability_perp*I)/4
!     W = -polarizability_anisotropy*U/4
     
 !    !$OMP PARALLEL DO SCHEDULE(GUIDED,4) DEFAULT(PRIVATE) SHARED(W,U,N,Uind) COLLAPSE(3)
     Ui = 1
     Uii = 0
     do l = 0, LMAX, LSTEP
     do jt2 = 0, JTOTMAX, JTOTSTEP
     do j2 = 0, jMAX, jSTEP
     Ui2 = Uind(j2,jt2,l); if (Ui2<0) cycle
     do jt1 = 0, JTOTMAX, JTOTSTEP
     do j1 = 0, jMAX, jSTEP
     Ui1 = Uind(j1,jt1,l); if (Ui1 < 0) cycle
     
     if (Ui1 .lt. Ui2) cycle
 
          Uii = Uii + 1
          if (Unexti(Ui) .ne. Uii) cycle

          W(Ui) = -polarizability_anisotropy*U(Ui)/4

          ! Transform the diagonal alpha_perp term to coupled basis
          if (Ui1 .eq. Ui2) then

               jmin = min(j1,j2)
               ! (maybe) TODO: This selection rule is wrong for negative N
               if (max(-jmin,-l-N) > min(jmin,l+N)) cycle

               prefac = sqrt(dble((2*jt1+1)*(2*jt2+1)))

               fac = 0
               do m = -jmin, jmin
                    if (abs(N-m) .gt. l) cycle
                    fac = fac + three(j1,l,jt1,m,N-m,-N)*three(j2,l,jt2,m,N-m,-N)
               enddo
               !if (abs(fac/(2*j1+1)-1) .gt. 1e-6) print *, 'fac != 1/2j+1', fac, j1
               W(Ui) = W(Ui) - real(polarizability_perp*prefac*fac/4,kind=lpot_k)
          endif

          Ui = Ui+1
          if (Unexti(Ui) .eq. 0) return

     enddo
     enddo
     enddo
     enddo
     enddo

!     !$OMP END PARALLEL DO

end subroutine setWmat

end module potentials

function delta(a,b)

     integer :: delta,a,b

     if (a .eq. b) then
          delta = 1
     else
          delta = 0
     endif

end function delta


