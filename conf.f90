module conf
   use iso_c_binding
   implicit none 

   ! Do a gas phase calculation? Set this to 0
   integer, parameter :: He = 1

   logical, parameter :: DEBUG = .false. ! Print out debug and timing info
   logical, parameter :: emit_distributions = .true.

   ! Energy measured in kelvin (i.e. in k_b units)
   ! distance in Ångstrøm
   ! => 1 time unit = hbar/k_b ~ 7.6 ps 
   ! => Velocity measured in Å*k_b/hbar ~ 13 m/s
   ! mass:  ((hbar)^2/(10^-20*boltzmann constant))/(1 atomic mass unit)

   integer, parameter :: ll = selected_int_kind(18)
   integer, parameter :: db = selected_real_kind(p=15,r=307)
   integer, parameter :: sg = selected_real_kind(p=6,r=37)
   ! The kind used by exponential of matrices
   integer, parameter :: exp_k = sg
   ! kind used by potential matrix
   integer, parameter :: pot_k = sg
   ! kind used by laser potential matrix
   integer, parameter :: lpot_k = sg

   !integer, parameter :: idx_k = selected_int_kind(18)
   integer, parameter :: idx_k = selected_int_kind(9)

   ! Options for matrix folder:
   ! "": current folder. Nothing special.
   ! "/dev/shm/": shared memory. Guaranteed not to hit disk (unless you swap).
   ! "/ramdisk/": shared memory you allocate (in /etc/fstab)
   !              as tmpfs. Better control over ramdisk size.
   ! "/path/to/big/ssd_backed/harddrive/": fast swap?
   character(len=*), parameter :: matrix_folder = "/dev/shm/"
   !character(len=*), parameter :: matrix_folder = "/home/femtolab/"
   !character(len=*), parameter :: matrix_folder = "/home/anders/data/big/"
   !character(len=*), parameter :: matrix_folder = "/home/anders/data/gas/"
   !character(len=*), parameter :: matrix_folder = "/home/anders/data/big_test/"
   !character(len=*), parameter :: matrix_folder = "/unsafe/tmp/big/"

   ! Dont put the status.dat file on a network drive!
   character(len=*), parameter :: statusfile = matrix_folder // 'status.dat'
   integer, parameter :: num_status_elements = 2
   character(len=*), parameter :: &
        ensemble_file = matrix_folder // "ensemble.dat", &
        vmat_file = matrix_folder // "vmat.dat", &
        vdiag_file = matrix_folder // "vmat_diag.dat", &
        rotmat_file = matrix_folder // "rotmat.dat", &
        nexti_file = matrix_folder // "nexti.dat", &
        vblock_file = matrix_folder // "vblock.dat", &
        expV_p_file = matrix_folder // "expV_pulse.dat", &
        expV_file = matrix_folder // "expV.dat", &
        expVdiag_p_file = matrix_folder // "expVdiag_pulse.dat", &
        expVdiag_file = matrix_folder // "expVdiag.dat", &
        expV_p_ind_file = matrix_folder // "expV_pulse_index.dat", &
        expV_ind_file = matrix_folder // "expV_index.dat", &
        expV_offset_file = matrix_folder // "expV_offset.dat", &
        expV_offset_p_file = matrix_folder // "expV_offset_pulse.dat"




   real(kind=db), parameter :: PI = 3.14159265358979323d0
   real(kind=db), parameter :: one_time_unit_in_ps = 7.6382_db
   real(kind=db), parameter :: picosecond = 1.0_db/one_time_unit_in_ps
   real(kind=db), parameter :: meter = 1d10
   real(kind=db), parameter :: k_b = 1.3806488d-23 ! J/K

   ! In Ångstrøm/7.6 ps 3e8 m/s * 10^10 Å/m * 10^-12 s/ps * / (7.63 tu/ps)
   real(kind=db), parameter :: speed_of_light = 299792458.0*1e-2*picosecond

   real(kind=db), parameter :: DT = 0.01*picosecond ! time step measured in inverse kelvin (~7.6 ps)
   real(kind=db), parameter :: DR = 0.14d0 ! radial step size seasured in Ångstrøm 
   integer, parameter :: NR = 127*HE + (1-HE) !63 !(power of two - 1) number of radial steps
   integer, parameter :: NR_pot = 74*He

   logical, parameter :: do_absorption = .true. ! Don't absorb
   integer, parameter :: absorption_zone = 32 ! Absorbing boundary thickness
                                              ! in number of radial grid points
   integer, parameter :: probability_current_wall = NR - absorption_zone - 5;

   real(kind=db), parameter :: DK = 2d0*PI/(2d0*(NR+1)*DR) ! step size in fourier space

   ! Number of time steps in imaginary time (1) and real time (2)
   ! NTblock is the number of steps to do between renormalizations
   ! (speeds up the second order method, but must be fairly low)
   integer, parameter :: NT1 = 3000, NT2 = 0, NTblock = 1
   real(kind=db), parameter :: DT_imag = 0.01*picosecond
   integer, parameter :: NTblock_imag = 1

   ! In the uncoupled basis, j and L is the molecule and He orbital momentum
   ! quantum numbers. JTOT = j + L.
   ! LA means \ell in the notes (for the potential expansion)
   !integer, parameter :: LMAX = 48*He, LSTEP=1, jMAX=200, jSTEP=2, LAMAX=16
   !integer, parameter :: LASTEP=2, JTOTMAX=200, JTOTSTEP=1
   !integer, parameter :: LMAX = 14*He, LSTEP=1, jMAX=32, jSTEP=1, LAMAX=16
   !integer, parameter :: LASTEP=2, JTOTMAX=32, JTOTSTEP=1*He+(1-He)*jSTEP
   ! CS2:
   !integer, parameter :: LMAX = 32*He, LSTEP=1, jMAX=32, jSTEP=2, LAMAX=16
   !integer, parameter :: LASTEP=2, JTOTMAX=32, JTOTSTEP=1

   ! I2:
   integer, parameter :: LMAX = 32*He, LSTEP=1, jMAX=200, jSTEP=1, LAMAX=24
   integer, parameter :: LASTEP=2, JTOTMAX=200, JTOTSTEP=1
   
   !integer, parameter :: LMAX = 16*He, LSTEP=1, jMAX=16, jSTEP=2, LAMAX=16
   !integer, parameter :: LASTEP=2, JTOTMAX=16, JTOTSTEP=1
   ! Don't need that big a basis when finding the initial thermal ensemble:
   !integer, parameter :: LMAX = 16*He, LSTEP=1, jMAX=16, jSTEP=1, LAMAX=16
   !integer, parameter :: LASTEP=2, JTOTMAX=16, JTOTSTEP=1*He+(1-He)*jSTEP
   !integer, parameter :: imag_JTOTMAX = 8 ! For HCCH, all bound states have jt<6
   !integer, parameter :: imag_JTOTMAX = 16 ! For CS2, all bound states have jt<16
   !integer, parameter :: imag_JTOTMAX = 27 ! For pseudo I2, all bound states have jt<27
   integer, parameter :: imag_JTOTMAX = 24 ! For I2, all bound states have jt<24

   !character(len=*), parameter :: molecule = "CS2-He"
   !character(len=*), parameter :: molecule = "HCCH-He"
   !character(len=*), parameter :: molecule = "HCCH-He-conv-I2"
   character(len=*), parameter :: molecule = "I2-He"

   ! hbar^2/(2*m) for the molecule, He, and for the reduced mass
   ! The unit of H2M is Å^2/(hbar/kb)
   ! From SI units, H2Mxx = hbar^2/(2m) / K_b * 1e20
   ! Bconst given in kelvin (hbar * 2pi * frequency / k_b)
   ! Bconst conversion factor from GHz: 
   real(kind=db), parameter :: H2Mhe=6.059615d0
   ! CS2:
   ! real(kind=db), parameter :: Bconst=0.15684d0, H2MMol=0.3189d0
   ! should be H2Mol = 0.3185 according to wolfram alpha
   !
   ! HCCH:
    !real(kind=db), parameter :: Bconst=1.6931d0, H2MMol=0.93246d0
    !NIST:
   !real(kind=db), parameter :: Bconst=1.69286d0, H2MMol=0.93152d0
   ! OCS:
   ! real(kind=db), parameter :: Bconst=0.2918d0, H2MMol=0.4038d0
   
   ! I2: mass = 253.809 g/mol
   real(kind=db), parameter :: Bconst=5.38d-2, H2MMol=4.77807d-2

   real(kind=db), parameter :: H2Mred = (H2Mmol + H2MHe)*He

   ! polarizability volumes in Å^3 parallel and perpendicular to the principal
   ! axis. polarizability volume = 1/4pi\epsilon_0 * polarizability
   ! CS2:
   ! real(kind=lpot_k), parameter :: polarizability_parallel=15.6_lpot_k, polarizability_perp=5.3_lpot_k
   ! OCS:
   ! real(kind=db) :: polarizability_parallel=50.72, polarizability_perp=26.15
   ! HCCH (B3LYP/aug-cc-pVTZ gave 0.002 Å^2 difference for mean value)
   ! Other results varied by as much as 0.08 Å^2
   !real(kind=lpot_k), parameter :: polarizability_parallel=4.675, polarizability_perp=2.889
   
   ! I2: J. Phys. Chem. A 1997, 101, 953-956 Electrooptical Properties and Molecular Polarization of Iodine, I 2
   real(kind=lpot_k), parameter :: polarizability_parallel=14.562, polarizability_perp=8.4627

   real(kind=lpot_k), parameter :: polarizability_anisotropy = polarizability_parallel-polarizability_perp
   ! Imax: peak pulse intensity in W/m^2
   ! waists in meter.
   real(kind=db), parameter :: pulse_Imax_SI=5e13*1e4, &
        !pulse_FWHM=0.30*picosecond, kick_waist=40d-06, probe_waist=40d-6
        pulse_FWHM=0.30*picosecond, kick_waist=35d-06, probe_waist=25d-6
   ! J/sm^2 / kb J/K * 10^-20 m^2/Å^2 * 10^-12 s/ps * 1/7.6 ps/tu
   real(kind=db), parameter :: pulse_Imax = pulse_Imax_SI/k_b*1d-32*picosecond
   real(kind=db), parameter :: pulse_sigma = pulse_FWHM/(2*sqrt(2.0_db*log(2.0_db)))
   integer, parameter :: NT_pulse = 3*150, NTblock_pulse = 1
   real(kind=db), parameter :: DT_pulse = 3*pulse_FWHM/NT_pulse

   ! How many terms to include in the calculation of the exponential
   ! of the time dependent laser interaction potential each time step:
   integer, parameter :: pulse_expW_truncation = 10
   integer, parameter :: krylov_subspace_size_W = 10
   integer, parameter :: krylov_subspace_size_V = 35
   integer, parameter :: krylov_subspace_size_V_after_pulse = 35
   integer, parameter :: krylov_subspace_size_UW = 35
   integer, parameter :: krylov_UW_num_steps = 1

   integer, parameter :: vmat_num_krylov_steps_after_pulse = 1
   integer, parameter :: vmat_num_krylov_steps_pulse = 1
   integer(kind=ll) :: num_calls

   integer, parameter :: focal_volume_averaging_shells = 1

   integer(kind=ll) :: ind(0:LMAX,0:jMAX,0:JTOTMAX)
   integer :: indOrder(0:JTOTMAX) ! Size of L,j matrix given jt
   integer :: num_N, imag_num_N
   integer(kind=ll) :: Uind(0:jMAX,0:JTOTMAX,0:LMAX)
   integer :: UindOrder(0:LMAX)
   integer :: num_jJ
   integer(kind=ll) :: num_rotmat, num_expvmat, num_Umat

   ! For the initial thermal ensemble
   ! Ground state: -30.8 K. At 0.37 K a state with E=-25K
   ! is 6 million times less likely to occur than the ground state.
   real(kind=db), parameter :: thermal_energy_threshold = 0*(-25)*He+200*(1-He)
   integer, parameter :: num_trial_states = 60*He+(1-He)
   real(kind=db), parameter :: temperature = 0! 0.37
   !real(kind=db), parameter :: even_abundance=1,odd_abundance=sqrt(6.25)
   real(kind=db), parameter :: even_abundance=1,odd_abundance=1
   real(kind=db), parameter :: fraction_partition_function = 0.99
 
   logical, parameter :: calculate_bound_state_overlap = He .eq. 1 .and. &
                         thermal_energy_threshold .ge. 0

   logical, parameter :: parallelize_propagation = .false. !&
   !     temperature .eq. 0 .and. focal_volume_averaging_shells .eq. 1

   ! Set to true if you want to keep calculated matrices between runs.
   ! Be careful about reusing them if you change other parameters.
   ! hint: delete status.dat to force recalculation.
   !
   ! Think before playing around with this one:
   logical, parameter :: keep_bigmats = .true. ! .false.

   integer, parameter :: num_pulse_samples = NT_pulse/NTblock_pulse
   integer, parameter :: num_fieldfree_samples = NT2/NTblock
   integer, parameter :: cos2_num_samples = num_fieldfree_samples + num_pulse_samples
 
   type Upointer ! Need these types to have arrays of pointers
        real(kind=lpot_k), dimension(:), contiguous, pointer :: p
        integer(kind=idx_k), dimension(:), contiguous, pointer :: Unexti
        integer(kind=ll), contiguous, pointer :: Ublock_offset(:,:)
   end type Upointer
  ! type eigvalpointer
  !      real(kind=db), dimension(:,:), contiguous, pointer :: p
  ! end type eigvalpointer

   ! Message tags
   
   integer, parameter :: MPI_work_request = 1
   integer, parameter :: MPI_result = 2
   integer, parameter :: MPI_work_packet = 3

   ! Misc

   integer(kind=C_SIZE_T) :: status_size, vmat_real_size, expvmat_cplx_size, Umat_real_size, Umat_cplx_size

   !  type expvmat_container
   !       ! Handles:
   !       type(c_ptr) :: expvmat_h, expvmat_pulse_h, expvmatHalf_pulse_h
   !       ! Pointers (C type, can't be used from fortran without first calling
   !       ! C_F_POINTER)
   !       type(c_ptr) :: expvmat_p, expvmat_pulse_p, expvmatHalf_pulse_p
   !       complex(kind=exp_k), dimension(:,:,:,:), &
   !       contiguous, pointer :: expvmat, expvmat_pulse, expvmatHalf_pulse
   !  end type expvmat_container


   type vmat_container
     ! Handles:
     type(c_ptr) :: v_h,diag_h,rotmat_h,nexti_h,block_h,expV_pulse_h,expV_h
     type(c_ptr) :: expdiag_pulse_h, expdiag_h, expind_pulse_h,expind_h
     type(c_ptr) :: expblock_pulse_h, expblock_h
     ! Pointers (C type, can't be used from fortran without first calling
     ! C_F_POINTER)
     type(c_ptr) :: v_p,diag_p,rotmat_p,nexti_p,block_p,expV_pulse_p,expV_p
     type(c_ptr) :: expdiag_pulse_p, expdiag_p, expind_pulse_p,expind_p
     type(c_ptr) :: expblock_pulse_p, expblock_p
     real(kind=pot_k), contiguous, pointer :: rotmat(:), v(:,:)
     real(kind=db), contiguous, pointer :: diag(:,:,:)
     integer(kind=idx_k), contiguous, pointer :: nexti(:)
     integer(kind=ll), contiguous, pointer :: block_offset(:,:)
     
     complex(kind=exp_k), contiguous, pointer :: expV_pulse(:), expV(:)
     complex(kind=db), contiguous, pointer :: expdiag_pulse(:,:,:), expdiag(:,:,:)
     integer(kind=idx_k), contiguous, pointer :: expind_pulse(:),expind(:)
     integer(kind=ll), contiguous, pointer :: expblock_pulse(:,:), expblock(:,:)

   end type vmat_container


contains
subroutine conf_init()

     implicit none
     integer :: l, i, j, jtot

     if (He .ne. 1 .and. He .ne. 0) call quit('He parameter must be 0 or 1.')

     print *, 'dt=',DT/picosecond,'ps', DT
     print *, 'sampling dt=',DT*NTblock/picosecond,'ps'
     print *, 'pulse dt=',DT_pulse/picosecond,'ps'
     print *, 'pulse sampling dt=',DT_pulse*NTblock_pulse/picosecond,'ps'

     ind = -1; ! ind(l,j,jtot) gives the 1d index in a 2d L,j array
     indOrder = -1 ! indOrder(jtot) is the order of the 2d L,j matrix
     do jtot=0,JTOTMAX,JTOTSTEP
          i = 0
          do j = 0, jMAX,jSTEP
               do l = 0, LMAX, LSTEP
                    if (abs(l-j)>jtot .or. l+j<jtot) cycle
                    i = i + 1
                    ind(l,j,jtot) = i
               enddo
          enddo
          indOrder(jtot) = i
     enddo

     num_N = maxval(indOrder)
     imag_num_N = maxval(indOrder(0:imag_JTOTMAX))
     print *, num_N, (LMAX+1)*(jMAX+1), num_N/dble((LMAX+1)*(jMAX+1)), imag_num_N

     Uind = -1
     UindOrder = -1
     do l = 0, LMAX, LSTEP
          i = 0
          do jtot = 0, JTOTMAX, JTOTSTEP
               do j = 0, jMAX, jSTEP
                    if (abs(j-l) > jtot .or. j+l < jtot) cycle
                    i = i + 1
                    Uind(j,jtot,l) = i
               enddo
          enddo
          UindOrder(l) = i
     enddo
     
     num_jJ = maxval(UindOrder)
     print *, num_jJ, (jMAX+1)*(JTOTMAX+1), num_jJ/dble((jMAX+1)*(JTOTMAX+1))


     status_size = num_status_elements*STORAGE_SIZE(int(0))/8
     vmat_real_size = STORAGE_SIZE(real(0,kind=db))/8
     expvmat_cplx_size = STORAGE_SIZE(cmplx(0,0,kind=exp_k))/8
     Umat_cplx_size = STORAGE_SIZE(cmplx(0,0,kind=db))/8
     Umat_real_size = STORAGE_SIZE(1.0_db)/8

     expvmat_cplx_size = INT(expvmat_cplx_size,C_SIZE_T) * INT(max(1,NR_pot),C_SIZE_T) * &
                      INT(num_N,C_SIZE_T)**2 * INT(JTOTMAX+1)

     vmat_real_size = INT(vmat_real_size,C_SIZE_T) * INT(NR,C_SIZE_T) * &
                      INT(num_N,C_SIZE_T)**2 * INT(JTOTMAX+1)

     Umat_real_size = INT(Umat_real_size,C_SIZE_T)*3*INT(jMAX+1,C_SIZE_T) * &
                      INT(JTOTMAX+1,C_SIZE_T)**2*INT(LMAX+1,C_SIZE_T)

     Umat_cplx_size = INT(Umat_cplx_size,C_SIZE_T)*3*INT(jMAX+1,C_SIZE_T) * &
                      INT(JTOTMAX+1,C_SIZE_T)**2*INT(LMAX+1,C_SIZE_T)


     num_rotmat = int(JTOTMAX+1,kind=ll)*int(num_N,kind=ll)**2*(LAMAX/LASTEP+1) 
     num_expvmat = INT(NR_pot,C_SIZE_T)*INT(num_N,C_SIZE_T)**2 * INT(JTOTMAX+1)
     num_Umat = 3_ll*int(jMAX+1,kind=ll)*int(JTOTMAX+1,kind=ll)**2*int(LMAX+1,kind=ll)

     print *, 'expvmat (complex):', (expvmat_cplx_size),'bytes'
     print *, 'Vmat    (  real ):', (vmat_real_size),'bytes'
     print *, 'Umat    (complex):', (Umat_cplx_size),'bytes'
     print *, 'Umat    (  real ):', (Umat_real_size),'bytes'

end subroutine conf_init


end module conf

