c-------------------------------------------------------------------------------

      subroutine fran_heocspot(r,cth,v,ipot)

c He-OCS potential from Legendre expansion
c V(R,theta)=Sum_l V_l(R)P_l(cos(theta)) : Courtesy of Francesco.
c
c if ipot=0 --> the potential is the HHDSD (DFT) potential
c if ipot=1 --> the potential is the Higgins potential
c 
c INPUT: r in Angstrom, cth=cos(theta), ipot
c OUTPUT: v in Kelvin

      implicit real*8(a-h,o-z)
      integer npun,nlam,maxy,icall,ipot,i,j
      parameter (npun=251,nlam=37,maxy=10000)
c to convert the potential unit to Kelvin from cm^-1
      parameter (vscale=1.4388324d0)
      dimension xa(npun,nlam),ya(npun,nlam),y2a(npun,nlam)
      dimension p(nlam+1)
      dimension y(nlam)
      save icall,xa,ya,y2a
      data icall /0/
      
c   ------ initialization done only in first call
      if(icall.eq.0) then

       open(unit=11,file='hhdsdpot.vlam',status='old')
       open(unit=22,file='higginspot.vlam',status='old')

       yp1=1.0d35  
       ypn=.0d0 
       if(ipot.eq.0) then
        do j=1,nlam
          do i=1,npun
           read(11,*) xa(i,j),ya(i,j)
          end do
          yp1=(ya(2,j)-ya(1,j))/(xa(2,j)-xa(1,j))
          call fran_spline(xa(1,j),ya(1,j),npun,yp1,ypn,y2a(1,j))
        end do
        else 
         do j=1,nlam
          do i=1,npun
           read(22,*) xa(i,j),ya(i,j)
          end do
          yp1=(ya(2,j)-ya(1,j))/(xa(2,j)-xa(1,j))
          call fran_spline(xa(1,j),ya(1,j),npun,yp1,ypn,y2a(1,j))
        end do
       end if

       icall=1
       close (11)
       close (22)

      end if

c ----------- loop over jacobi coordinates

        call vpleg(nlam,cth,p)
        call fran_splint(xa,ya,y2a,npun,r,y,nlam)

        vtot=0.0d0 
        do j=1,nlam
         vtot=vtot+y(j)*p(j)
        end do
        v=vtot*vscale
      return
      end 

      subroutine vpleg(lmax,y,p)
      implicit real*8 (a-h,o-z)
      integer lmax,l,lp,lm
      
      dimension p(lmax+1)
c
          p(1)=1.d0
          p(2)=y
        if(lmax.gt.1) then  
          do 10 l=2,lmax
            lp=l+1
            lm=l-1
            xlml=dble(lm+l)
            xlm=dble(lm)
            xli=1.d0/dble(l)
              p(lp)=(xlml*y*p(l)-xlm*p(lm))*xli
 10       continue
        end if
      return
      end

      subroutine fran_spline(xa,ya,n,yp1,ypn,y2a)
      implicit real*8 (a-h,o-z)
      integer n,nmax,i,k
      parameter (nmax=10000)
      dimension xa(n),ya(n),y2a(n),u(nmax)
      if (yp1.gt..99d30) then
        y2a(1)=0.d0
        u(1)=0.d0
      else
        y2a(1)=-0.5d0
        u(1)=(3.d0/(xa(2)-xa(1)))*((ya(2)-ya(1))/(xa(2)-xa(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(xa(i)-xa(i-1))/(xa(i+1)-xa(i-1))
        p=sig*y2a(i-1)+2.d0
        y2a(i)=(sig-1.d0)/p
        u(i)=(6.d0*((ya(i+1)-ya(i))/(xa(i+1)-xa(i))-(ya(i)-ya(i-1))
     &      /(xa(i)-xa(i-1)))/(xa(i+1)-xa(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99d30) then
        qn=0.d0
        un=0.d0
      else
        qn=0.5d0
        un=(3.d0/(xa(n)-xa(n-1)))*(ypn-(ya(n)-ya(n-1))/(xa(n)-xa(n-1)))
      endif
      y2a(n)=(un-qn*u(n-1))/(qn*y2a(n-1)+1.)
      do 12 k=n-1,1,-1
        y2a(k)=y2a(k)*y2a(k+1)+u(k)
12    continue
      return
      end

      subroutine fran_splint(xa,ya,y2a,n,r,y,nlam)
      implicit real*8 (a-h,o-z)
      integer n,nlam,klo,khi,j
      dimension xa(n),ya(n,nlam),y2a(n,nlam),y(nlam)
c
        x=r

c     if(x.lt.xa(1)) x=xa(1)
c     if(x.ge.xa(n)) x=xa(n)-0.00001d0

      h=(xa(2)-xa(1))
      hinv=1.d0/h
      klo=(x-xa(1))*hinv+1
      khi=klo+1
c
c     klo=1
c     khi=n
c1     if (khi-klo.gt.1) then
c       k=(khi+klo)/2
c       if(xa(k).gt.x)then
c         khi=k
c       else
c         klo=k
c       endif
c     goto 1
c     endif
c     h=xa(khi)-xa(klo)
c     if (h.eq.0.d0) then
c       write(*,*) khi,klo,xa(khi),xa(klo)
c       pause 'bad xa input.'
c     end if
c
      a=(xa(khi)-x)*hinv
      b=(x-xa(klo))*hinv

        do j=1,nlam
            y(j)=a*ya(klo,j)+b*ya(khi,j)+
     &      ((a**3-a)*y2a(klo,j)+(b**3-b)*y2a(khi,j))*(h**2)/6.d0
        end do
      return
      end

c
