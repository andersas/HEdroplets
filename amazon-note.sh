#!/bin/sh
# On a new cluster node:
apt-get update
#apt-get install -y gfortran gcc liblapack-dev fftw3-dev mpich2 gsl-bin libgsl0ldbl libgsl0-dev libgsl0-dbg vim git make screen parallel
apt-get install -y gfortran gcc liblapack-dev libfftw3-dev mpich2 gsl-bin libgsl-dev libgsl-dbg vim git make screen parallel
# 2x 320 GB ssd as swap. Why not?
mkswap -f /dev/xvdb
mkswap -f /dev/xvdc
# Equal priority = raid 0 swap
swapon -p 0 /dev/xvdb
swapon -p 0 /dev/xvdc
mount -t tmpfs -o remount,size=800g tmpfs /run/shm/
echo As user:
echo git clone https://github.com/andersas/HEdroplets.git
echo ulimit -s 419430400
echo export OMP_STACKSIZE=400M

# run with ulimit -s 419430400 and/or
# export OMP_STACKSIZE=400M
# At least for the rather big basis, 300 mb stack size is bare minimum,
# as required by diagonalize_wmat. Less can be used if diagonalize_wmat
# is not parallelized.
#
#
# Get rid of openblas multithreading:
# export OPENBLAS_NUM_THREADS=1
#
# Control ports mpich use:
# export MPIR_CVAR_CH3_PORT_RANGE=10000:10100
# or
# export MPIEXEC_PORT_RANGE=10000:10003
# rm -f somefile somefile2 ; for i in `ls | egrep *e[0-9]{2}` ; do echo -n "$i " >> somefile && find ./$i/ -name norm.dat -exec tail -n 1 "{}" \; >> somefile ; done && sort -g somefile > somefile2
#
# Run this to find bound state overlaps
# rm -f somefile somefile2 ; for i in `ls | egrep *e[0-9]{2}` ; do echo -n "$i " >> somefile && find ./$i/ -name bound_state_overlap.dat -exec tail -n 1 "{}" \; >> somefile && echo >> somefile ; done && sort -g somefile | grep -v "^$" > somefile2
# Thin out ensemble members
# ls | sort -g -k 4 -r | sed -n 'n;p' | sed 's/ /\\ /g' | xargs find | grep "part.dat" | sed 's/ /\\ /g' | xargs truncate -s 0
