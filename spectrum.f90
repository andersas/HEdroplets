program init

     use conf

     call conf_init()
     call spectrum()

end program init


subroutine spectrum()
     use conf
     use potentials
     use thermal

     integer :: j, jt, l, ir
     real(kind=db) :: dtR
     integer :: jt_num_states(0:imag_JTOTMAX)
     real(kind=db) :: E(num_trial_states,0:imag_JTOTMAX)
     complex(kind=db) :: k(NR) = (/( ir*DK, ir=1,NR )/)
     complex(kind=db) :: expkin(NR), expkinHalf(NR)
     real(kind=db) :: vmat(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)
     real(kind=db) :: expvmat(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)
     
     complex(kind=db) :: a(NR,num_N,num_trial_states,0:imag_JTOTMAX)

     real(kind=db), parameter :: kelvin_to_wavenumbers = 0.695034761

     print *, 'Loading potential...'
     call setpotmat_simple(molecule,imag_JTOTMAX,JTOTSTEP,imag_num_N,vmat)

     print *, 'Exponentiating potential....'
     dtR = DT_imag
     expkin = (/( exp(-H2Mred*k(ir)**2*dtR), ir=1,NR )/)
     expkinHalf = (/( exp(-H2Mred*k(ir)**2*dtR/2), ir=1,NR )/)
     
     call exponentiate_imag_vmat(dtR,vmat,expvmat)

     print *, 'Finding energy spectrum....'

     jt_num_states = (/( num_trial_states, jt=0,imag_JTOTMAX )/)

!     !$OMP PARALLEL DO DEFAULT(private) SHARED(jt_num_states,a,E,expvmat,expkin,expkinHalf,indOrder)
      call energy_spectrum(jt_num_states, a(:,:,:,jt),E(:,jt),expvmat, expkin, expkinHalf)
      do jt=0,imag_JTOTMAX,JTOTSTEP
      do ir = 1, jt_num_states(jt)
          print *, E(ir,jt), E(ir,jt)*kelvin_to_wavenumbers
      enddo
     enddo
!     !$OMP END PARALLEL DO


     return

 98  write(*,*) 'Could not open', imag_expV_file, '. Quitting!'
     return
end subroutine spectrum
