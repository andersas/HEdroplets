complex(kind=db) function dot(a,b)
     use conf
     implicit none

     complex(kind=db), intent(in), dimension(NR,num_N,0:JTOTMAX) :: a,b
     integer :: jt
     integer(kind=ll) :: i

     dot = 0d0

     do jt=0,JTOTMAX,JTOTSTEP
          do i = 1,indOrder(jt)
               dot = dot + sum(conjg(a(:,i,jt))*b(:,i,jt))
          enddo
     enddo

     dot = dot * DR
     return
end function dot

complex(kind=db) function dot_laserrep(a,b)
     use conf
     implicit none

     complex(kind=db), intent(in), dimension(NR,num_jJ,0:LMAX) :: a,b
     integer :: l
     integer(kind=ll) :: i

     dot_laserrep = 0d0

     do l=0,LMAX,LSTEP
          do i = 1,UindOrder(l)
               dot_laserrep = dot_laserrep + sum(conjg(a(:,i,l))*b(:,i,l))
          enddo
     enddo

     dot_laserrep = dot_laserrep * DR
     return
end function dot_laserrep


complex(kind=db) function dot_subspace(a,b,jt)
     use conf
     implicit none

     complex(kind=db), intent(in), dimension(NR,num_N) :: a,b
     integer, intent(in) :: jt
     integer(kind=ll) :: i

     dot_subspace = 0d0

     do i = 1,indOrder(jt)
          dot_subspace = dot_subspace + sum(conjg(a(:,i))*b(:,i))
     enddo

     dot_subspace = dot_subspace * DR
     return
end function dot_subspace

complex(kind=db) function dot_laserblock(a,b,l)
     use conf
     implicit none

     complex(kind=db), intent(in), dimension(NR,num_jJ) :: a,b
     integer, intent(in) :: l
     integer(kind=ll) :: i

     dot_laserblock = 0d0

     do i = 1,UindOrder(l)
          dot_laserblock = dot_laserblock + sum(conjg(a(:,i))*b(:,i))
     enddo

     dot_laserblock = dot_laserblock * DR
     return
end function dot_laserblock

real(kind=db) function norm(a)
     use conf
     implicit none
     complex(kind=db), intent(in), dimension(NR,num_N,0:JTOTMAX) :: a
     complex(kind=db) :: dot

     norm = sqrt(real(dot(a,a)))
     return
end function norm

real(kind=db) function norm_laserrep(a)
     use conf
     implicit none
     complex(kind=db), intent(in), dimension(NR,num_jJ,0:LMAX) :: a
     complex(kind=db) :: dot_laserrep

     norm_laserrep = sqrt(real(dot_laserrep(a,a)))
     return
end function norm_laserrep



real(kind=db) function norm_subspace(a,jt)
     use conf
     implicit none
     complex(kind=db), intent(in), dimension(NR,num_N) :: a
     complex(kind=db) :: dot_subspace
     integer, intent(in) :: jt

     norm_subspace = sqrt(real(dot_subspace(a,a,jt)))
end function norm_subspace

real(kind=db) function norm_laserblock(a,l)
     use conf
     implicit none
     complex(kind=db), intent(in), dimension(NR,num_jJ) :: a
     complex(kind=db) :: dot_laserblock
     integer, intent(in) :: l

     norm_laserblock = sqrt(real(dot_laserblock(a,a,l)))
end function norm_laserblock


real(kind=db) function norm_squared_jsubspace(a,j)
     use conf
     implicit none
     complex(kind=db), intent(in), dimension(NR,num_N,0:JTOTMAX) :: a
     integer, intent(in) :: j
     integer :: jt, l
     integer(kind=ll) :: i
     real(kind=db) :: res
 
     res = 0d0

     do jt=0,JTOTMAX,JTOTSTEP
          do l=0,LMAX,LSTEP
               i = ind(l,j,jt); if (i<0) cycle
               res = res + sum(real(conjg(a(:,i,jt))*a(:,i,jt)))
          enddo
     enddo

     norm_squared_jsubspace = res * DR    

end function norm_squared_jsubspace

real(kind=db) function norm_squared_lsubspace(a,l)
     use conf
     implicit none
     complex(kind=db), intent(in), dimension(NR,num_N,0:JTOTMAX) :: a
     integer, intent(in) :: l
     integer :: jt, j
     integer(kind=ll) :: i
     real(kind=db) :: res
 
     res = 0d0

     do jt=0,JTOTMAX,JTOTSTEP
          do j=0,jMAX,jSTEP
               i = ind(l,j,jt); if (i<0) cycle
               res = res + sum(real(conjg(a(:,i,jt))*a(:,i,jt)))
          enddo
     enddo

     norm_squared_lsubspace = res * DR

end function norm_squared_lsubspace

subroutine norm_squared_rsubspace(a,rvec)
     use conf
     implicit none

     complex(kind=db), intent(in), dimension(NR,num_N,0:JTOTMAX) :: a
     integer :: jt
     integer(kind=ll) :: i
     real(kind=db), intent(out) :: rvec(NR)

     rvec = 0d0

     do jt=0,JTOTMAX,JTOTSTEP
          do i = 1,indOrder(jt)
               rvec = rvec + real(conjg(a(:,i,jt))*a(:,i,jt))
          enddo
     enddo

     rvec = rvec * DR
     return
end subroutine norm_squared_rsubspace



subroutine project_out(x,a)
     ! Project a out of x. a must have norm=1
     use conf
     implicit none

     complex(kind=db), dimension(NR,num_N,0:JTOTMAX) :: x,a
     intent(inout) :: x
     intent(in) :: a
     complex(kind=db) :: dot

     x = x - dot(a,x)*a

end subroutine project_out

subroutine project_out_subspace(x,a,jt)
     ! Project a out of x. a must have norm=1
     use conf
     implicit none

     complex(kind=db), dimension(NR,num_N) :: x,a
     intent(inout) :: x
     intent(in) :: a
     complex(kind=db) :: dot_subspace
     integer :: jt

     x = x - dot_subspace(a,x,jt)*a

end subroutine project_out_subspace

complex(kind=db) function copy_statevector(b,a)
     use conf
     implicit none

     complex(kind=db), intent(in), dimension(NR,num_N,0:JTOTMAX) :: a
     complex(kind=db), intent(out), dimension(NR,num_N,0:JTOTMAX) :: b
     integer :: jt

     do jt=0,JTOTMAX,JTOTSTEP
          b(:,1:indOrder(jt), jt) = a(:,1:indOrder(jt),jt)
     enddo

end function copy_statevector


real(kind=db) function bound_state_overlap(jt_num_states,energy_eigenstates,E_stationary,a)
     use conf

     integer, intent(in) :: jt_num_states(0:imag_JTOTMAX)
     complex(kind=db), intent(in) :: energy_eigenstates(NR,num_N,num_trial_states,0:imag_JTOTMAX)
     real(kind=db), intent(in), dimension(num_trial_states,0:imag_JTOTMAX) :: E_stationary

     complex(kind=db), intent(in) :: a(NR,num_N,0:JTOTMAX)

     complex(kind=db) :: dot_subspace
     complex(kind=db) :: overlap

     integer :: jt,i

     bound_state_overlap = 0

     if (He .ne. 0) then

     do jt=0,min(JTOTMAX,imag_JTOTMAX),JTOTSTEP
          do i = 1,num_trial_states
               if (E_stationary(i,jt) .ge. 0) exit
               overlap = dot_subspace(a(:,:,jt),energy_eigenstates(:,:,i,jt),jt)
               bound_state_overlap = bound_state_overlap + abs(overlap)**2
          enddo
     enddo

     endif

end function bound_state_overlap


subroutine apply_expWmat_fast_block(matvec_block,factor,a)
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db) :: ap(NR,num_jJ,0:LMAX)
     complex(kind=db) :: factor
     external :: matvec_block
     integer, parameter :: mw = krylov_subspace_size_W
     
     complex(kind=db), external :: dot_laserblock
     real(kind=db), external :: norm_laserblock

     call prepare_statevec_for_laser_matrices(ap,a)
     call exp_a_krylov_block(num_jJ,LMAX,UindOrder,factor,ap,matvec_block,norm_laserblock,dot_laserblock,mw,1)
     call unprepare_statevec_for_laser_matrices(a,ap)

end subroutine apply_expWmat_fast_block

subroutine apply_expWmat_fast(matvec,factor,a)
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db) :: ap(NR,num_jJ,0:LMAX)
     complex(kind=db) :: factor
     external :: matvec
     integer, parameter :: mw = krylov_subspace_size_W

     complex(kind=db), external :: dot_laserrep
     real(kind=db), external :: norm_laserrep

     call prepare_statevec_for_laser_matrices(ap,a)
     call exp_a_krylov(num_jJ,LMAX,factor,ap,matvec,norm_laserrep,dot_laserrep,mw,1)
     call unprepare_statevec_for_laser_matrices(a,ap)

end subroutine apply_expWmat_fast

subroutine apply_cos2mat_fast(cos2mat,Unexti,a,Ublock_offset)
     ! Multiply the W matrix or U matrix with a state vector
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_jJ,0:LMAX)
     real(kind=lpot_k), intent(in) :: cos2mat(num_Umat)
     integer(kind=idx_k), intent(in) :: Unexti(num_Umat)
     integer(kind=ll) :: Ui, Uii
     complex(kind=db), dimension(NR,num_jJ,0:LMAX) :: a_out
     integer(kind=ll), intent(in) :: Ublock_offset(2,0:LMAX)

     integer :: l
     integer(kind=ll) :: Ui1, Ui2

     a_out = 0

outer: do l = 0, LMAX,LSTEP
     Ui = Ublock_offset(1,l)
     Uii = Ublock_offset(2,l)
     do Ui2 = 1,UindOrder(l)
     do Ui1 = Ui2,UindOrder(l)

          Uii = Uii + 1

          if (Unexti(Ui) .ne. Uii) cycle
     
          if (Ui1.ne.Ui2) then
               a_out(:, Ui1, l) = a_out(:, Ui1, l) + cos2mat(Ui)*a(:, Ui2, l)
               a_out(:, Ui2, l) = a_out(:, Ui2, l) + cos2mat(Ui)*a(:, Ui1, l)
          else
               a_out(:, Ui1, l) = a_out(:, Ui1, l) + cos2mat(Ui)*a(:, Ui1, l)
          endif

          Ui = Ui + 1
          if (Unexti(Ui) .eq. 0) exit outer
     enddo
     enddo
     end do outer

     a = a_out

end subroutine apply_cos2mat_fast

subroutine apply_cos2mat_fast_block(cos2mat,Unexti,a,Ublock_offset,l)
     ! Multiply the W matrix or U matrix with a state vector
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_jJ)
     real(kind=lpot_k), intent(in) :: cos2mat(num_Umat)
     integer(kind=idx_k), intent(in) :: Unexti(num_Umat)
     integer(kind=ll) :: Ui, Uii
     integer(kind=ll), intent(in) :: Ublock_offset(2,0:LMAX)
     complex(kind=db), dimension(NR,num_jJ) :: a_out

     integer, intent(in) :: l
     integer(kind=ll) :: Ui1, Ui2

     a_out = 0

     Ui = Ublock_offset(1,l)
     Uii = Ublock_offset(2,l)

outer: do Ui2 = 1,UindOrder(l)
     do Ui1 = Ui2,UindOrder(l)

          Uii = Uii + 1

          if (Unexti(Ui) .ne. Uii) cycle
     
          if (Ui1.ne.Ui2) then
               a_out(:, Ui1) = a_out(:, Ui1) + cos2mat(Ui)*a(:, Ui2)
               a_out(:, Ui2) = a_out(:, Ui2) + cos2mat(Ui)*a(:, Ui1)
          else
               a_out(:, Ui1) = a_out(:, Ui1) + cos2mat(Ui)*a(:, Ui1)
          endif

          Ui = Ui + 1
          if (Unexti(Ui) .eq. 0) exit outer
     enddo
     end do outer

     a = a_out

end subroutine apply_cos2mat_fast_block

subroutine apply_vmat(v,diag,rotmat,nexti,a,block_offset)
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     
     real(kind=pot_k), intent(in) :: rotmat(num_rotmat)
     integer(kind=idx_k) :: nexti(num_rotmat)
     integer(kind=ll) :: i,ii
     real(kind=db), intent(in) :: diag(NR,num_N,0:JTOTMAX)
     real(kind=pot_k), intent(in) :: v(NR_pot,0:LAMAX)
     integer(kind=ll),intent(in) :: block_offset(2,0:JTOTMAX)
     
     complex(kind=pot_k) :: a_out(NR,num_N,0:JTOTMAX)
     complex(kind=pot_k) :: va(NR_pot,num_N,0:LAMAX/LASTEP,0:JTOTMAX)

     integer :: la,jt
     integer(kind=ll) :: i1,i2

     if (He .eq. 0) then
          call apply_Hrot(a)
          return
     endif

     a_out = 0

     do jt=0,JTOTMAX,JTOTSTEP
     do i1 = 1,indOrder(jt)
     do la = 0, LAMAX/LASTEP
         va(:,i1,la,jt) = v(:,la*LASTEP)*cmplx(a(1:NR_pot,i1,jt),kind=pot_k)
     enddo
     enddo
     enddo

outer: do jt=0,JTOTMAX,JTOTSTEP
     i = block_offset(1,jt)
     ii = block_offset(2,jt)
     do la = 0, LAMAX, LASTEP
     do i1 = 1,indOrder(jt)
     do i2 = i1,indOrder(jt)
     
     ii = ii+1
     if (nexti(i) .ne. ii) cycle

          if (i1 .ne. i2) then
               a_out(1:NR_pot,i1,jt) = a_out(1:NR_pot,i1,jt) + rotmat(i)*va(:,i2,la/LASTEP,jt)
               a_out(1:NR_pot,i2,jt) = a_out(1:NR_pot,i2,jt) + rotmat(i)*va(:,i1,la/LASTEP,jt)
           !    a_out((NR_pot+1):NR,i1,jt) = a_out((NR_pot+1):NR,i1,jt) + &
           !         (rotmat(i)*v(NR_pot,la))*cmplx(a((NR_pot+1):NR,i2,jt),kind=pot_k)
           !    a_out((NR_pot+1):NR,i2,jt) = a_out((NR_pot+1):NR,i2,jt) + &
           !         (rotmat(i)*v(NR_pot,la))*cmplx(a((NR_pot+1):NR,i1,jt),kind=pot_k)
          else
               a_out(1:NR_pot,i2,jt) = a_out(1:NR_pot,i2,jt) + rotmat(i)*va(:,i1,la/LASTEP,jt)
           !    a_out((NR_pot+1):NR,i2,jt) = a_out((NR_pot+1):NR,i2,jt) + &
           !         (rotmat(i)*v(NR_pot,la))*cmplx(a((NR_pot+1):NR,i1,jt),kind=pot_k)
          endif
          i = i + 1
          if (nexti(i) .eq. 0) exit outer
     enddo
     enddo
     enddo
     end do outer

     a = a_out + diag*a

end subroutine apply_vmat

subroutine apply_Hrot(a)
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     integer(kind=ll) :: ir, j, jt, l, i
     real(kind = db) :: Hrot

     do jt = 0,JTOTMAX,JTOTSTEP
     do j = 0, jMAX,jSTEP
          Hrot = Bconst*j*(j+1)
          do l = 0, LMAX, LSTEP
               i = ind(l,j,jt); if (i .lt. 0) cycle
               a(:,i,jt) = Hrot*a(:,i,jt) 
          enddo
     enddo
     enddo

end subroutine apply_Hrot

subroutine apply_T(a)

     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     integer :: ir
     real(kind=db), parameter :: k(NR) = (/( ir*DK, ir=1,NR )/)
     complex(kind=db), parameter :: kin(NR) = (/( H2Mred*k(ir)**2, ir=1,NR )/)

     if (He .eq. 0) return

     call apply_expT(kin,a); ! apply_expT doesnt actually care
                             ! that we didn't exponentiate first...

end subroutine apply_T


subroutine apply_vmat_block(v,diag,rotmat,nexti,a,block_offset,jt)
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_N)
     
     real(kind=pot_k), intent(in) :: rotmat(num_rotmat)
     integer(kind=idx_k) :: nexti(num_rotmat)
     integer(kind=ll) :: i,ii
     integer(kind=ll),intent(in) :: block_offset(2,0:JTOTMAX)
     real(kind=db), intent(in) :: diag(NR,num_N,0:JTOTMAX)
     real(kind=pot_k), intent(in) :: v(NR_pot,0:LAMAX)
     
     complex(kind=pot_k) :: a_out(NR,num_N)
     complex(kind=pot_k) :: va(NR_pot,num_N,0:LAMAX/LASTEP)

     integer :: la,jt
     integer(kind=ll) :: i1,i2

     a_out = 0

     do i1 = 1,indOrder(jt)
     do la = 0, LAMAX/LASTEP
         va(:,i1,la) = v(:,la*LASTEP)*cmplx(a(1:NR_pot,i1),kind=pot_k)
     enddo
     enddo

     i = block_offset(1,jt)
     ii = block_offset(2,jt)
outer: do la = 0, LAMAX, LASTEP
     do i1 = 1,indOrder(jt)
     do i2 = i1,indOrder(jt)
     
     ii = ii+1
     if (nexti(i) .ne. ii) cycle

          if (i1 .ne. i2) then
               a_out(1:NR_pot,i1) = a_out(1:NR_pot,i1) + rotmat(i)*va(:,i2,la/LASTEP)
               a_out(1:NR_pot,i2) = a_out(1:NR_pot,i2) + rotmat(i)*va(:,i1,la/LASTEP)
          !     a_out((NR_pot+1):NR,i1) = a_out((NR_pot+1):NR,i1) + &
          !          (rotmat(i)*v(NR_pot,la))*cmplx(a((NR_pot+1):NR,i2),kind=pot_k)
          !     a_out((NR_pot+1):NR,i2) = a_out((NR_pot+1):NR,i2) + &
          !          (rotmat(i)*v(NR_pot,la))*cmplx(a((NR_pot+1):NR,i1),kind=pot_k)
          else
               a_out(1:NR_pot,i2) = a_out(1:NR_pot,i2) + rotmat(i)*va(:,i1,la/LASTEP)
          !     a_out((NR_pot+1):NR,i2) = a_out((NR_pot+1):NR,i2) + &
          !          (rotmat(i)*v(NR_pot,la))*cmplx(a((NR_pot+1):NR,i1),kind=pot_k)
          endif
          i = i + 1
          if (nexti(i) .eq. 0) exit outer
     enddo
     enddo
     end do outer

     a = a_out + diag(:,:,jt)*a

end subroutine apply_vmat_block

subroutine setblockmap(blockmap,a)
     ! Create the mask norm_jt_subspace(a) > 1e-4
     use conf
     implicit none

     logical, intent(out) :: blockmap(0:JTOTMAX)
     complex(kind=db),intent(in) :: a(NR,num_N,0:JTOTMAX)

     real(kind=db) :: norm_subspace
     integer :: jt
          
     do jt = 0,JTOTMAX,JTOTSTEP
          blockmap(jt) = norm_subspace(a(:,:,jt),jt) .gt. 1e-4
     enddo

end subroutine setblockmap

subroutine apply_expvmat(expvmat, expind, expdiag, block, blockmap, a)
     use conf
     implicit none

     complex(kind=db),intent(inout) :: a(NR,num_N,0:JTOTMAX)
     logical, intent(in) :: blockmap(0:JTOTMAX)
     integer(kind=ll), intent(in) :: block(2,0:JTOTMAX)
     complex(kind=db),intent(in) :: expdiag(NR,num_N,0:JTOTMAX)
     integer(kind=idx_k), intent(in) :: expind(num_expvmat)
     complex(kind=exp_k), intent(in) :: expvmat(num_expvmat)

     complex(kind=db) :: a_out(NR,num_N,0:JTOTMAX)
     
     integer(kind=ll) :: ei, eii, i1,i2
     integer :: jt,ir

     a_out = 0
     
     if (He .eq. 1) then
outer: do jt = 0,JTOTMAX,JTOTSTEP
     if (.not. blockmap(jt)) cycle
     ei = block(1,jt)
     eii = block(2,jt)
     do i1 = 1,indOrder(jt)
     do i2 = i1,indOrder(jt)
     do ir = 1,NR_pot
     
          eii = eii + 1
          if (expind(ei) .ne. eii) cycle
          
          a_out(ir,i2,jt) = a_out(ir,i2,jt) + expvmat(ei)*a(ir,i1,jt)
          if (i2.ne.i1) &
          a_out(ir,i1,jt) = a_out(ir,i1,jt) + expvmat(ei)*a(ir,i2,jt)

          ei = ei + 1
          if (expind(ei) .eq. 0) exit outer
     enddo
     enddo
     enddo
     end do outer
     endif

     do jt = 0,JTOTMAX,JTOTSTEP
     if (.not. blockmap(jt)) cycle
     do i1 = 1,indOrder(jt)
     a_out((NR_pot+1):NR,i1,jt) = a_out((NR_pot+1):NR,i1,jt) + &
               expdiag((NR_pot+1):NR,i1,jt)*a((NR_pot+1):NR,i1,jt)
     enddo
     enddo

     a = a_out

end subroutine apply_expvmat


double precision function absorb(x,s)
     implicit none

     double precision, intent(in) :: x,s
     double precision  :: smooth_transfer

     absorb = 1-smooth_transfer((x+s)/s)

end function absorb

real(kind=db) function absorb_old(x,s)
     use conf

     real(kind=db) :: x,s

     if (x < -s) then
          absorb_old = 1d0
     else if (x > 0) then
          absorb_old = 0d0
     else
          absorb_old = exp(1d0-1d0/(1d0-((s+x)/s)**2))
     endif

end function absorb_old


double precision function smooth_transfer(x)
     implicit none

     double precision :: psi
     double precision, intent(in) :: x

     smooth_transfer = psi(x)/(psi(x) + psi(1-x))

end function smooth_transfer

double precision function psi(x)

     implicit none
     double precision, intent(in) :: x
     if (x > 0) then
          psi = exp(-1/x)
     else
          psi = 0
     endif

end function psi



subroutine accumulate_absorbed_probability(cos2,cos2_naive,cos2mat,current_integral,N,ap,R,Unexti,t)
     use conf
     implicit none
     real(kind=db), intent(out) :: cos2,cos2_naive
     real(kind=lpot_k), intent(in) :: cos2mat(num_Umat)
     integer, intent(in) :: N
     real(kind=db), intent(inout) :: current_integral(N)
     complex(kind=db), intent(in) :: ap(NR,num_jJ,0:LMAX)
     integer, intent(in) :: R
     integer(kind=idx_k), intent(in) :: Unexti(num_Umat)
     real(kind=db), intent(in) :: t
     real(kind=db) :: channel_current, channel_weight,naive_weight
     complex(kind=db) :: term


     complex(kind=db) :: phase(0:jMAX), Pjj, point_derivative

     integer :: l,j
     integer(kind=ll) :: Ui, Uii, Ui1, Ui2, j1, j2, jt1, jt2

     ! Note: the current integral is hermitian as the cos^2 theta matrix, so we
     ! only need to store one side of it. Further, we only need to store
     ! the real part, as the complex part cancels out in the expression
     ! for the expectation value (because it is hermitian). 
     ! The off-diagonal real parts just need a factor 2 when calculating
     ! the exepectation value.

     ! Calculate exp(iEj/hbar*t). To get exp(i(Ej-Ej')/hbar*t) just
     ! calculate phase(j)/phase(j'). (need to evaluate complex exponential
     ! jMAX times instead of O(jMAX^2) times)
     forall (j = 0:jMAX) phase(j) = exp((0,1)*Bconst*j*(j+1)*t)

     cos2 = 0;
     cos2_naive = 0;

     Ui = 1
     Uii = 0
outer: do l = 0, LMAX, LSTEP
     do jt2 = 0, JTOTMAX, JTOTSTEP
     do j2 = 0, jMAX, jSTEP
     Ui2 = Uind(j2,jt2,l); if (Ui2<0) cycle
     do jt1 = 0,JTOTMAX,JTOTSTEP
     do j1 = 0,jMAX,jSTEP
     Ui1 = Uind(j1,jt1,l); if(Ui1<0) cycle
          if (Ui1 .lt. Ui2) cycle

          Uii = Uii + 1
          if (Unexti(Ui) .ne. Uii) cycle
          
          Pjj = phase(j2)/phase(j1)
          ! Off-diagonally: Only need the real part.
          ! The complex parts end up cancelling.
          ! On the diagonal: only need the imaginary part (real part = 0)
          ! Store - imag because of the factor i
      
          term = Pjj*(ap(R,Ui2,l)*conjg(point_derivative(ap(:,Ui1,l),R)) - &
                 conjg(ap(R,Ui1,l))*point_derivative(ap(:,Ui2,l),R)  )

          if (Ui1 .ne. Ui2) then
               current_integral(Ui) = current_integral(Ui) + DT*realpart(term)
          else
               current_integral(Ui) = current_integral(Ui) - DT*imagpart(term)
          endif
     
          channel_current = H2Mred*current_integral(Ui)
          if (Ui1 .ne. Ui2) then ! Two off-diagonal terms
               channel_current = channel_current*2*real(Pjj)
          endif

          channel_weight = DR*sum(real(conjg(ap(1:R,Ui1,l))*ap(1:R,Ui2,l)))
          naive_weight = DR*sum(real(conjg(ap(:,Ui1,l))*ap(:,Ui2,l)))
          if (Ui1 .ne. Ui2) then
               channel_weight = 2*channel_weight
               naive_weight = 2*naive_weight
          endif
               
          cos2 = cos2 + cos2mat(Ui)*(channel_current+channel_weight)
          cos2_naive = cos2_naive + cos2mat(Ui)*naive_weight
               
          Ui = Ui + 1;
          if (Unexti(Ui) .eq. 0) exit outer
     enddo
     enddo
     enddo
     enddo
     end do outer

end subroutine accumulate_absorbed_probability

complex(kind=db) function point_derivative(u,R)
     use conf
     implicit none
     complex(kind=db), intent(in) :: u(NR)
     integer, intent(in) :: R
     
     ! Assume R is not at the edges of u and evaluate the
     ! central difference (2. order)

     ! 2nd order: 
     !point_derivative = (u(R+1) - u(R-1))/2/DR
     ! 4th order:
     point_derivative = (-u(R+2)/4 + 2*u(R+1) - 2*u(R-1) + u(R-2)/4)/(3*DR)
     ! 6th order:
     !point_derivative = (u(R+3)/15 - 3*u(R+2)/5 + 3*u(R+1) - 3*u(R-1) + 3*u(R-2)/5 - u(R-3)/15)/4/DR

     return
end function point_derivative

subroutine apply_absorbing_boundary(a)
     use conf
     implicit none

     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     integer :: ir,jt,i
     real(kind=db) :: absorb, norm
     real(kind=db), save :: fac(NR) = 0
     
     if (He .eq. 0) return

     if (fac(1) == 0) then
          fac = (/( absorb(real(ir-NR,kind=db),real(absorption_zone,kind=db)), ir=1,NR )/)
     endif
      
     do jt = 0,JTOTMAX,JTOTSTEP
     do i = 1,indOrder(jt)
          a(:,i,jt) = a(:,i,jt) * fac
     enddo
     enddo

end subroutine apply_absorbing_boundary


subroutine apply_expvmat_krylov_block(matvec_block,factor,a,num_steps)
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db), intent(in) :: factor
     external :: matvec_block
     integer,intent(in) :: num_steps
     complex(kind=db), external :: dot_subspace
     real(kind=db), external :: norm_subspace
     integer, parameter :: mv = krylov_subspace_size_V

     !complex(kind=db), external :: dot_subspace
     !real(kind=db), external :: norm_subspace

     call exp_a_krylov_block(num_N,JTOTMAX,indOrder,factor,a,&
          matvec_block,norm_subspace,dot_subspace, mv,num_steps)

end subroutine apply_expvmat_krylov_block

subroutine apply_expvmat_krylov(matvec,factor,a,num_steps)
     use conf
     implicit none
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db), intent(in) :: factor
     external :: matvec
     integer,intent(in) :: num_steps
     complex(kind=db), external :: dot_subspace
     real(kind=db), external :: norm_subspace
     integer, parameter :: mv = krylov_subspace_size_V

     complex(kind=db), external :: dot
     real(kind=db), external :: norm

     call exp_a_krylov(num_N,JTOTMAX,factor,a, matvec,norm,dot, mv,num_steps)

end subroutine apply_expvmat_krylov



subroutine apply_expT(expkin,a)
     use conf
     implicit none

     complex(kind=db), intent(in) :: expkin(NR)
     complex(kind=db), intent(inout) :: a(NR,num_N,0:JTOTMAX)
     integer :: jt, order

     if (He .eq. 0) return

     do jt=0,JTOTMAX,JTOTSTEP
          order = indOrder(jt)
          call apply_expT_singleJ(expkin,a(1:NR,1:order,jt),order)
     enddo

end subroutine apply_expT

subroutine apply_expT_singleJ(expkin,a,order)
     use conf
     implicit none

     integer, intent(in) :: order
     complex(kind=db), intent(in) :: expkin(NR)
     complex(kind=db), intent(inout) :: a(NR,order)
     integer :: i

     if (He .eq. 0) return

     ! Fourier transform radial part
     do i=1,order
          call fftNN('r2k',NR,a(1,i),a(1,i))
     enddo
     ! Apply T operator (diagonal in Fourier space)
     forall(i=1:order) a(1:NR,i) = expkin(1:NR)*a(1:NR,i)
     ! Fourier transform back
     do i=1,order
          call fftNN('k2r',NR,a(1,i),a(1,i))
     enddo

end subroutine apply_expT_singleJ


subroutine prepare_statevec_for_laser_matrices(ap, a)
     use conf

     implicit none
     complex(kind=db), intent(in) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db), intent(out) :: ap(NR,num_jJ,0:LMAX)

     integer :: l,j,jt
     integer(kind=ll) :: ui, ii

     do jt = 0,JTOTMAX,JTOTSTEP
     do j = 0,jMAX,jStep
     do l = 0,LMAX,LSTEP
          ii = ind(l,j,jt); if (ii < 0) cycle
          ui = Uind(j,jt,l)!; if (ui < 0) cycle

          ap(:,ui,l) = a(:,ii,jt);
     enddo
     enddo
     enddo

end subroutine prepare_statevec_for_laser_matrices

subroutine unprepare_statevec_for_laser_matrices(a, ap)
     use conf

     implicit none
     complex(kind=db), intent(out) :: a(NR,num_N,0:JTOTMAX)
     complex(kind=db), intent(in) :: ap(NR,num_jJ,0:LMAX)

     integer :: l,j,jt
     integer(kind=ll) :: ui, ii

     do l = 0,LMAX,LSTEP
     do jt = 0,JTOTMAX,JTOTSTEP
     do j = 0,jMAX,jStep
          ui = Uind(j,jt,l); if (ui < 0) cycle
          ii = ind(l,j,jt)!; if (ii < 0) cycle

          a(:,ii,jt) = ap(:,ui,l)
     enddo
     enddo
     enddo

end subroutine unprepare_statevec_for_laser_matrices


subroutine exp_a_krylov(dim1,dim2,factor,a,matvec,vecnorm,vecdot,krylov_subspace_size,num_steps)
     use conf
     implicit none
     
     integer, intent(in) :: dim1, dim2
     complex(kind=db), intent(inout) :: a(NR,dim1,0:dim2)
     complex(kind=db), intent(in) :: factor
     integer :: krylov_subspace_size
     external :: matvec
     complex(kind=db), external :: vecdot
     real(kind=db), external :: vecnorm
     integer, intent(in) :: num_steps

     complex(kind=db)  :: step_factor

     integer :: i

     step_factor = factor/num_steps

     do i = 1,num_steps
          call exp_a_krylov_step(dim1,dim2,step_factor,a,matvec,vecnorm,vecdot,krylov_subspace_size)
     enddo

end subroutine exp_a_krylov


subroutine exp_a_krylov_block(dim1,dim2,orderArray,factor,a,matvec_block,vecnorm_block,vecdot_block,krylov_subspace_size,num_steps)
     use conf
     implicit none

     integer, intent(in) :: dim1, dim2
     complex(kind=db), intent(inout) :: a(NR,dim1,0:dim2)
     integer, intent(in) :: orderArray(0:dim2)
     complex(kind=db), intent(in) :: factor
     integer, intent(in) :: num_steps
     integer,intent(in) :: krylov_subspace_size
     real(kind=db) :: diag(krylov_subspace_size+1),offdiag(krylov_subspace_size)
     complex(kind=db) :: a_proj(krylov_subspace_size+1)
     complex(kind=db) :: V(NR,dim1,krylov_subspace_size+1)
     integer :: m
     real(kind=db) :: tol, beta
     external :: matvec_block
     real(kind=db), external :: vecnorm_block
     complex(kind=db), external :: vecdot_block

     complex(kind=db)  :: step_factor
     integer :: i,it,ord,stepnum


     step_factor = factor/num_steps

     tol = 1e-6
blockloop: do it = 0,dim2
     ord = orderArray(it)
     do stepnum = 1,num_steps

     m = min(krylov_subspace_size,ord-1)
     ! Find the Lanczos basis in the krylov space spanned by
     ! mat and a of dimension at most m.
     call lanczos_block(dim1,ord,it,a(:,:,it),m,diag,offdiag,V,beta,tol,matvec_block,vecnorm_block,vecdot_block)
     if (m .eq. 0) cycle blockloop

     ! afterwards use dsyev or dsteqr to diagonalize the resulting
     ! symmetric tridiagonal matrix and exponentiate it.

     call exponentiate_sym_tridiagonal_col1(step_factor,m+1,diag,offdiag,a_proj)
     !call exponentiate_sym_tridiagonal_col1_slow(step_factor,m+1,diag,offdiag,a_proj)
     ! Now a_proj is the projection of exp(factor*mat)*a on the krylov subspace
     ! generated by mat and a.

     ! Get it out to the big space again:
     a_proj(1:(m+1)) = a_proj(1:(m+1))*beta
     a(:,1:ord,it) = V(:,1:ord,1)*a_proj(1)
     do i = 2, m+1
          a(:,1:ord,it) = a(:,1:ord,it) + V(:,1:ord,i) * a_proj(i)
     enddo

     enddo
     end do blockloop

end subroutine exp_a_krylov_block


subroutine exp_a_krylov_step(dim1,dim2,factor,a,matvec,vecnorm,vecdot,krylov_subspace_size)
     use conf
     implicit none

     integer, intent(in) :: dim1,dim2
     complex(kind=db), intent(inout) :: a(NR,dim1,0:dim2)
     complex(kind=db), intent(in) :: factor
     integer :: krylov_subspace_size
     real(kind=db) :: diag(krylov_subspace_size+1),offdiag(krylov_subspace_size)
     complex(kind=db) :: a_proj(krylov_subspace_size+1)
     complex(kind=db) :: V(NR,dim1,0:dim2,krylov_subspace_size+1)
     integer :: m, mold
     real(kind=db) :: tol
     external :: matvec
     complex(kind=db), external :: vecdot
     real(kind=db), external :: vecnorm

     !integer(kind=ll) :: order
     integer :: i,j

     m = min(krylov_subspace_size,dim1*dim1*(dim2+1))
     tol = 1e-4
     mold = m
     ! Find the Lanczos basis in the krylov space spanned by
     ! mat and a of dimension at most m.
     call lanczos(dim1,dim2,a,m,diag,offdiag,V,tol,matvec,vecnorm,vecdot)
     if (mold .ne. m) then
          print *, 'HAPPY BREAKDOWN', mold, '=>', m
     endif

     !do i = 1,m+1
     !do j = 1,m+1
     !     print *, i,j, vecdot(V(:,:,:,i),V(:,:,:,j))
     !enddo
     !enddo

     !stop

     ! afterwards use dsyev or dsteqr to diagonalize the resulting
     ! symmetric tridiagonal matrix and exponentiate it.
     call exponentiate_sym_tridiagonal_col1(factor,m+1,diag,offdiag,a_proj)
     !call exponentiate_sym_tridiagonal_col1_slow(factor,m+1,diag,offdiag,a_proj)
     ! Now a_proj is the projection of exp(factor*mat)*a on the krylov subspace
     ! generated by mat and a.

     ! Get it out to the big space again:
    
     !print *, sqrt(sum(real(conjg(a_proj)*a_proj)))
     a = V(:,:,:,1)*a_proj(1)
     do i = 2, m+1
          a = a + V(:,:,:,i) * a_proj(i)
     enddo

     !print *, dot(a,V(:,:,:,1))

end subroutine exp_a_krylov_step

subroutine exponentiate_sym_tridiagonal_col1(factor,order,diag,offdiag,col1)
     use conf
     implicit none
     complex(kind=db), intent(in) :: factor
     integer, intent(in) :: order
     real(kind=db), intent(inout) :: diag(order),offdiag(order-1)
     complex(kind=db), intent(out) :: col1(order)

     complex(kind=db) :: expdiag_Z(order)

     real(kind=db) :: Z(order,order)
     real(kind=db) :: work(2*order)
     integer :: i,info

     Z = 0 
     forall (i=1:order) Z(i,i) = 1

          call dsteqr('I',order,diag,offdiag,Z,order,work,info)
          if (info .ne. 0) call quit('DSTEQR failed.')

          ! diag now contains the eigenvalues. offdiag is cluttered.
          ! Now calculate the first column of
          ! exp(mat*factor) = Z*exp(factor*diag)*transpose(Z)

          !print *, diag ! Eigenvalues
          !stop

          expdiag_Z = exp(factor*diag)*Z(1,:)

          do i = 1,order
          col1(i) = sum(Z(i,:)*expdiag_Z)
          enddo

end subroutine exponentiate_sym_tridiagonal_col1

subroutine exponentiate_sym_tridiagonal_col1_slow(factor,order,diag,offdiag,col1)
     use conf
     implicit none
     complex(kind=db), intent(in) :: factor
     integer, intent(in) :: order
     real(kind=db), intent(inout) :: diag(order),offdiag(order-1)
     complex(kind=db), intent(out) :: col1(order)

     complex(kind=db) :: expdiag_Z(order)

     real(kind=db) :: Z(order,order)
     real(kind=db) :: work(3*order)
     integer :: i,info

     Z = 0 
     forall (i=1:order) Z(i,i) = diag(i)
     forall (i=1:(order-1)) Z(i,i+1) = offdiag(i)

     call dsyev('V','U',order,Z,order,diag,work,3*order,info)
     if (info .ne. 0) call quit('DSYEV failed in krylov.')

     ! diag now contains the eigenvalues. offdiag is cluttered.
     ! Now calculate the first column of
     ! exp(mat*factor) = Z*exp(factor*diag)*transpose(Z)

     expdiag_Z = exp(factor*diag)*Z(1,:)

     do i = 1,order
          col1(i) = sum(Z(i,:)*expdiag_Z)
     enddo

end subroutine exponentiate_sym_tridiagonal_col1_slow

subroutine lanczos(dim1,dim2,a,m,diag,offdiag,V,tol,matvec,vecnorm,vecdot)
     use conf
     implicit none
     ! a assumed to have norm 1
     integer, intent(in) :: dim1,dim2
     complex(kind=db), intent(in) :: a(NR,dim1,0:dim2)
     integer, intent(inout) :: m
     real(kind=db), intent(out) :: diag(m+1),offdiag(m)
     complex(kind=db), intent(out) :: V(NR,dim1,0:dim2,m+1)
     external :: matvec
     complex(kind=db), external :: vecdot
     real(kind=db), external :: vecnorm

     complex(kind=db) :: ap(NR,dim1,0:dim2)
     real(kind=db), intent(inout) :: tol

     integer :: i

     real :: t1,t2,tsum

     if (tol .le. 0) tol = 1e-6

     diag = 0
     offdiag = 0

     ap = a

     V(:,:,:,1) = ap

     tsum = 0
     do i = 1,m
          call cpu_time(t1)
          call matvec(ap)
          call cpu_time(t2)
          tsum = tsum + t2-t1

          if (i .gt. 1) ap = ap - offdiag(i-1) * V(:,:,:,i-1)

          diag(i) = real(vecdot(V(:,:,:,i),ap))

          ap = ap - diag(i) * V(:,:,:,i)

          offdiag(i) = vecnorm(ap)

          if (offdiag(i) .lt. tol) then ! happy breakdown *<|:-) Ho ho ho
               m = i
               V(:,:,:,i+1) = 0
               exit
          endif

          ap = ap / offdiag(i)

          V(:,:,:,i+1) = ap

     enddo

     print *, tsum

end subroutine lanczos


subroutine lanczos_block(dim1,order,blocknum,a,m,diag,offdiag,V,beta,tol,matvec_block,vecnorm_block,vecdot_block)
     use conf
     implicit none
     integer, intent(in) :: dim1,order,blocknum
     complex(kind=db), intent(in) :: a(NR,dim1)
     integer, intent(inout) :: m
     real(kind=db), intent(out) :: diag(m+1),offdiag(m),beta
     complex(kind=db), intent(out) :: V(NR,dim1,m+1)

     complex(kind=db) :: ap(NR,dim1)
     real(kind=db), intent(inout) :: tol
     real(kind=db) :: tol2

     integer :: i

     external :: matvec_block
     real(kind=db), external :: vecnorm_block
     complex(kind=db), external :: vecdot_block

     if (tol .le. 0) tol = 1e-6
     
     tol2 = tol
     if (dim1 .eq. num_jJ) tol2 = 1e-4

     beta = vecnorm_block(a,blocknum)
     !if (dim1 .eq. num_jJ) print *, blocknum, beta
     if (beta .lt. tol2) then
          m = 0
          !if (dim1 .eq. num_jJ) print *, 'reject'
          return 
     else
          ap = a / beta
          !if (dim1 .eq. num_jJ) print *, 'accept'
     endif

     diag = 0
     offdiag = 0

     V(:,:,1) = ap

     do i = 1,m
          call matvec_block(ap,blocknum)

          if (i .gt. 1) ap = ap - offdiag(i-1) * V(:,:,i-1)
          
          diag(i) = real(vecdot_block(V(:,:,i),ap,blocknum))

          ap = ap - diag(i) * V(:,:,i)

          offdiag(i) = vecnorm_block(ap,blocknum)

          if (offdiag(i) .lt. tol) then ! happy breakdown *<|:-) Ho ho ho
               m = i
               V(:,:,i+1) = 0
               exit
          endif

          ap = ap / offdiag(i)

          V(:,:,i+1) = ap
     enddo

end subroutine lanczos_block



