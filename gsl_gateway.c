// The program is using the Gnu Scientific Library.
// That library is written in C, so we need wrappers to
// translate the calling conventions between fortran and C.

// Fortran translates all function names into
// lowercase and appends an underscore.
// Also, Fortran is call by reference, so
// all arguments are pointers. C is call by value.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_integration.h>

// Call these functions from fortran without _
// and with normal arguments:

int
gsl_sf_coupling_6j_e_anders(int two_ja, int two_jb, int two_jc,
                            int two_jd, int two_je, int two_jf,
                            gsl_sf_result * result);

double sixjsymbol_(int *j1, int *j2, int *j3, int *j4, int *j5, int *j6) {
     //double tmp;
     //printf("(%i, %i, %i, %i, %i, %i)\n", *j1, *j2, *j3,*j4,*j5,*j6);
     //return gsl_sf_coupling_6j(2*(*j1), 2*(*j2), 2*(*j3), 2*(*j4),2*(*j5),2*(*j6));
     gsl_sf_result result;
     int status;
     status = gsl_sf_coupling_6j_e_anders(2*(*j1), 2*(*j2), 2*(*j3), 2*(*j4),2*(*j5),2*(*j6), &result);

     if (status != GSL_SUCCESS) {
          fprintf(stderr,"GSL OVERFLOW");
          exit(1);
     }
     return result.val;
}

// Wigner 3j symbol
double three_(int *j1, int *j2, int *j3, int *m1, int *m2, int *m3) {
     return gsl_sf_coupling_3j(2*(*j1),2*(*j2),2*(*j3),2*(*m1),2*(*m2),2*(*m3));
}
// Same but with m=0
double three0_(int *j1, int *j2, int *j3) {
     // Apparently we loose some speed here. There is a faster
     // way to calculate (j1,j2,j3,0,0,0) than gsl does.
     return gsl_sf_coupling_3j(2*(*j1),2*(*j2),2*(*j3),0,0,0);
}

// Associated legendre polynomials
double plgndr_(int *l, int *m, double *x) {
     if (*m < 0 || *m > *l || fabs(*x) > 1) {
          fprintf(stderr,"PLGNDR: BAD ARGUMENTS!\n");
          exit(1);
     }
     return gsl_sf_legendre_Plm(*l,*m,*x);
}


// Spherical harmonic Ylm(x,phi), x = cos(theta)
double complex ylmcos_(int *l, int *m, double *x, double *phi) {
     double sphPlgndr;
     double complex result;
     int mm;

     if (*l < 0 || *m < -*l || *m > *l || fabs(*x) > 1) {
          fprintf(stderr,"PLGNDR: BAD ARGUMENTS!\n");
          exit(1);
     }

     mm = *m;
     if (*m < 0)
          mm = -mm;

     sphPlgndr = gsl_sf_legendre_sphPlm(*l,mm,*x);
     // sphPlm uses the spherical harmonic normalization
     // and thereby avoids overflow issues for high l,m.
     
     result = sphPlgndr * cexp(I*(*m)*(*phi));

     // Multiply by -1^|m| if m < 0. (because the legendre polynomials
     // must be calculated for nonnegative m)
     if (*m < 0 && (mm)%2 == 1) result = -result;

     return result;
}


void gauss_legendre_table_(double *a, double *b, size_t *order, double *xi, double *wi) {

     gsl_integration_glfixed_table *table;
     int status;
     size_t i;
     size_t n = *order;
     double a_,b_; // Just to help the optimizer realize the values won't change
     a_ = *a; 
     b_ = *b;

     table = gsl_integration_glfixed_table_alloc(n);

     for (i = 0; i < n; i++) {
     
          status = gsl_integration_glfixed_point(a_,b_,i,&xi[i],&wi[i],table);
          if (status != GSL_SUCCESS) {
             fprintf(stderr,"GAUSS_LEGENDRE_TABLE: gsl_integration failed.\n");
             exit(1);
          }

     }

     gsl_integration_glfixed_table_free(table);
}


