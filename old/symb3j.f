      double precision function symb3j(j1,j2,j3)
      implicit none
C
C      The square of the 3-j symbols of the type 	(j1 j2 j3)^2
C      						               (0  0  0 )
C      Slightly modified from Mikko's version because
C      it calculates the logarithms of the factorials.
C
C      Well --- almost. I have added a bunch of factors
C      at the end, just to avoid having to put them in later.
c
      double precision k(0:180), apu3j, apu, pi4
      parameter (pi4 = 4.d0*3.14159265358979D0)
      integer j,j1,j2,j3,jhalf,jsum
      data k /181*-1.d0/
      save
C
C      Calculate factorials only once. Is this safe ?
C
      if (k(0) .lt.-0.5d0) then
      k(0) = 0.d0
      do 1 j = 1, 180
           k(j)  = dlog(dble(j)) +  k(j-1)
    1        continue
      end if
c
c      testit
c
      if(j1.gt.(j2+j3) .or. j1.lt.abs(j2-j3)) then
        symb3j = 0.d0
        return
      endif
      jsum = j1 + j2 + j3
      if(mod(jsum,2) .ne. 0) then
        symb3j     = 0.d0
        return
        endif
      jhalf = jsum/2
c
      apu3j = k(jsum-2*j1)+k(jsum-2*j2)+k(jsum-2*j3)-k(jsum+1)
      apu = k(jhalf)-(k(jhalf-j1)+k(jhalf-j2)+k(jhalf-j3))
      symb3j = dexp(apu3j+apu+apu)
C
C      From here on we deviate from the straight path. The modification
C      to follow is NOT the correct 3J symbol any more, but rather
C      what I need for the droplet program.
C
      symb3j = (2*j1+1)*(2*j2+1)*(2*j3+1)*symb3j/pi4**3
      return
      end
