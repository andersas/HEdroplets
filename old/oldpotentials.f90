subroutine exponentiate_vmat(JTOT,dtC,vmat,expvmatHalf,expvmat)
  use conf
  integer :: i,ir,info,jt,matsize
  integer :: JTOT
  real(kind=db), intent(in) :: vmat(num_N,num_N,0:JTOTMAX,NR)
  complex(kind=db) :: expw(num_N,num_N)
  complex(kind=db), intent(in) :: dtC
  complex(kind=db), intent(out) :: expvmatHalf(num_N,num_N,0:JTOTMAX,NR), expvmat(num_N,num_N,0:JTOTMAX,NR)
  real(kind=db) :: mat(num_N,num_N),w(num_N),work(num_N*num_N+10)
  if (JTOT > JTOTMAX) then
     write(*,*) 'JTOT given is larger than JTOTMAX. Quitting.'
     stop
  end if
  expw = cmplx(0d0,0d0,kind=db)
  do ir=1,NR
     do jt=0,JTOT,JTOTSTEP
        matsize = indOrder(jt)
        mat(1:matsize,1:matsize) = vmat(1:matsize,1:matsize,jt,ir)
        call DSYEV('V','U',matsize,mat,num_N,w,work,num_N*num_N+10,info);   if(info/=0) call quit('DSYEV failed.')
        ! mat now contains orthonormal eigenvectors of vmat.
        ! w contains the eigenvalues
!do i=1,N
!write(*,'(2I5,99f15.7)') ir,i,w(i)
!enddo
        forall(i=1:matsize) expw(i,i) = exp(-dtC*w(i))
        expvmat(1:matsize,1:matsize,jt,ir) = &
                 matmul(matmul( mat(1:matsize,1:matsize), expw(1:matsize,1:matsize)), transpose(mat(1:matsize,1:matsize)))
        forall(i=1:matsize) expw(i,i) = exp(-dtC*w(i)/2d0)
        expvmatHalf(1:matsize,1:matsize,jt,ir) = &
                 matmul(matmul( mat(1:matsize,1:matsize), expw(1:matsize,1:matsize)), transpose(mat(1:matsize,1:matsize)))
     enddo
  enddo
end subroutine exponentiate_vmat
!


subroutine readvmat(system,vmat,OK)
  use conf
  implicit none
  integer :: xLMAX,xLSTEP,xjMAX,xjSTEP,xLAMAX,xLASTEP,xJTOTMAX,xJTOTSTEP,xNR
  real(kind=db) :: vmat(num_N,num_N,0:JTOTMAX,NR),xDR,xH2Mmol,xH2MHe,xBconst
  logical :: OK
  character(len=*) :: system
  character(len=:), allocatable :: xsystem
  integer :: syslen
  OK = .false.
  open(77,file='vmat.dat',status='old',form='UNFORMATTED',err=99)
  read(77) syslen
  allocate(character(len=syslen) :: xsystem)
  read(77) xsystem
  read(77)xLMAX,xLSTEP,xjMAX,xjSTEP,xLAMAX,xLASTEP,xJTOTMAX,xJTOTSTEP,xNR,xDR,xH2Mmol,xH2MHe,xBconst
  if(xLMAX/=LMAX.or.xLSTEP/=LSTEP.or.xjMAX/=jMAX.or.xjSTEP/=jSTEP.or.xLAMAX/=LAMAX.or.xLASTEP/=LASTEP.or. &
     xJTOTMAX/=JTOTMAX.or.xJTOTSTEP/=JTOTSTEP.or.xNR/=NR.or.abs(xDR-DR)>1d-10.or.abs(xH2Mmol-H2Mmol)>1d-10.or. &
     abs(xH2MHe-H2MHe)>1d-10.or.abs(xBconst-Bconst)>1d-10 .or. xsystem /= system) goto 99
  read(77)vmat(1:num_N,1:num_N,0:JTOTMAX,1:NR)
  OK = .true.
  close(77)
  deallocate(xsystem)
  return

99 write(*,*)'generate vmat:'
   close(77)
end subroutine readvmat
subroutine writevmat(system,vmat)
  use conf
  implicit none
  real(kind=db) :: vmat(num_N,num_N,0:JTOTMAX,NR)
  character(len=*) :: system
  integer :: syslen
  syslen = len(system)
  open(77,file='vmat.dat',status='unknown',form='UNFORMATTED',err=99)
  write(77) syslen ! These must be separate otherwise fortran writes
  write(77) system ! the object as a tuple.
  write(77)LMAX,LSTEP,jMAX,jSTEP,LAMAX,LASTEP,JTOTMAX,JTOTSTEP,NR,DR,H2Mmol,H2MHe,Bconst
  write(77)vmat(1:num_N,1:num_N,0:JTOTMAX,1:NR)
  close(77)
  return

99 write(*,*)'unable to write vmat';stop
end subroutine writevmat
!
subroutine setpotmat(system,vmat)
  use conf
  implicit none
  integer :: i1,i2,ir,j1,j2,l1,l2,la,jt
  real(kind=db) :: vmat(num_N,num_N,0:JTOTMAX,NR), THREE0, sixJsymbol,fac
  real(kind=db) :: v(NR,0:LAMAX)
  real(kind=db) :: r(NR)
  logical :: OK
  character(len=*) :: system

  call readvmat(system,vmat,OK)
  if(OK) return

  call radialpot(system,v) ! Calculate v(r,l)
  ! Then calculate the interaction matrix in coupled basis
  r = (/( ir*DR, ir=1,NR )/)
  vmat = 0d0
  do jt=0,JTOTMAX,JTOTSTEP
print *,jt
     do j1=0,jMAX,jSTEP
        do l1=0,LMAX,LSTEP
           i1 = ind(l1,j1,jt);  if(i1<0) cycle
           do j2=0,jMAX,jSTEP
              do l2=0,LMAX,LSTEP
                 i2 = ind(l2,j2,jt);  if(i2<0) cycle
                 do la = 0, LAMAX, LASTEP
                    if(la<abs(l1-l2).or.la>l1+l2.or.la<abs(j1-j2).or.la>j1+j2) cycle
                    fac = (-1)**(l1+l2+jt)* sqrt(dble( (2*j1+1)*(2*j2+1)*(2*l1+1)*(2*l2+1) )) &
                        * THREE0(l1,la,l2)*THREE0(j1,la,j2)*sixJsymbol(l1,la,l2,j2,jt,j1)
                    if(abs(fac)>1d-10) vmat(i1,i2,jt,1:NR) = vmat(i1,i2,jt,1:NR) + fac * v(1:NR,la)
                 enddo
                 if(i1==i2) then
                    forall(ir=1:NR) vmat(i1,i2,jt,ir) = vmat(i1,i2,jt,ir) + H2Mred*l1*(l1+1)/r(ir)**2 + Bconst*j1*(j1+1)
                 endif
              enddo
           enddo
        enddo
     enddo
  enddo
  call writevmat(system,vmat)
end subroutine setpotmat

subroutine radialpot(system,v)
  use conf
  implicit none
  real(kind=db) :: v(NR,0:LAMAX)
  character(len=*) :: system

  if (system .eq. "CS2") then
     call get_CS2_potential(v) ! LASTEP and LAMAX must be compatible with CS2He
  else if (system .eq. "morse") then
       write(*,*) 'Potential not recognized:', system 
       stop
  else
       write(*,*) 'Potential not recognized:', system 
       stop
  end if 

end subroutine radialpot

subroutine get_CS2_potential(v)
     use conf
     implicit none
     real(kind=db) :: v(NR,0:LAMAX)
     real(kind=db) :: r(NR)
     ! primed (p) variables refer to parameters in the cs2he.vlam file
     integer :: NRp, LAMAXp, LASTEPp,nlp
     real(kind=db), allocatable :: rp(:),ya(:,:),y2a(:,:),yvec(:),p(:)
     integer :: i,ir,l,ii
     real(kind=db) :: rr,rrr
     r = (/( ir*DR, ir=1,NR )/)
     
     open(4210,file='cs2he.vlam',status='old',err=99)
     read(4210,*) NRp,LAMAXp,LASTEPp,nlp
     if(LAMAXp/=LAMAX.or.LASTEPp/=LASTEP) goto 98
     allocate(rp(NRp),ya(NRp,nlp),y2a(NRp,nlp),yvec(nlp),p(LAMAX+1))
     do i=1,nlp
        do ir=1,NRp
          read(4210,*)ii,rp(ir),ya(ir,i),y2a(ir,i)
        enddo
     enddo
     close(4210)

     do ir=1,NR
          rr=r(ir)
          rrr=min(max(rp(1),rr),rp(NRp))
          call splintvecCS2(rp,ya,y2a,NRp,rrr,yvec,nlp)
          i=0
          do l=0,LAMAX,LASTEP
               i=i+1
               v(ir,l)=yvec(i)
          enddo
     enddo
     return
!
99 write(*,*)'CS2He()::file cs2he.vlam not found.';stop
98 write(*,*)'CS2He()::LMAX/=LAMAX.or.LSTEP/=LASTEP';stop
end subroutine get_CS2_potential


subroutine splintvecCS2(xa,ya,y2a,n,x,y,nlam)
      use conf
      implicit none
      integer n,nlam,k,klo,khi,j
      real(kind=db) xa(n),ya(n,nlam),y2a(n,nlam),y(nlam),x,h,a,b
      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.d0) call quit('bad xa input.')
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      do j=1,nlam
         y(j) = a*ya(klo,j)+b*ya(khi,j)+ &
               ((a**3-a)*y2a(klo,j)+(b**3-b)*y2a(khi,j))*(h**2)/6.d0
      enddo
      return
end




