C       adapted from Numerical Recipes:
C       associated Legendre polynomials according to Num. Rec. definition,
C       which differs by a factor (-1)**M
C
      double precision FUNCTION PLGNDR(L,M,X)
      implicit double precision (a-h,o-z)
      IF(M.LT.0 .OR. M.GT.L. OR. ABS(X).GT.1.) THEN
        WRITE(6,*) 'PLGNDR bad arguments'
        STOP
      END IF
      PMM = 1.
      IF(M.GT.0) THEN
        SOMX2 = SQRT((1.-X)*(1.+X))
        FACT = 1.
        DO I = 1, M
          PMM = -PMM*FACT*SOMX2
          FACT = FACT+2.
        END DO
      ENDIF
      IF(L.EQ.M) THEN
        PLGNDR = PMM
      ELSE
        PMMP1 = X*(2*M+1)*PMM
        IF(L.EQ.M+1) THEN
          PLGNDR = PMMP1
        ELSE
          DO LL = M+2, L
            PLL = (X*(2*LL-1)*PMMP1-(LL+M-1)*PMM)/(LL-M)
            PMM = PMMP1
            PMMP1 = PLL
          END DO
          PLGNDR = PLL
        ENDIF
      ENDIF
      RETURN
      END
C
C**********************************************************************
C
      complex*16 FUNCTION YLMcos (L,M,X,PH)
C       spherical harmonic Y_lm(theta,phi); X=cos(theta)
      implicit none
      double precision X,PH,PLGNDR,logfac
      integer L,M,MM
C
      IF(M.LT.-L .OR. M.GT.L. OR. ABS(X).GT.1.) THEN
        WRITE(*,*) 'YLMcos bad arguments:',L,M,X,PH
        STOP
      END IF
      MM = M
      IF( M.LT.0 )  MM = -M
      YLMcos = PLGNDR(L,MM,X)*cdexp(dcmplx(0d0,M*PH)) *
     *               dsqrt((2d0*L+1d0) *
     *                 dexp(logfac(l-mm)-logfac(l+mm))
     *                /4d0/3.14159265358979323846d0)
      IF( M.LT.0 ) YLMcos = YLMcos * (-1)**MM
      RETURN
      END
C
C**********************************************************************
C
      double precision FUNCTION logfac(n)
C       log(n!); if n<= 0, logfac = log(1) = 0
      implicit none
      integer n,i,Nlnfac,Nlnfac1
      parameter (Nlnfac = 280, Nlnfac1 = Nlnfac+1)
      double precision lnfac(0:Nlnfac)
      data  lnfac /Nlnfac1*-1d0/
      save
C
C       (this method by kro)
      if( lnfac(0) .lt. -0.5d0 ) then !initialize
         lnfac(0) = 0d0
         do i = 1,Nlnfac
            lnfac(i) = lnfac(i-1) + dlog(dble(i))
         enddo
      endif
      if( abs(n).gt.Nlnfac ) stop
      logfac = 0d0
      if( n.le.0 ) return
      logfac = lnfac(n)
      return
      end
C
C**********************************************************************
C
      double precision FUNCTION THREE0(l1,l2,l3)
C       3j Symbol with m's = 0 as defined in Edmonds (not factor
C       from symb3j() and not squared!)  for EVEN l's
      implicit none
      double precision symb3j,pi
      parameter (pi = 3.14159265358979323846d0)
      integer l1,l2,l3
C
      THREE0 = symb3j(l1,l2,l3)*(4d0*pi)**3/
     *           ((2d0*l1+1d0)*(2d0*l2+1d0)*(2d0*l3+1d0))
      THREE0 = dsqrt(THREE0) * (-1)**((l1+l2+l3)/2) ! right sign !
      return
      end
C
C**********************************************************************
C
      double precision FUNCTION THREE(j1,j2,j3,m1,m2,m3)
C       3j Symbol with arb. m's as defined in Edmonds (not factor
C       from symb3j() and not squared!)  for EVEN l's
      implicit none
      double precision pi,CLEB
      parameter (pi = 3.14159265358979323846d0)
      integer j1,j2,j3,m1,m2,m3
C
      THREE = (-1)**(j1-j2-m3)*CLEB(j1,m1,j2,m2,j3,-m3)
      THREE = THREE/dsqrt(2d0*j3+1d0)
      return
      end
C
C**********************************************************************
C
      double precision FUNCTION CLEB(j1,m1,j2,m2,j,m)
C       Clebsch-Gordon as defined in Weissbluth (1.5-37) for EVEN j's
      implicit none
      double precision logfac,sum,help
      integer j1,j2,m1,m2,j,m,k
      logical triang
C       usink ze formlik (3.6.11), Edmonds or (1.5-37) Weissbluth.
C       not usink rekurrenc formliki!
C       (not likink convoluted rekurrenc); plus formlik (3.7.3) or
C       (1.5-47), da.
C       [ Edmonds (3.6.10) is faster though; but (3.6.11) has same
C       number of factorials in nom. and denom.]
      CLEB = 0d0
      if( (m1+m2-m).ne.0 ) return
      if( .not.triang(j1,j2,j) ) return
      if( abs(m1).gt.j1 ) return
      if( abs(m2).gt.j2 ) return
      if( abs(m ).gt.j  ) return
C
      sum = 0
      do k = 0, MIN(j1+j2-j,j1-m1,j2+m2)
        if( (j-j2+m1+k).lt.0  .OR.  (j-j1-m2+k).lt.0 ) goto 10
        help = (logfac(j1+m1)+logfac(j1-m1)+logfac(j2+m2)+logfac(j2-m2)
     *   +logfac(j+m)   +logfac(j-m))*0.5d0
     *   -logfac(k) -logfac(j1+j2-j-k) -logfac(j1-m1-k) -logfac(j2+m2-k)
     *   -logfac(j-j2+m1+k) -logfac(j-j1-m2+k)
        sum = sum + (-1)**k * dexp(help)
 10        continue
      enddo
      CLEB = sum* dexp(0.5d0*(
     *    logfac(j1+j2-j) +logfac(j+j1-j2) +logfac(j+j2-j1)
     *   -logfac(j+j1+j2+1) ) ) * dsqrt(2d0*j+1d0)
      return
      end
C
C**********************************************************************
C
      logical FUNCTION triang(j1,j2,j)
C       triangle condition weissbluth (1.5-40) for EVEN j's
      implicit none
      integer j1,j2,j
      triang = .FALSE.
      if( ( j1+j2-j).lt.0 ) return
      if( ( j1-j2+j).lt.0 ) return
      if( (-j1+j2+j).lt.0 ) return
C       equivalent to |j1-j2| <= j <= j1+j2
      triang = .TRUE.
      return
      end
