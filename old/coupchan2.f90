! solve the coupled channel eq. (12) of hutsonJCP93
! lilli:
! ifort -O2 coupchan.f90 heacetpotzil.f HeHCN.f heocspot.f sixJsymbol.f90 ../ylm.f ../symb3j.f -I/apps/FFTW3 -L/apps/FFTW3 -lfftw3 -L/opt/intel/mkl/9.0/lib/64 -lmkl -lguide -lmkl_lapack64 -o coupchan
! mac:
! gfortran -O2 coupchan.f90 heacetpotzil.f HeHCN.f heocspot.f sixJsymbol.f90 ../ylm.f ../symb3j.f -I/usr/local/include/ -L/usr/local/lib/ -lfftw3 ~/Downloads/lapack-3.4.2/liblapack.a -framework accelerate -o coupchan
! dino:
! gfortran -O2 coupchan.f90 heacetpotzil.f HeHCN.f heocspot.f sixJsymbol.f90 ../ylm.f ../symb3j.f -I/usr/include/ -L/usr/lib/ -lfftw3 -llapack -o coupchan
module global
  implicit none
  real*8, parameter :: PI = 3.14159265358979323d0
  real*8, parameter :: HR = 0.16d0
  real*8, parameter :: DT = 0.002d0
!  real*8, parameter :: H2Mmol = 0.93246d0, H2M = 6.059615d0, Bconst = 1.6931d0 ! HCCH
!  real*8, parameter :: H2Mmol = 0.4038d0, H2M = 6.059615d0, Bconst = 0.2918d0 ! OCS
!  real*8, parameter :: H2Mmol = 0.3d0, H2M = 6.059615d0, Bconst = 0.1d0
  real*8, parameter :: H2Mmol = 0.3189d0, H2M = 6.059615d0, Bconst = 0.1570d0
  real*8, parameter :: pulsestrength = 3d0
  integer, parameter :: NT1 = 500, NT2 = 1000, NTblock = 50
  integer, parameter :: LMAX = 16, LSTEP = 2, jMAX = 16, jSTEP = 2, LAMAX = 16, LASTEP = 2, JTOTMAX = 8, JTOTSTEP = 2
  integer, parameter :: NR = 63 !(2^m-1)
  integer, parameter :: NCO = 100
  integer :: itype,N,JTOT,NJ(0:JTOTMAX),ind(0:LMAX,0:jMAX,0:JTOTMAX)
  complex*16 :: dtC
  real*8 :: H2Mred,HK
  real*8, allocatable :: r(:),k(:),v(:,:)
  complex*16, allocatable :: expkin(:), expvmat(:,:,:,:),expvmatHalf(:,:,:,:), cosdist(:)
contains
subroutine init
  real*8 :: vlr
  integer :: ir,la,i,j,l,jt
!
  itype = 3
!
  print *,'LMAX =',LMAX ,', jMAX =',jMAX ,', LAMAX =',LAMAX ,', JTOTMAX =',JTOTMAX
  print *,'LSTEP=',LSTEP,', jSTEP=',jSTEP,', LASTEP=',LASTEP,', JTOTSTEP=',JTOTSTEP
  print *,'NR=',NR,', itype=',itype
  print *,'DT=',DT,', HR=',HR,', pulsestrength=',pulsestrength
  print *,'H2Mmol=',H2Mmol,', Bconst=',Bconst
!
  H2Mred = H2M + H2Mmol
  HK = 2d0*PI/(2d0*(NR+1)*HR)
  N = (LMAX+1)*(jMAX+1)
  allocate(v(NR,0:LAMAX),r(NR),k(NR),expkin(NR),expvmat(N,N,0:JTOTMAX,NR),expvmatHalf(N,N,0:JTOTMAX,NR),cosdist(0:NCO))
  r = (/( ir*HR, ir=1,NR )/)
  k = (/( ir*HK, ir=1,NR )/)
  if(itype==3) then
     call CS2He(r,NR,LAMAX,LASTEP,v) ! LASTEP and LAMAX must be compatible with CS2He
  else
     do la=0,LAMAX, LASTEP
        do 1 ir=1,NR
1          v(ir,la) = Vlr(itype,r(ir),la)
     enddo
  endif
  do la=0,LAMAX, LASTEP
     do 2 ir=1,NR
2       write(1101,'(I5,99e15.6)')la,r(ir),v(ir,la)
     write(1101,*);write(1101,*)
  enddo
!
  ind = -1
  do 9 jt=0,JTOTMAX,JTOTSTEP
     i = 0
     do 9 j=0,jMAX,jSTEP
        do 9 l=0,LMAX,LSTEP
           if(abs(l-j)>jt.or.l+j<jt) cycle
           i = i+1
           NJ(jt) = i
9          ind(l,j,jt) = i
end subroutine init
!
subroutine readvmat(vmat,OK)
  integer :: xLMAX,xLSTEP,xjMAX,xjSTEP,xLAMAX,xLASTEP,xJTOTMAX,xJTOTSTEP,xNR
  real*8 :: vmat(N,N,0:JTOTMAX,NR),xHR,xH2Mmol,xH2M,xBconst
  logical :: OK
  OK = .false.
  open(77,file='vmat.dat',status='old',form='UNFORMATTED',err=99)
  read(77)xLMAX,xLSTEP,xjMAX,xjSTEP,xLAMAX,xLASTEP,xJTOTMAX,xJTOTSTEP,xNR,xHR,xH2Mmol,xH2M,xBconst
  if(xLMAX/=LMAX.or.xLSTEP/=LSTEP.or.xjMAX/=jMAX.or.xjSTEP/=jSTEP.or.xLAMAX/=LAMAX.or.xLASTEP/=LASTEP.or. &
     xJTOTMAX/=JTOTMAX.or.xJTOTSTEP/=JTOTSTEP.or.xNR/=NR.or.abs(xHR-HR)>1d-10.or.abs(xH2Mmol-H2Mmol)>1d-10.or. &
     abs(xH2M-H2M)>1d-10.or.abs(xBconst-Bconst)>1d-10) goto 99
  read(77)vmat(1:N,1:N,0:JTOTMAX,1:NR)
  OK = .true.
  close(77)
  return

99 write(*,*)'generate vmat:'
   close(77)
end subroutine readvmat
subroutine writevmat(vmat)
  real*8 :: vmat(N,N,0:JTOTMAX,NR)
  open(77,file='vmat.dat',status='unknown',form='UNFORMATTED',err=99)
  write(77)LMAX,LSTEP,jMAX,jSTEP,LAMAX,LASTEP,JTOTMAX,JTOTSTEP,NR,HR,H2Mmol,H2M,Bconst
  write(77)vmat(1:N,1:N,0:JTOTMAX,1:NR)
  close(77)
  return

99 write(*,*)'unable to write vmat';stop
end subroutine writevmat
!
subroutine setpotmat(vmat)
  integer :: i1,i2,ir,j1,j2,l1,l2,la,jt
  real*8 :: vmat(N,N,0:JTOTMAX,NR), THREE0, sixJsymbol,fac
  logical :: OK
  call readvmat(vmat,OK)
  if(OK) return
  vmat = 0d0
  do 4 jt=0,JTOTMAX,JTOTSTEP
print *,jt
     do 4 j1=0,jMAX,jSTEP
        do 4 l1=0,LMAX,LSTEP
           i1 = ind(l1,j1,jt);  if(i1<0) cycle
           do 4 j2=0,jMAX,jSTEP
              do 4 l2=0,LMAX,LSTEP
                 i2 = ind(l2,j2,jt);  if(i2<0) cycle
                 do la = 0, LAMAX, LASTEP
                    if(la<abs(l1-l2).or.la>l1+l2.or.la<abs(j1-j2).or.la>j1+j2) cycle
                    fac = (-1)**(l1+l2+jt)* sqrt(dble( (2*j1+1)*(2*j2+1)*(2*l1+1)*(2*l2+1) )) &
                        * THREE0(l1,la,l2)*THREE0(j1,la,j2)*sixJsymbol(l1,la,l2,j2,jt,j1)
                    if(abs(fac)>1d-10) vmat(i1,i2,jt,1:NR) = vmat(i1,i2,jt,1:NR) + fac * v(1:NR,la)
                 enddo
                 if(i1==i2) then
                    forall(ir=1:NR) vmat(i1,i2,jt,ir) = vmat(i1,i2,jt,ir) + H2Mred*l1*(l1+1)/r(ir)**2 + Bconst*j1*(j1+1)
                 endif
4 continue
  call writevmat(vmat)
end subroutine setpotmat
!
subroutine diagpotmat(vmat)
  integer :: i,ir,info,jt
  real*8 :: vmat(N,N,0:JTOTMAX,NR)
  complex*16, allocatable :: expw(:,:)
  real*8, allocatable :: mat(:,:),w(:),work(:)
  allocate(w(N),work(N*N+10),expw(N,N),mat(N,N))
  expw = cmplx(0d0,0d0)
  do ir=1,NR
     do jt=0,JTOT,JTOTSTEP
        mat(1:NJ(jt),1:NJ(jt)) = vmat(1:NJ(jt),1:NJ(jt),jt,ir)
        call DSYEV('V','U',NJ(jt),mat,N,w,work,N*N+10,info);   if(info/=0) call quit('DSYEV failed.')
!do i=1,N
!write(*,'(2I5,99f15.7)') ir,i,w(i)
!enddo
        forall(i=1:NJ(jt)) expw(i,i) = exp(-dtC*w(i))
        expvmat(1:NJ(jt),1:NJ(jt),jt,ir) = &
                 matmul(matmul( mat(1:NJ(jt),1:NJ(jt)) , expw(1:NJ(jt),1:NJ(jt))), transpose(mat(1:NJ(jt),1:NJ(jt))))
        forall(i=1:NJ(jt)) expw(i,i) = exp(-dtC*w(i)/2d0)
        expvmatHalf(1:NJ(jt),1:NJ(jt),jt,ir) = &
                 matmul(matmul( mat(1:NJ(jt),1:NJ(jt)) , expw(1:NJ(jt),1:NJ(jt))), transpose(mat(1:NJ(jt),1:NJ(jt))))
     enddo
  enddo
  deallocate(w,work,expw,mat)
end subroutine diagpotmat
!
subroutine timeblock(NTblock,f)
  integer :: NTblock,it,ir,i,jt
  complex*16 :: f(NR,N,0:JTOTMAX)
  do jt=0,JTOT,JTOTSTEP
     forall(ir=1:NR) f(ir,1:NJ(jt),jt) = matmul(expvmatHalf(1:NJ(jt),1:NJ(jt),jt,ir),f(ir,1:NJ(jt),jt))
     do it=1,NTblock
        if(it>1) forall(ir=1:NR) f(ir,1:NJ(jt),jt) = matmul(expvmat(1:NJ(jt),1:NJ(jt),jt,ir),f(ir,1:NJ(jt),jt))
        do 1 i=1,NJ(jt)
1          call fftNN('r2k',NR,f(1,i,jt),f(1,i,jt))
        forall(i=1:NJ(jt)) f(1:NR,i,jt) = expkin(1:NR)*f(1:NR,i,jt)
        do 2 i=1,NJ(jt)
2          call fftNN('k2r',NR,f(1,i,jt),f(1,i,jt))
     enddo
     forall(ir=1:NR) f(ir,1:NJ(jt),jt) = matmul(expvmatHalf(1:NJ(jt),1:NJ(jt),jt,ir),f(ir,1:NJ(jt),jt))
  enddo
end subroutine timeblock
!
subroutine printf(f,time,ifil)
  integer :: ifil,jt,l1,j1,ir,i1
  complex*16 :: f(NR,N,0:JTOTMAX)
  real*8 :: time
  do 2 jt=0,JTOT,JTOTSTEP
     do 2 j1=0,jMAX,jSTEP
        do 2 l1=0,LMAX,LSTEP
           i1 = ind(l1,j1,jt);  if(i1<0) cycle
           do 1 ir=1,NR
1             write(ifil,'(f13.5,4I5,99f15.8)')time,jt,j1,l1,i1,r(ir),f(ir,i1,jt)
           write(ifil,*)
2 continue
end subroutine printf
!
real*8 function normf(f)
  integer :: jt,l1,j1,i1
  complex*16, intent(in) :: f(NR,N,0:JTOTMAX)
  normf = 0d0
  do 2 jt=0,JTOT,JTOTSTEP
     do 2 j1=0,jMAX,jSTEP
        do 2 l1=0,LMAX,LSTEP
           i1 = ind(l1,j1,jt);  if(i1<0) cycle
           normf = normf + sum(f(:,i1,jt)*conjg(f(:,i1,jt)))
2 continue
  normf = sqrt(normf*HR)
end function normf

complex*16 function dot_product(a,b)
     integer :: jt, l1, j1, i1
     complex*16, dimension(NR,N,0:JTOTMAX),intent(in) :: a,b
     dot_product = 0d0

     do jt=0,JTOT,JTOTSTEP
          do j1=0,jMax,jSTEP
               do l1=0,LMAX,LSTEP
                    i1 = ind(l1,j1,jt); if (i1<0) cycle
                    dot_product = dot_product + sum(a(:,i1,jt)*conjg(a(:,i1,jt))
               enddo
          enddo
     enddo
     dot_product = dot_product*NR;              

end function dot_product

end module global
! ============================================================================================
program asdf
  use global
  implicit none
  integer :: info,it,i,ir
  real*8 :: s,t,normf
  real*8, allocatable :: vmat(:,:,:,:)
  complex*16, allocatable :: f(:,:,:)
  call init;                                  print *,'... init done.'
  allocate(vmat(N,N,0:JTOTMAX,NR),f(NR,N,0:JTOTMAX));   f=cmplx(0d0,0d0)
  print *,'vmat size:',N*N*NR*JTOTMAX
  call setpotmat(vmat);                       print *,'... potmat set up.'
!
  JTOT = 0
!
  dtC = DT * cmplx(1d0,0d0)
  expkin = (/( exp(-H2Mred*k(ir)**2*dtC), ir=1,NR )/)
  call diagpotmat(vmat);                      print *,'... potmat diagonalized.'
!
! imaginary time propagation: ----------------
  forall(i=1:NJ(0)) f(1:NR,i,0) = (/( cmplx( r(ir)*exp(-r(ir)/4d0)*(NR*HR-r(ir)),0d0) , ir=1,NR )/)
  s = sqrt(sum(f*conjg(f))*HR);  f = f/s
  do it=0,NT1,NTblock
     t = it*DT
     print *,'it=',it
     call timeblock(NTblock,f)
     s = sqrt(sum(f*conjg(f))*HR) ! norm is sum over all channels of \int |chi|^2
!     s = normf(f)
     f = f/s
     if(imag(dtC)<1d-20) write(110,*) DT,DT*it,-log(s)/(DT*NTblock)
     call printcosdist(f,t,121,131)
  enddo
  call printf(f,0d0,101); call flush(110); call flush(101)
!
! short pulse limit: -------------------------
  call LLJNtoLMLM(f)
  call pulse(f);                              print *,'... applied pulse.'
  call printf(f,0d0,102)
  call LMLMtoLLJN(f)
  call printcosdist(f,0d0,122,132)
!
  JTOT = JTOTMAX
  call printf(f,0d0,1030)
!
  dtC = DT * cmplx(0d0,1d0)
  expkin = (/( exp(-H2Mred*k(ir)**2*dtC), ir=1,NR )/)
!!!  call setpotmat(vmat);                       print *,'... potmat set up.'
  call diagpotmat(vmat);                      print *,'... potmat diagonalized.'
!
! real time propagation: ----------------------
  do it=0,NT2,NTblock
     t = it*DT
print *,'   1...'
     call printcosdist(f,t,123,133); call flush(133)
print *,'...2...'
!call printf(f,t,103)
     call timeblock(NTblock,f)
print *,'...3'
     s = normf(f)
     !s = sqrt(sum(f*conjg(f))*HR) ! norm is sum over all channels of \int |chi|^2
     print *,'it=',it,s
  enddo
  call printf(f,0d0,103)
end program asdf
! ============================================================================================
subroutine printcosdist(f,time,ifil,jfil)
  use global
  implicit none
  integer :: jt1,jt2,j1,j2,l,ll,i1,i2,m,ix,ifil,jfil
  complex*16 :: f(NR,N,0:JTOTMAX), ylmcos,s,t
  complex*16, allocatable, save :: ylmarray(:,:,:)
  real*8 :: THREE,x,time,cos2,norm
  integer, save :: ISET = 0
  if(ISET == 0) then
     ISET = 1
     allocate(ylmarray(0:NCO,0:jMAX,-jMAX:jMAX))
     do 11 j1=0,jMAX,jSTEP
        do 11 m = -j1, j1
           do 11 ix=0,NCO
              x = dble(ix)*2d0/dble(NCO)-1d0
11            ylmarray(ix,j1,m) = ylmcos(j1,m,x,0d0)
  endif
!
  cosdist = 0d0
  do 1 jt1=0,JTOT,JTOTSTEP
     do 1 jt2=0,JTOT,JTOTSTEP
        do 1 l=0,LMAX,LSTEP
           do 1 j1=0,jMAX,jSTEP
               i1 = ind(l,j1,jt1);  if(i1<0) cycle
               do 1 j2=0,jMAX,jSTEP
                  i2 = ind(l,j2,jt2);  if(i2<0) cycle
                  s = sum(conjg(f(1:NR,i1,jt1))*f(1:NR,i2,jt2))*HR
                  do 1 m = -min(j1,j2), min(j1,j2)
                     t = THREE(j1,l,jt1,m,-m,0) * THREE(j2,l,jt2,m,-m,0) * (-1)**(j1+j2) * sqrt(2d0*jt1+1d0)*sqrt(2d0*jt2+1d0)
                     do 1 ix=0,NCO
1                       cosdist(ix) = cosdist(ix) + s * t * conjg(ylmarray(ix,j1,m))*ylmarray(ix,j2,m)
!
  cos2 = 0d0;  norm = 0d0
  do 2 ix=0,NCO
     x = dble(ix)*2d0/dble(NCO)-1d0
     if(ix<NCO) cos2 = cos2 + cosdist(ix)*x**2
     if(ix<NCO) norm = norm + cosdist(ix)
2    write(ifil,'(99f15.7)')time,x,cosdist(ix)
  write(ifil,*)
  write(jfil,'(99f15.7)')time,cos2,norm
end subroutine printcosdist
!
subroutine pulse(f)  ! we assume JTOT=0 before pulse (ground state, T=0K)
  use global
  implicit none
  integer, parameter :: KD=2
  integer :: j,lj,li,l,ir,INFO,i
  complex*16 :: f(NR,N,0:JTOTMAX)
  real*8 :: THREE0
  real*8, allocatable :: AB(:,:),Z(:,:),D(:),work(:)
  complex*16, allocatable :: expd(:),fh(:,:)
  if(JTOT/=0) call quit('JTOT /= 0, stopped.')
  allocate(AB(KD+1,1+jMAX),Z(1+jMAX,1+jMAX),D(1+jMAX),work(3*(1+jMAX)),expd(1+jMAX),fh(1+jMAX,0:LMAX))
  AB = 0d0
  do j=0,jMAX
    lj = min(j , 80)   !THREE0 would give NaN's and AB becomes independent of j anyway.
    AB(1,j+1) = (2*lj+1)*THREE0(lj,2,lj)**2 + (2*lj+1)*THREE0(lj,0,lj)**2/2d0
    li=lj+2
    AB(3,j+1) = sqrt((2d0*li+1)*(2d0*lj+1))*THREE0(li,2,lj)**2
  enddo
  call DSBEV( 'V','L', 1+jMAX, KD, AB, KD+1, D, Z, 1+jMAX, WORK, INFO )
  if(INFO/=0) then
     write(*,*)INFO;call quit('pulse()::DSBEV failed, stopped.')
  endif
  forall(j=0:jMAX) expd(j+1) = exp(cmplx(0d0,-pulsestrength *D(j+1)))
!
  do ir=1,NR
     fh = 0d0
     do 1 j=0,jMAX,jSTEP
        do 1 l=0,LMAX,LSTEP
           i = ind(l,j,0);  if(i<0) cycle
           fh(j+1,l) = f(ir,i,0)
1    continue
     do l=0,LMAX,LSTEP
        fh(1:jMAX+1,l) = matmul(fh(1:jMAX+1,l),Z)
        fh(1:jMAX+1,l) = expd*fh(1:jMAX+1,l)
        fh(1:jMAX+1,l) = matmul(Z,fh(1:jMAX+1,l))
     enddo
     i=0
     do 2 j=0,jMAX,jSTEP
        do 2 l=0,LMAX,LSTEP
           i=i+1
2          f(ir,i,0) = fh(j+1,l) ! must not use ind(...) here!
  enddo
  deallocate(AB,Z,D,work,expd,fh)
end subroutine pulse
!
subroutine LLJNtoLMLM(f)
  use global
  implicit none
  integer :: i,j,l
  complex*16 :: f(NR,N,0:JTOTMAX)
  if(JTOT/=0) call quit('JTOT /= 0, stopped.')
  ! ...for JTOT = 0, LLJN and LMLM basis are proportional
  do 1 j=0,jMAX,jSTEP
     do 1 l=0,LMAX,LSTEP
        i = ind(l,j,0);  if(i<0) cycle
1       f(1:NR,i,0) = f(1:NR,i,0)/sqrt(2d0*l+1d0)*(-1)**l ! *(-1)**m, but we don't store this trivial m dep.,
                                                          ! because pulse does not act on m
end subroutine LLJNtoLMLM
!
subroutine LMLMtoLLJN(f) ! f is a_LMlm(r), but now l /= L  ==>  J /= 0
  use global
  implicit none
  integer :: jt,i,i2,l,j,m
  real*8 :: THREE,s
  complex*16 :: f(NR,N,0:JTOTMAX)
  if(JTOT/=0) call quit('JTOT /= 0, stopped.')
  do 1 jt=JTOTMAX,0,-JTOTSTEP ! don't overwrite jtot=0 until the end
     i = 0; i2 = 0
     do 1 j=0,jMAX,jSTEP
        do 1 l=0,LMAX,LSTEP
           i2= i2+1
           i = ind(l,j,jt);  if(i<0) cycle
           s=0d0
           do 2 m=-j,j
2             s = s + (-1)**m*THREE(j,l,jt,m,-m,0) ! (-1)**m is m-dep. of f, see LLJNtoLMLM(f)
1          f(1:NR,i,jt) = f(1:NR,i2,0)*sqrt(2d0*jt+1d0)*(-1)**(j-l) * s
end subroutine LMLMtoLLJN
!
real*8 function Vlr(itype,r,ll)
  implicit none
  real*8, parameter :: oneHartreeIsKelvin = 315773.218d0
  real*8, parameter :: oneBohrIsAng = 0.529177d0
  real*8, parameter :: fromCmtoKelvin = 1.43876864d0
  real*8, parameter :: sigma1=4d0,eps1=100d0, oneover=2d0
  integer, parameter :: ntheta = 1000
  real*8 :: pot,dcth,cth,ff,r,rau,plgndr,x,morse
  integer :: itype,ll,ith
  vlr = 0d0
  dcth = 2d0/dble(ntheta)
  if(itype<0) then ! neg. types are simple models
     if(itype==-1) then 
        if(ll/=0.and.ll/=2) return
        ff = sqrt(oneover)**ll ! ll=0: 1; ll=2: 2
        x = (sigma1 / max(r,0.2d0*sigma1))**6
        vlr = 4d0*eps1/ff*(x**2-x)               * real(cmplx(0d0,1d0)**ll)
     elseif(itype==-2) then ! Morse
        do ith=0,ntheta
           ff = 1d0;  if(ith==0.or.ith==ntheta) ff=0.5d0
           cth = ith*dcth - 1d0
           vlr = vlr + plgndr(ll,0,cth)*morse(r,cth)*ff
        enddo
        vlr = vlr * dcth * (2d0*ll+1d0)/2d0
     endif
  else
     rau = r/oneBohrIsAng
     do ith=0,ntheta
        ff = 1d0;  if(ith==0.or.ith==ntheta) ff=0.5d0
        cth = ith*dcth - 1d0
        if(itype==1) then ! HCCH
           call POTXC(rau,cth,pot)
           pot = pot*oneHartreeIsKelvin
        elseif(itype==2) then ! OCS (higgins)
           call fran_heocspot(r,cth,pot,1)
        endif
        vlr = vlr + plgndr(ll,0,cth)*pot*ff
     enddo
     vlr = vlr * dcth * (2d0*ll+1d0)/2d0
     if(r<2.5d0) vlr = 1d3
  endif
end function Vlr
!----------------------------------------------------------------------
subroutine CS2He(rex,NRex,LAMAX,LASTEP,v)
  implicit none
  integer :: NRex,LAMAX,LASTEP,i,ir,l,ii
  real*8 :: rr,rrr,cth,v(NRex,0:LAMAX),rex(NRex)
  integer, save :: ifirst=1, nr,nl,LSTEP,LMAX
  real*8, save, allocatable :: r(:),ya(:,:),y2a(:,:),yvec(:),p(:)
  if(ifirst==1) then
     ifirst=0
     open(4210,file='cs2he.vlam',status='old',err=99)
     read(4210,*)nr,LMAX,LSTEP,nl
     if(LMAX/=LAMAX.or.LSTEP/=LASTEP) goto 98
     allocate(r(nr),ya(nr,nl),y2a(nr,nl),yvec(nl),p(LMAX+1))
     do 1 i=1,nl
        do 1 ir=1,nr
1          read(4210,*)ii,r(ir),ya(ir,i),y2a(ir,i)
     close(4210)
  endif
  do ir=1,NRex
     rr=rex(ir)
     rrr=min(max(r(1),rr),r(nr))
     call splintvecCS2(r,ya,y2a,nr,rrr,yvec,nl)
     i=0
     do l=0,LMAX,LSTEP
        i=i+1
        v(ir,l)=yvec(i)
     enddo
  enddo
  return
!
99 write(*,*)'CS2He()::file cs2he.vlam not found.';stop
98 write(*,*)'CS2He()::LMAX/=LAMAX.or.LSTEP/=LASTEP';stop
end subroutine CS2He
!
      subroutine splintvecCS2(xa,ya,y2a,n,x,y,nlam)
      implicit none
      integer n,nlam,k,klo,khi,j
      real*8 xa(n),ya(n,nlam),y2a(n,nlam),y(nlam),x,h,a,b
      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.d0) pause 'bad xa input.'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      do j=1,nlam
         y(j) = a*ya(klo,j)+b*ya(khi,j)+ &
               ((a**3-a)*y2a(klo,j)+(b**3-b)*y2a(khi,j))*(h**2)/6.d0
      enddo
      return
      end
!----------------------------------------------------------------------
!       sin transform, non-normalized, therefore x2y does not matter
subroutine fftNN(x2y,N,a,b)
  implicit none
  include 'fftw3.f'
  complex*16 :: a(N),b(N)
  real*8, save, allocatable :: c(:),cr(:)
  integer*8, save :: ISET = 0 , plan
  integer :: i,N
  character*3 x2y
  if( ISET==0 ) then
    ISET = 1
    allocate (c(N),cr(N))
    call dfftw_plan_r2r_1d(plan,N, c, c, FFTW_RODFT00, FFTW_MEASURE)
  endif
  c = real(a)
!  call dfftw_execute_r2r(plan,c,c) ! fftw: sin-trafo with factor 2 !!!
  call dfftw_execute(plan) ! fftw: sin-trafo with factor 2 !!!
  cr = c
  c = imag(a)
!  call dfftw_execute_r2r(plan,c,c)
  call dfftw_execute(plan)
  b = cmplx(cr,c) / sqrt(2d0*dble(N+1))
end subroutine fftNN
!----------------------------------------------------------------------
	subroutine quit(str)
	character(*) :: str
	write(*,*) str;   stop
	end subroutine quit
!----------------------------------------------------------------------
real*8 function ljpot(r,co)
  implicit none
  real*8 r,co,eps,sig,leg2,x
  leg2 = 0.5d0*(3d0*co**2-1d0)
  eps = 1d0 - 0.4d0*leg2
  sig = 1d0 + 0.4d0*leg2
  x = (sig/r)**6
  ljpot = 4d0*eps*(x**2-x)
end function ljpot
!----------------------------------------------------------------------
real*8 function morse(r,co)
  implicit none
  real*8 r,co,eps,r0,a,leg2,x
  leg2 = 0.5d0*(3d0*co**2-1d0) ! P_2 legendre polynomial
  eps = 5d0 * (1d0 - 0.6d0*leg2)
  r0  = 1d0 * (1d0 + 0.3d0*leg2)
  a   = 2d0
  morse = eps*((1d0-exp(-a*(r-r0)))**2-1d0)
end function morse
