      subroutine HeHCN(rr,dtheta,pot,iso)
c To generate the 1st type of anisotropic potential between He and HCN
c        proposed by Atkins and Hutson ; JCP vol.105, 440 (1996).
c iso=0, full anisotropic potential: iso=1, isotropic potential
c INPUT: rr in Angstrom, ctheta=cos(theta)
c OUTPUT: pot in units of Kelvin
c Inside this routine, 
c       He-HCN potential in atomic units; lenght in a_B, energy in Hartree
      implicit none
      real*8 bohr,hartree, dnOPT2, plgndr
      real*8 rr,ctheta,pot,alpHe,diHCN,quHCN,c60,c62,c71,c73
      real*8 ep0,ep1,ep2,ep3,rm0,rm1,rm2,rm3,beta
      real*8 r,r2,r6,vind1,vind2,vind,eps,rmin,rminbeta,rbeta
     +,rmin6,rmin7,rmin8,c6,c7,c8,d6rm,dd6rm,d7rm,dd7rm
     +,d8rm,dd8rm,vfix,dvfix,aa,vrep,vdisp,dtheta,c80,a0
      integer l,iso
!      real*8 fla(8),factln
      data bohr/0.529177d0/,hartree/3.157872d05/
      data alpHe/1.383d0/, diHCN/1.174d0/, quHCN/1.777d0/
      data c60/13.067d0/,c62/1.864d0/,c71/9.939d0/,c73/9.401d0/
      data c80/624.545d0/,a0/266.690d0/
      real*8 poly(9)
!      data fla/8*-2./
!      save fla

!      if(fla(1).lt.-1d0) then
!         do i=1,8
!            fla(i) = factln(i)
!         enddo
!      endif

      r=rr/bohr
      r2=r*r
      r6=r2*r2*r2

      if(iso.eq.0) then
        ctheta=dtheta
        do l=1,3!...9
          poly(l)=plgndr(l,0,ctheta)
        enddo
      elseif(iso.eq.1) then
        ctheta=0.
        do l=1,9
          poly(l)=0.
        enddo
      else
        write(6,*)'wrong value for potential type in HeHCN',iso
      endif
      
      vind1=-alpHe*diHCN**2*(1.+poly(2))
      vind2=-6.*alpHe*diHCN*quHCN*ctheta**3
      vind=(vind1+vind2/r)/r6

c parameters fitted to the spectroscopic data
        data ep0/24.825d0/,ep1/3.402d0/,ep2/0.385d0/
     +     ,ep3/0.854d0/
c**NOTE: rm1 is corrected from the table value in the ref.
        data rm0/3.715d0/,rm1/0.020d0/,rm2/0.458d0/,rm3/0.071d0/
c beta in unit of (bohr)^{-1}
        data beta/2.064d0/
        
        eps=ep0+ep1*poly(1)+ep2*poly(2)+ep3*poly(3)
c in Hartree from cm^-1.
        eps=eps*4.5563353d-6

        rmin=rm0+rm1*poly(1)+rm2*poly(2)+rm3*poly(3)
c in a_B from A.
        rmin=rmin/bohr

        rmin6=rmin**6
        rmin7=rmin6*rmin
        rmin8=rmin7*rmin
        rminbeta = rmin*beta

        c6=c60+c62*poly(2)
        c7=c71*ctheta+c73*ctheta**3

!        d6rm=dnOPT(6,rmin,beta,fla)
        d6rm=dnOPT2(6,rminbeta)
!        dd6rm=beta*(dnOPT(5,rmin,beta,fla)-d6rm)
        dd6rm=beta*(dnOPT2(5,rminbeta)-d6rm)
!        d7rm=dnOPT(7,rmin,beta,fla)
        d7rm=dnOPT2(7,rminbeta)
        dd7rm=beta*(d6rm-d7rm)
!        d8rm=dnOPT(8,rmin,beta,fla)
        d8rm=dnOPT2(8,rminbeta)
        dd8rm=beta*(d7rm-d8rm)

        vfix=(vind1+vind2/rmin)/rmin6-c6*d6rm/rmin6-c7*d7rm/rmin7
        dvfix=(-6.*vind1-7.*vind2/rmin)/rmin7-c6*dd6rm/rmin6
     +       +6.*c6*d6rm/rmin7-c7*dd7rm/rmin7+7.*c7*d7rm/rmin8

        if(iso.eq.0) then
        c8=(beta*(eps+vfix)+dvfix)*rmin8/(dd8rm-d8rm*(8./rmin-beta))
        aa=(c8*d8rm/rmin8-eps-vfix)*exp(rminbeta)
        else
        c8=c80
        aa=a0
        endif

        rbeta=r*beta
        vrep=aa*exp(-rbeta)
!        vdisp=-(c6*dnOPT(6,r,beta,fla)+c7*dnOPT(7,r,beta,fla)/r
!     +           +c8*dnOPT(8,r,beta,fla)/r2)/r6
        vdisp=-(c6*dnOPT2(6,rbeta)+c7*dnOPT2(7,rbeta)/r
     +           +c8*dnOPT2(8,rbeta)/r2)/r6


      pot=vrep+vind+vdisp  
      pot=pot*hartree

      return
      end

      function dnOPT2(n,rrbeta)
      real*8 rrbeta,dnOPT2
      integer n,m
      dnOPT2=1d0
      do m=n,1,-1
        dnOPT2=dnOPT2*rrbeta/dble(m) + 1d0
      enddo
      dnOPT2=-exp(-rrbeta)*dnOPT2+1.d0
      return
      end

      function dnOPT(n,rr,beta,factlnarr)
      real*8 rr,beta,dnOPT,factlnarr(n)
      integer n,m
      dnOPT=1.
      do m=1,n
        dnOPT=dnOPT+(beta*rr)**m/exp(factlnarr(m))
      enddo
      dnOPT=-exp(-beta*rr)*dnOPT+1.d0
      return
      end
   
!      function dn(n,rr,beta)
!      real*8 rr,beta,dn,factln
!      integer n,m
!      dn=1.
!      do m=1,n
!        dn=dn+(beta*rr)**m/exp(factln(m))
!      enddo
!      dn=-exp(-beta*rr)*dn+1.d0
!      return
!      end      
C
C     DCN-He potential:
C     we assume the potential is the same as HCN-He, i.e. D sits at the
C     same position as H. therefore we only have to adjust the center of
C     mass and voila, HeHCN becomes HeDCN!
C     for adjusting the center of mass, we need the equilibrium positions
C     of H/D, C and N. we take them from the ab initio MP4 calculation of:
C     Drucker, Tao, and Klemperer, JPC(!) 99, 2646 (1995)
C       H-C: 1.064 A
C       C-N: 1.156 A
C     (ground state vibr. averaged)
C     cos=1...H/D,  cos=-1...N
c
      subroutine HeDCN(rr,dtheta,pot,iso)
c
c INPUT: rr in Angstrom, ctheta=cos(theta)
      implicit none
      real*8, save :: mH=1d0, mD=2d0, mC= 12d0, mN=14d0
      real*8 :: rr,dtheta,pot,comHCN,comDCN,r,dth,x2,z
      integer :: iso
c
      comHCN = (mH*1.064d0 - mN*1.156d0)/(mH+mC+mN)
      comDCN = (mD*1.064d0 - mN*1.156d0)/(mD+mC+mN)
      x2 = rr*rr * (1d0-dtheta**2)
      z  = rr * dtheta + (comDCN-comHCN) ! 0.058
      r  = sqrt(x2 + z*z)
      dth= z/r
      call HeHCN(r,dth,pot,iso)
      end
