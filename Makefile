
COMPILER = gfortran
#COMPILER = ifort
CC = gcc
#CC = icc

CHECK = -fcheck=array-temps,bounds #-ftree-vectorizer-verbose=1
# 
FFLAGS = -O3 -fopenmp -fexternal-blas -Wall -pedantic -mtune=native -march=native -pipe -ffast-math -flto -fuse-linker-plugin -fweb -frename-registers -funroll-loops -funroll-all-loops $(CHECK)
# -g and -flto are not compatible
#FFLAGS = -g -O0 -fopenmp -Wall -fexternal-blas -pedantic -mtune=native -march=native -pipe -fcheck=all 

INCLUDE = -I/usr/include -I/usr/include/mpi
LINK = -L/usr/lib/ -lm -lfftw3 -llapack -lmpi -lgsl -lgslcblas -lblas -llapack

CFLAGS = -O3 -Wall -pedantic -mtune=native -march=native -pipe -ffast-math -flto -fuse-linker-plugin -fweb -frename-registers -std=gnu99 -funroll-loops
#CFLAGS = -g -O0 -Wall -pedantic -mtune=native -march=native -pipe -ffast-math -std=gnu99


mains = coupchan.f90 spectrum.f90 test.f90 read_ensemble.f90
targets = $(basename $(mains))
target_o = $(targets:=.o)

source = $(wildcard *.f)
source90 = $(wildcard *.f90)
obj = $(source:.f=.o) $(source90:.f90=.o)

csource = $(wildcard *.c)
cobj = $(csource:.c=.o)

all: coupchan

tests: $(targets)

utils: $(targets)


$(targets): $(obj) $(cobj)
	$(COMPILER) $(filter-out $(target_o), $(obj)) $(cobj) $@.o $(FFLAGS) $(LINK) -o $@
# Dont include other targets main files


%.mod : %.o
	touch $@

#conf.mod: conf.o
#	touch conf.mod
thermal.mod: boltzmann.o
	touch thermal.mod
#shm.mod: shm.o
#	touch shm.mod

confdep = $(filter-out conf.o, $(obj))
$(confdep): conf.mod

thermaldep = $(filter-out conf.o boltzmann.o shm.o, $(obj))
$(thermaldep): thermal.mod

shmdep = $(filter-out conf.o boltzmann.o shm.o, $(obj))
$(shmdep): shm.mod

potentialsdep = $(filter-out conf.o boltzmann.o shm.o potentials.o, $(obj))
$(potentialsdep): potentials.mod

#prefetchdep = potentials.o
#$(prefetchdep): prefetch.mod


%.o : %.f
	$(COMPILER) $(FFLAGS) $(INCLUDE) -o $@ -c $<

%.o : %.f90
	$(COMPILER) $(FFLAGS) $(INCLUDE) -o $@ -c $<

%.o : %.c
	$(CC) $(CFLAGS) $(INCLUDE) -o $@ -c $<

% : Makefile

.PHONY: clean
clean:
	rm -f *.o conf.mod thermal.mod shm.mod

.PHONY: distclean
distclean: clean
	rm -f $(targets)
