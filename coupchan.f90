! solve the coupled channel eq. (12) of hutsonJCP93

program init
     use conf

     call conf_init() ! Load parameters (get size of num_N and num_jJ right)

     call coupchan() ! Call main program

end program init

subroutine setup_mpi(numprocs,myrank,hostname,hostnamelen)
     use conf
     implicit none
     include 'mpif.h'
     integer :: ierr, hostnamelen, numprocs, myrank
     character(MPI_MAX_PROCESSOR_NAME) :: hostname

     call MPI_init(ierr)
     if (ierr .ne. 0) call quit('Failed to initialize MPI')
     ! Get the number of processors this job is using
     ! and the rank (id) of the current process
     call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)
     call MPI_COMM_RANK(MPI_COMM_WORLD, myrank, ierr)
     call MPI_GET_PROCESSOR_NAME(hostname, hostnamelen, ierr)

     print *, 'Process',myrank, 'on ',hostname(1:hostnamelen)

     if (numprocs .eq. 1) then
          print *, 'You need at least two processes (use mpirun).'
          print *, 'This process will just make sure the static matrices are calculated.'
     endif

end subroutine setup_mpi

subroutine allocate_vmats(h)
     use conf
     use shm
     use iso_c_binding
     implicit none

     integer(kind=C_SIZE_T)::vshape(2),diagshape(3),rotmatshape(1),nextishape(1)
     integer(kind=C_SIZE_T) :: block_offset_shp(2)
     integer(kind=C_SIZE_T) :: vsize,diagsize,expdiagsize,rotmatsize,nextisize,&
          llsize, expnextisize
     integer(kind=C_SIZE_T) :: block_offset_size
     type(vmat_container) :: h

     integer(kind=C_SIZE_T) :: idxsize, single_element_size, exp_single_element_size, single_pot_size

     single_element_size = STORAGE_SIZE(real(0,kind=db))/8
     single_pot_size = STORAGE_SIZE(real(0,kind=pot_k))/8
     exp_single_element_size = STORAGE_SIZE(cmplx(0,kind=db))/8
     idxsize = STORAGE_SIZE(INT(0,kind=idx_k))/8
     llsize = STORAGE_SIZE(INT(0,kind=ll))/8

     vshape = (/NR_pot,LAMAX+1/)
     vsize = max(1,INT(NR_pot,C_SIZE_T)*(LAMAX+1))*single_pot_size
     diagshape = (/NR,num_N,JTOTMAX+1/)
     diagsize = INT(NR,C_SIZE_T)*num_N*(JTOTMAX+1)*single_element_size
     expdiagsize = INT(NR,C_SIZE_T)*num_N*(JTOTMAX+1)*exp_single_element_size
     rotmatshape = (/num_rotmat/)
     rotmatsize = num_rotmat*single_pot_size
     nextishape = (/num_rotmat/)
     nextisize = num_rotmat*idxsize
     expnextisize = num_expvmat*idxsize
     block_offset_shp = (/2,JTOTMAX+1/)
     block_offset_size = 2*(JTOTMAX+1)*llsize

     call allocate_shared(vmat_file,vsize,h%v_h,h%v_p)
     call allocate_shared(vdiag_file,diagsize,h%diag_h,h%diag_p)
     call allocate_shared(rotmat_file,rotmatsize,h%rotmat_h,h%rotmat_p)
     call allocate_shared(nexti_file,nextisize,h%nexti_h,h%nexti_p)
     call allocate_shared(vblock_file,block_offset_size,h%block_h,h%block_p)
!--
     call allocate_shared(expV_p_file,expvmat_cplx_size,h%expV_pulse_h,h%expV_pulse_p)
     call allocate_shared(expV_file,expvmat_cplx_size,h%expV_h,h%expV_p)
     call allocate_shared(expVdiag_p_file,expdiagsize,h%expdiag_pulse_h,h%expdiag_pulse_p)
     call allocate_shared(expVdiag_file,expdiagsize,h%expdiag_h,h%expdiag_p)
     call allocate_shared(expV_p_ind_file,nextisize,h%expind_pulse_h,h%expind_pulse_p)
     call allocate_shared(expV_ind_file,nextisize,h%expind_h,h%expind_p)
     call allocate_shared(expV_offset_file,block_offset_size,h%expblock_h,h%expblock_p)
     call allocate_shared(expV_offset_p_file,block_offset_size,h%expblock_pulse_h,h%expblock_pulse_p)


     call C_F_POINTER(h%v_p,h%v,vshape)
     call C_F_POINTER(h%diag_p,h%diag,diagshape)
     call C_F_POINTER(h%rotmat_p,h%rotmat,rotmatshape)
     call C_F_POINTER(h%nexti_p,h%nexti,nextishape)
     call C_F_POINTER(h%block_p,h%block_offset,block_offset_shp)

     call C_F_POINTER(h%expV_pulse_p,h%expV_pulse,(/num_expvmat/))
     call C_F_POINTER(h%expV_p,h%expV,(/num_expvmat/))
     call C_F_POINTER(h%expdiag_pulse_p,h%expdiag_pulse,diagshape)
     call C_F_POINTER(h%expdiag_p,h%expdiag,diagshape)
     call C_F_POINTER(h%expind_pulse_p,h%expind_pulse,(/num_expvmat/))
     call C_F_POINTER(h%expind_p,h%expind,(/num_expvmat/))
     call C_F_POINTER(h%expblock_p,h%expblock,block_offset_shp)
     call C_F_POINTER(h%expblock_pulse_p,h%expblock_pulse,block_offset_shp)

     h%v(1:NR_pot,0:LAMAX) => h%v
     h%diag(1:NR,1:num_N,0:JTOTMAX) => h%diag
     h%block_offset(1:2,0:JTOTMAX) => h%block_offset
     ! rotmat and nexti are ok
     
     h%expdiag_pulse(1:NR,1:num_N,0:JTOTMAX) => h%expdiag_pulse
     h%expdiag(1:NR,1:num_N,0:JTOTMAX) => h%expdiag

end subroutine allocate_vmats

subroutine calculate_vmats_and_initial_states(vmats,status_handle,status_ptr,hostname)

     ! One process per host needs to calculate the exponentials of
     ! the potential matrices.
     ! This is controlled via the status.dat file
     ! DONT PUT THIS ON A NETWORK DRIVE

     use conf
     use shm
     use potentials
     use iso_c_binding
     implicit none
     include 'mpif.h'
     type(vmat_container), intent(out) :: vmats
     type(c_ptr), intent(out) :: status_handle
     integer, volatile, pointer, intent(out) :: status_ptr(:)
     character(len=*), intent(in) :: hostname
     type(c_ptr) :: status_C_ptr
     complex(kind=db) :: dtC_pulse, dtC
     integer :: ierr

     call allocate_shared(statusfile,status_size,status_handle,status_C_ptr)
     call C_F_POINTER(status_C_ptr, status_ptr, (/num_status_elements/))

     if (.not. keep_bigmats) status_ptr = 0;

     ! Call MPI barrier to ensure all processes have allocated the shared
     ! arrays before unlinking them from the filesystem.
     call MPI_Barrier(MPI_COMM_WORLD,ierr)
     if (ierr .ne. 0) call quit('Barrier failed')

     if (shm_lock_exclusive(status_handle) .ne. 0) call quit('Status lock failed')
     
     if (status_ptr(1) .ne. 0) then ! somebody calculated the matrices. Skip!
          if (shm_unlock(status_handle) /= 0) call quit('Status unlock failed')
          return
     endif
     
     if (.not. keep_bigmats) then
      ! Unlink this here so a crash won't clobber the next run
      if (unlink(statusfile) .ne. 0) call quit('Could not unlink status file')
      ! Also unlink the expvmat files so they won't clobber the filesystem
      ! They might not exists (because not all were allocated) so we ignore
      ! errors.
      ierr = unlink(vmat_file)
      ierr = unlink(vdiag_file)
      ierr = unlink(rotmat_file)
      ierr = unlink(nexti_file)
      ierr = unlink(vblock_file)
      ierr = unlink(expV_p_file)
      ierr = unlink(expV_file)
      ierr = unlink(expVdiag_p_file)
      ierr = unlink(expVdiag_file)
      ierr = unlink(expV_p_ind_file)
      ierr = unlink(expV_ind_file)
      ierr = unlink(expV_offset_file)
      ierr = unlink(expV_offset_p_file)
     endif

     ! load the potential matrix 
     print *, 'Loading potential matrix on ', hostname, '...'

     call setpotmat(molecule,vmats%v,vmats%diag,vmats%rotmat,vmats%nexti,vmats%block_offset)

     print *, 'Potential matrix loaded on ',hostname,'.'

     print *, 'Finding initial thermal ensemble on ',hostname,'...'
     call find_initial_ensemble(vmats)
    
     print *, 'Exponentiating V on ', hostname,'...'
     dtC = cmplx(0,DT,kind=db)
     dtC_pulse = cmplx(0,DT_pulse,kind=db)/2 ! /2 for trotter step
     call exponentiate_vmat(vmats%v,vmats%diag,vmats%rotmat,vmats%nexti,&
          dtC,dtC_pulse, &
          vmats%expV, vmats%expV_pulse, &
          vmats%expdiag, vmats%expdiag_pulse, &
          vmats%expind, vmats%expind_pulse, &
          vmats%expblock, vmats%expblock_pulse)

     ! End with unlocking the status file, signaling the other processes
     ! that the matrices have been calculated.

     status_ptr(1) = 1
     if (shm_unlock(status_handle) .ne. 0) call quit('Status unlock failed')

end subroutine calculate_vmats_and_initial_states

subroutine populate_imag_full_vmat(vmats,vmat_full)
     use conf
     implicit none

     type(vmat_container), intent(in) :: vmats
     real(kind=db), intent(out) :: vmat_full(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)

     integer :: la,jt
     integer(kind=ll) :: i1,i2,i,ii
     
     vmat_full = 0

     if (He .eq. 1) then
     i = 1
     ii = 0
outer: do jt=0,imag_JTOTMAX,JTOTSTEP
     do la = 0, LAMAX, LASTEP
     do i1 = 1,indOrder(jt)
     do i2 = i1,indOrder(jt)

     ii = ii+1
     if (vmats%nexti(i) .ne. ii) cycle

          vmat_full(i2,i1,jt,1:NR_pot) = vmat_full(i2,i1,jt,1:NR_pot) + vmats%rotmat(i)*vmats%v(:,la)
          if (i1 .ne. i2) &
          vmat_full(i1,i2,jt,1:NR_pot) = vmat_full(i1,i2,jt,1:NR_pot) + vmats%rotmat(i)*vmats%v(:,la)

          i = i + 1
          if (vmats%nexti(i) .eq. 0) exit outer
     enddo
     enddo
     enddo
     end do outer
     endif

     do jt=0,imag_JTOTMAX,JTOTSTEP
     do i1=1,indOrder(jt)
     vmat_full(i1,i1,jt,:) = vmat_full(i1,i1,jt,:) + vmats%diag(:,i1,jt)
     enddo
     enddo

end subroutine populate_imag_full_vmat


subroutine find_initial_ensemble(vmats)

     use conf
     use thermal
     use potentials
     implicit none

     type(vmat_container) :: vmats
     real(kind=db) :: vmat_full(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)
     complex(kind=db) :: expkin(NR), expkinHalf(NR)
     integer :: ir
     complex(kind=db) :: k(NR) = (/( ir*DK, ir=1,NR )/)
     real(kind=db) :: dtR
     real(kind=db) :: T, percentile
     complex(kind=db), allocatable :: a_ensemble(:,:,:)
     real(kind=db), allocatable :: statistical_weight(:), E_ensemble(:)
     integer :: jt_num_states(0:imag_JTOTMAX)
     complex(kind=db) :: energy_eigenstates(NR,num_N,num_trial_states,0:imag_JTOTMAX)
     real(kind=db), dimension(num_trial_states,0:imag_JTOTMAX) :: E_stationary

     integer, allocatable :: jtots(:)
     integer :: ensemble_size
     integer :: i
     
     real(kind=db) :: expvmat(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)

     call populate_imag_full_vmat(vmats,vmat_full)

     dtR = DT_imag
     expkin = (/( exp(-H2Mred*k(ir)**2*dtR), ir=1,NR )/)
     expkinHalf = (/( exp(-H2Mred*k(ir)**2*dtR/2), ir=1,NR )/)
     
     call exponentiate_imag_vmat(dtR,vmat_full,expvmat)
     print *, 'Imaginary time potential exponentiated.'
     
     ! Find initial state with imaginary time propagation
     T = temperature
     percentile = fraction_partition_function
     
     print *, 'Calculating the energy spectrum...'
     call energy_spectrum(jt_num_states,energy_eigenstates,E_stationary,expvmat,expkin,expkinHalf)

     print *, 'Getting Boltzmann ensemble...'
     call thermal_ensemble(T,percentile,ensemble_size,a_ensemble,jtots, &
                           statistical_weight,E_ensemble,jt_num_states, &
                           energy_eigenstates,E_stationary)
     
     print *, 'Found initial thermal ensemble:'

     do i = 1, ensemble_size
          print *, E_ensemble(i), jtots(i), statistical_weight(i)
     enddo

     open(77,file=ensemble_file,status='unknown',form='UNFORMATTED')
     write(77) ensemble_size
     write(77) E_ensemble, jtots, statistical_weight, a_ensemble
     write(77) imag_JTOTMAX, num_trial_states, num_N
     write(77) jt_num_states,energy_eigenstates,E_stationary
     close(77)

     deallocate(E_ensemble,jtots,statistical_weight,a_ensemble)

end subroutine find_initial_ensemble


subroutine allocate_shared_laser_matrices(Nmax,U,W,shm_handles)
     use conf
     use shm
     use iso_c_binding
     implicit none
     include 'mpif.h'
     integer :: Nmax
     type(Upointer), dimension(-Nmax:Nmax) :: U,W
     !type(eigvalpointer), dimension(-Nmax:Nmax) :: W_eigval
     type(c_ptr), dimension(0:Nmax,4) :: shm_handles
     !real(kind=db), dimension(:,:), contiguous, pointer :: tmp2
     integer(kind=ll) :: shp(1),block_shp(2)
     integer(kind=ll) :: Usize,Unextisize,Ublock_size
     type(c_ptr) :: shm_pointer
     integer :: N
     character(len=1024) :: str_N
     integer :: ierr

     shp = (/num_Umat/)
     block_shp = (/2,LMAX+1/)
     Usize = num_Umat*STORAGE_SIZE(real(0,kind=lpot_k))/8
     Unextisize = num_Umat*STORAGE_SIZE(int(0,kind=idx_k))/8
     Ublock_size = 2*(LMAX+1)*STORAGE_SIZE(int(0,kind=ll))/8

     do N = 0,Nmax
          write (str_N,*) N
          ! Allocate U:
          call allocate_shared(matrix_folder//'U'//trim(str_N)//'.dat',Usize,shm_handles(N,1),shm_pointer)
          call C_F_POINTER(shm_pointer,U(N)%p,shp)

          ! Allocate W
          call allocate_shared(matrix_folder//'W'//trim(str_N)//'.dat',Usize,shm_handles(N,2),shm_pointer)
          call C_F_POINTER(shm_pointer,W(N)%p,shp)

          call allocate_shared(matrix_folder//'Unexti'//trim(str_N)//'.dat',Unextisize,shm_handles(N,3),shm_pointer)
          call C_F_POINTER(shm_pointer,U(N)%Unexti,shp)
          call C_F_POINTER(shm_pointer,W(N)%Unexti,shp)

          call allocate_shared(matrix_folder//'Ublock'//trim(str_N)//'.dat',Ublock_size,shm_handles(N,4),shm_pointer)
          call C_F_POINTER(shm_pointer,U(N)%Ublock_offset,block_shp)
          call C_F_POINTER(shm_pointer,W(N)%Ublock_offset,block_shp)

          U(N)%Ublock_offset(1:2,0:LMAX) => U(N)%Ublock_offset
          W(N)%Ublock_offset(1:2,0:LMAX) => W(N)%Ublock_offset


     enddo

     if (.not. keep_bigmats) then
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      if (ierr .ne. 0) call quit('Barrier failed')

      ! All processes does this (if only one per host, we need a lock on status)
      ! but it does not matter. Unlinking an unexisting file will only
      ! signal an error that we simply ignore.
      do N = 0,Nmax
           write (str_N,*) N
           ierr = unlink(matrix_folder//'U'//trim(str_N)//'.dat')
           ierr = unlink(matrix_folder//'W'//trim(str_N)//'.dat')
           ierr = unlink(matrix_folder//'Unexti'//trim(str_N)//'.dat')
           ierr = unlink(matrix_folder//'Ublock'//trim(str_N)//'.dat')
      enddo
     endif

end subroutine allocate_shared_laser_matrices

subroutine calculate_laser_matrices(Nmax,U,W)
     use conf
     use potentials
     implicit none
     integer, intent(in) :: Nmax
     type(Upointer), dimension(0:Nmax) :: U,W
     !type(eigvalpointer), dimension(0:Nmax) :: W_eigval
     integer :: N

     do N = 0,Nmax
          ! Get coupled matrix representation of cos^2 theta
          print *, 'Setting U (=cos2 matrix) for N=',N,'...'
          call setUmat(U(N)%p, U(N)%Unexti, U(N)%Ublock_offset, N)
          ! Then the laser potential (sans E^2)
          print *, 'Setting W (=laser potential) for N=',N,'...'
          call setWmat(W(N)%p, U(N)%p, U(N)%Unexti, N)
     enddo
end subroutine calculate_laser_matrices




subroutine coupchan()
     use conf
     use shm
     use potentials
     use iso_c_binding
     implicit none
     include 'mpif.h'

     ! Needed because we pass pointers around.
     ! Maybe this needs to go in its own module.
     interface
          subroutine calculate_vmats_and_initial_states(vmats,status_handle,status_ptr,hostname)
               use conf
               type(vmat_container), intent(out) :: vmats
               type(c_ptr), intent(out) :: status_handle
               integer, volatile, pointer, intent(out) :: status_ptr(:)
               character(len=*), intent(in) :: hostname
          end subroutine calculate_vmats_and_initial_states
     end interface

     type(vmat_container) :: vmats

     complex(kind=db), allocatable :: a_initial(:,:,:)
     real(kind=db), allocatable :: statistical_weight(:), E(:)
     integer, allocatable :: jtots(:)
     integer :: ensemble_size

     type(c_ptr) :: status_handle
     integer, volatile, pointer :: status_ptr(:)

     integer :: Nmax
     type(c_ptr), dimension(:,:), allocatable :: shm_handles
     type(Upointer), dimension(:), allocatable :: U,W
     !type(eigvalpointer), dimension(:), allocatable  :: W_eigval

     integer :: ierr, hostnamelen, numprocs, myrank
     character(MPI_MAX_PROCESSOR_NAME) hostname
     integer :: imag_JTOTMAX_p, num_trial_states_p, num_N_p
     integer :: jt_num_states(0:imag_JTOTMAX)
     complex(kind=db) :: energy_eigenstates(NR,num_N,num_trial_states,0:imag_JTOTMAX)
     real(kind=db), dimension(num_trial_states,0:imag_JTOTMAX) :: E_stationary

     call setup_mpi(numprocs, myrank, hostname, hostnamelen)

     call allocate_vmats(vmats)
     
     call calculate_vmats_and_initial_states(vmats,status_handle,status_ptr,hostname(1:hostnamelen))

     ! Only once process per host actually calculates the initial states.
     ! It is communicated out to the rest of the processes via
     ! the ensemble.dat file:
     open(77,file=ensemble_file,status='old',form='UNFORMATTED')
     read(77) ensemble_size
     allocate(E(ensemble_size), jtots(ensemble_size), statistical_weight(ensemble_size), a_initial(NR,num_N,ensemble_size))
     read(77) E, jtots, statistical_weight, a_initial
     read(77) imag_JTOTMAX_p, num_trial_states_p, num_N_p
     if (imag_JTOTMAX_p .ne. imag_JTOTMAX .or. &
         num_trial_states_p .ne. num_trial_states .or. &
         num_N_p .ne. num_N) call quit('Incompatible ensemble file.')
     read(77) jt_num_states,energy_eigenstates,E_stationary
     close(77)


     Nmax = maxval(jtots)

     ! TODO: Figure out if negative N are needed
     allocate(U(0:Nmax), W(0:Nmax)) !, W_eigval(0:Nmax)
     allocate(shm_handles(0:Nmax,4))
     ! Allocate cos^2 theta matrix and laser potential pointer arrays:
     call allocate_shared_laser_matrices(Nmax,U,W,shm_handles)

     if (shm_lock_exclusive(status_handle) .ne. 0) &
          call quit('Lock call failed')
     
     if (status_ptr(2) .ne. 1) then
          call calculate_laser_matrices(Nmax,U,W)
          status_ptr(2) = 1
     endif

     if (shm_unlock(status_handle) .ne. 0) &
          call quit('Unlock call failed')

     if (numprocs .ne. 1) then
          
     if (myrank .eq. 0) then
          call dispatcher(numprocs,ensemble_size,jtots,statistical_weight,hostname(1:hostnamelen))
          call send_shutdown_signal()
     else
          call worker(myrank,ensemble_size,a_initial,jtots,Nmax,vmats,U,W,jt_num_states,energy_eigenstates,E_stationary)
          call wait_for_shutdown_signal()
     end if
     else
          print *, 'Not propagating. Bye!'
          print *, '(need more than one thread for this, run with mpirun -n 2 at least)'
     endif

     ! Maybe also deallocate stuff.. Not really needed because
     ! we end the program now.

     ! status_handle, expvmats, E, jtots, statistical_weight, a_initial

     call MPI_FINALIZE (ierr)
     
end subroutine coupchan

subroutine dispatcher(numprocs, ensemble_size, jtots, statistical_weight, hostname)
     use conf
     implicit none
     include 'mpif.h'
     integer, intent(in) :: numprocs, ensemble_size, jtots(ensemble_size)
     real(kind=db), intent(in) :: statistical_weight(ensemble_size)
     character(len=*),intent(in) :: hostname
     real(kind=db) :: weight
     integer :: i, N, jt, shell, jtmax
     integer :: worker_id, tag
     real(kind=db), dimension(focal_volume_averaging_shells) :: FVA_weights, FVA_shell_intensities
     real(kind=db) :: cos2(cos2_num_samples)
     real(kind=db) :: buffer(4+cos2_num_samples)
     integer :: num_jobs, num_finished


     ! All set up, ask workers for pulse propagation
     ! and calculation of the degree of alignment

     ! Loop over all initial states, N, and pulse strengths:
     ! 
     ! Wait for worker to send message
     !    If message is result -> save it
     !    if message is work request -> send work packet, advance loop
     ! 
     ! After end of loop: loop over incomming requests again
     !    if message is result -> save it
     !    if message is work request -> send cease instruction

     call system('mkdir data 2>/dev/null')

     call focal_volume_average(FVA_weights, FVA_shell_intensities)

     cos2 = 0

     num_jobs = 0
     num_finished = 0 
     do i = 1, ensemble_size
          num_jobs = num_jobs + 2*jtots(i)+1
!          num_jobs = num_jobs + jtots(i)+1
     enddo
     num_jobs = num_jobs * focal_volume_averaging_shells

     print *, 'Dispatcher on ',hostname,' starting',num_jobs,'jobs...'

     jtmax = maxval(jtots)

     ! By looping over N first, we can make sure each worker only
     ! needs to ever hold one copy of the interaction and cos2 matrix.
     do N = 0,jtmax ! Because of symmetry, don't calculate negative N 
          do i = 1, ensemble_size
          jt = jtots(i)
          if (abs(N) .gt. abs(jt)) cycle
               do shell = 1,focal_volume_averaging_shells
                    weight = statistical_weight(i)/(2*jt+1)*FVA_weights(shell)
                    if (N > 0) weight = 2*weight
                    
                    tag = MPI_work_request - 1
                    
                    do while (tag .ne. MPI_work_request)
                         call get_worker_message(buffer,worker_id,tag)
                         if (tag .eq. MPI_work_request) then
                              call send_work(worker_id,weight,i,N,FVA_shell_intensities(shell))
                         elseif (tag .eq. MPI_result) then
                              call save_result(cos2,buffer)
                              num_finished = num_finished+1
                              print *, num_finished,'/',num_jobs,': ',num_finished/dble(num_jobs)*100
                         else
                              call quit('Received unexpected tag')
                         endif
                    enddo
               enddo
          enddo
     enddo

     print *,'All work dispatched, waiting for results.'

     ! All work dispatched. Wait for results and tell new workers
     ! to cease.

     do i=1,numprocs-1

          tag = MPI_work_request - 1

          do while (tag .ne. MPI_work_request)
               call get_worker_message(buffer,worker_id,tag)
               if (tag .eq. MPI_work_request) then
                    call send_cease(worker_id)
               elseif (tag .eq. MPI_result) then
                    call save_result(cos2,buffer)
                    num_finished = num_finished+1
                    print *, num_finished,'/',num_jobs,': ',num_finished/dble(num_jobs)*100
               else
                    call quit('Received unexpected tag')
               endif
          enddo
     enddo

     ! TODO: Also save a trace for each state + intensity shell
     
     ! Save cos2 somewhere.

     open (unit=20,file='cos2.dat',status='replace',action='write')
     
     call dump_cos2(20,cos2)

     close(20)

end subroutine dispatcher

subroutine dump_cos2(u,cos2)
     use conf
     implicit none

     integer, intent(in) :: u
     real(kind=db), intent(in) :: cos2(cos2_num_samples)
     real(kind=db) :: t
     integer :: i

     t = -1.5*pulse_FWHM
     do i = 1,num_pulse_samples
          write(u,*) t/picosecond,cos2(i)
          t = t + NTblock_pulse*DT_pulse
     enddo

     do i = 1,num_fieldfree_samples
          write(u,*) t/picosecond,cos2(i+num_pulse_samples)
          t = t + NTblock*DT
     enddo

end subroutine dump_cos2


subroutine worker(myrank,ensemble_size,a_initial,jtots,Nmax,vmats,U,W,jt_num_states,energy_eigenstates,E_stationary)
     use conf
     use iso_c_binding
     implicit none
     integer, intent(in) :: myrank, ensemble_size, jtots(ensemble_size)
     complex(kind=db), intent(in) :: a_initial(NR,num_N,ensemble_size)
     type(vmat_container), intent(in) :: vmats
     integer, intent(in) :: Nmax
     type(Upointer), dimension(0:Nmax), intent(in) :: U,W
     !type(eigvalpointer), dimension(0:Nmax), intent(in) :: W_eigval
     !integer, volatile, pointer, intent(inout) :: status_ptr(:)
     complex(kind=db) :: expkin(NR), expkinHalf(NR), expkinHalf_pulse(NR)
     complex(kind=db) :: dtC, dtC_pulse
     integer :: ir
     real(kind=db) :: k(NR) = (/( ir*DK, ir=1,NR )/)
     real(kind=db) :: weight, I_max
     integer :: ensemble_member,N,oldN
     real(kind=db) :: cos2(cos2_num_samples)
     !real(kind=db), dimension(num_jJ,num_jJ,0:LMAX) :: U,W
     !real(kind=db), dimension(num_jJ,0:LMAX) :: W_eigval
     logical :: request_work
     character(len=1024) :: scratchdir
     
     ! state vector:
     complex(kind=db) :: a(NR,num_N,0:JTOTMAX)
     
     ! The spectrum
     integer, intent(in) :: jt_num_states(0:imag_JTOTMAX)
     complex(kind=db), intent(in) :: energy_eigenstates(NR,num_N,num_trial_states,0:imag_JTOTMAX)
     real(kind=db), intent(in), dimension(num_trial_states,0:imag_JTOTMAX) :: E_stationary
     real(kind=db) :: bound_state_overlap

     ! Exponentiate kinetic energy:
     dtC = DT * cmplx(0_db,1_db)
     expkin = (/( exp(-H2Mred*k(ir)**2*dtC), ir=1,NR )/)
     expkinHalf = (/( exp(-H2Mred*k(ir)**2*dtC/2), ir=1,NR )/)
     dtC_pulse = DT_pulse * cmplx(0_db,1_db)
     expkinHalf_pulse = (/( exp(-H2Mred*k(ir)**2*dtC_pulse/2), ir=1,NR )/)

     
     call system('mkdir ensemble 2>/dev/null')

     print *, 'Worker', myrank, 'ready!'


     oldN = JTOTMAX+1 ! never happens
     ! Loop over incomming requests:
     do while (request_work(weight,ensemble_member,N,I_max))
          
          write (scratchdir,*) weight,ensemble_member,N,I_max/pulse_Imax
          call system('mkdir "ensemble/'//trim(ADJUSTL(scratchdir))//'" 2>/dev/null')
          call CHDIR('ensemble/'//trim(ADJUSTL(scratchdir)))
          ! The dispatcher dispatches jobs in order of increasing N.
          !if (N .ne. oldN) then
               !if (oldN .ne. JTOTMAX+1) then
               !     call deallocate_shared(shm_handles(oldN,1))
               !     call deallocate_shared(shm_handles(oldN,2))
               !     call deallocate_shared(shm_handles(oldN,3))
               !endif
              oldN = N
          !endif
     
          ! Get the state vector to propagate
          a = 0
          a(:,:,jtots(ensemble_member)) = a_initial(:,:,ensemble_member)

          ! Solve the Schrodinger equation for this configuration:
          !
          ! First apply the pulse:
          cos2=0
          call apply_pulse(cos2(1:num_pulse_samples),a,I_max,U(N),W(N), &
               vmats,expkinHalf_pulse)
          if (calculate_bound_state_overlap) then
          open (unit=20,file='bound_state_overlap.dat',status='replace',action='write')
          write(20,*) bound_state_overlap(jt_num_states,energy_eigenstates,E_stationary,a)
          flush(20)
          endif
          ! Then propagate the wave function without the pulse potential
          call fieldfree_propagation(cos2(num_pulse_samples+1:),a,U(N), &
               vmats,expkin,expkinHalf)

          if (calculate_bound_state_overlap) then
          write(20,*) bound_state_overlap(jt_num_states,energy_eigenstates,E_stationary,a)
          close(20)
          endif
          ! Return a list of cos^2 theta samples
          call send_result(weight,ensemble_member,N,I_max,cos2)
          call CHDIR('../../')
     enddo

     print *, 'Worker',myrank,'finished.'

end subroutine worker



function request_work(weight,ensemble_member,N,I_max)
     use conf
     implicit none
     include 'mpif.h'

     logical request_work
     real(kind=db), intent(out) :: weight, I_max
     integer, intent(out) :: ensemble_member,N
     integer, parameter :: dispatcher = 0
     integer :: ierr, mpi_status(MPI_STATUS_SIZE)
     real(kind=db) :: buffer(4)

     buffer = 0
     
     ! Send a request for work packet
     call MPI_SEND(buffer,0,MPI_DOUBLE_PRECISION,dispatcher,MPI_work_request,MPI_COMM_WORLD,ierr)
     if (ierr .ne. 0) call quit('Failed to send request for work.')

     ! Dispatcher should send a work packet back.
     call MPI_RECV(buffer,4,MPI_DOUBLE_PRECISION,dispatcher,MPI_work_packet,MPI_COMM_WORLD,mpi_status,ierr)
     if (ierr .ne. 0) call quit('Failed to receive work packet.')

     weight = buffer(1)
     ensemble_member = INT(buffer(2))
     N = INT(buffer(3))
     I_max = buffer(4)

     request_work = (weight .ge. 0) ! If weight is negative, dispatcher
                                    ! has no more work to distribute.

     return
end function request_work


subroutine send_result(weight,ensemble_member,N,I_max,cos2)
     use conf
     implicit none
     include 'mpif.h'
     real(kind=db), intent(in) :: cos2(cos2_num_samples)
     real(kind=db) :: buffer(4+cos2_num_samples)
     real(kind=db), intent(in) :: weight, I_max
     integer, intent(in) :: ensemble_member,N
     integer, parameter :: dispatcher = 0
     integer :: ierr

     buffer(1) = weight
     buffer(2) = dble(ensemble_member)
     buffer(3) = dble(N)
     buffer(4) = I_max
     buffer(5:) = cos2

     call MPI_SEND(buffer,cos2_num_samples+4,MPI_DOUBLE_PRECISION,dispatcher,MPI_result,MPI_COMM_WORLD,ierr)
     if (ierr .ne. 0) call quit('Failed to send result')



end subroutine send_result


subroutine get_worker_message(buffer,worker_id,tag)
     use conf
     implicit none
     include 'mpif.h'
     real(kind=db), intent(out) :: buffer(4+cos2_num_samples)
     integer, intent(out) :: worker_id, tag
     integer :: ierr, mpi_status(MPI_STATUS_SIZE), request
     logical :: flag
     integer :: i
     
     ierr = 0
     flag = .false.

     call MPI_IRECV(buffer,4+cos2_num_samples,MPI_DOUBLE,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,request,ierr)
     if (ierr .ne. 0) call quit('Could not receive worker message.')

outer:do
          do i = 1,1000
               CALL MPI_TEST(request,flag,mpi_status,ierr) 
               if (ierr .ne. 0) call quit('MPI_TEST failed.')
               if (flag) exit outer
          enddo
          
          if (He .eq. 1) then
               call sleep(5) ! mpi won't do blocking IO for us...
          else
               call sleep(1)
          endif
     end do outer

     worker_id = mpi_status(MPI_SOURCE)
     tag = mpi_status(MPI_TAG)

end subroutine

subroutine wait_for_shutdown_signal()
     use conf ! Simulate blocking IO for shutdown signal
     implicit none
     include 'mpif.h'
     integer :: buffer, request, ierr, mpi_status(MPI_STATUS_SIZE), i
     logical :: flag

     call MPI_IBCAST(buffer,1,MPI_INTEGER,0,MPI_COMM_WORLD,request,ierr)

outer:do
          do i = 1, 1000
               call MPI_TEST(request,flag,mpi_status,ierr)
               if (ierr .ne. 0) call quit('MPI_TEST failed waiting for shutdown')
               if (flag) exit outer
          end do

          if (He .eq. 1) then
               call sleep(5) ! mpi won't do blocking IO for us...
          else
               call sleep(1)
          endif
     end do outer

end subroutine wait_for_shutdown_signal

subroutine send_shutdown_signal()
     use conf
     implicit none
     include 'mpif.h'
     integer :: buffer, request, ierr

     buffer = 42

     call MPI_IBcast(buffer, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, request, ierr)

end subroutine send_shutdown_signal

subroutine save_result(cos2,buffer)
     use conf
     implicit none
     real(kind=db), intent(inout) :: cos2(cos2_num_samples)
     real(kind=db), intent(in) :: buffer(4+cos2_num_samples)
     real(kind=db) :: weight, I_max
     integer(kind=db) :: ensemble_member, N
     character(len=1024) :: filename
        
     weight = buffer(1)
     ensemble_member = INT(buffer(2))
     N = INT(buffer(3))
     I_max = buffer(4)

     cos2 = cos2 + weight * buffer(5:)

     ! Optionally print something out, or save to a file

     print *, 'Received', weight,ensemble_member,N,I_max

     write (filename,"(A10,I5,I5,ES10.2E2,ES10.2E2,A4)") 'data/cos2_',ensemble_member,N,I_max,weight,'.dat'
     print *, trim(filename)

     open (unit=21,file=trim(filename),status='replace',action='write')

     call dump_cos2(21,buffer(5:))

     close(21)


end subroutine save_result


subroutine send_work(worker_id, weight,ensemble_member,N,I_max)
     use conf
     implicit none
     include 'mpif.h'
     integer :: worker_id, ensemble_member, N, ierr
     real(kind=db) :: weight, I_max
     real(kind=db) :: buffer(4)

     buffer(1) = weight
     buffer(2) = dble(ensemble_member)
     buffer(3) = dble(N)
     buffer(4) = I_max

     call MPI_SEND(buffer,4,MPI_DOUBLE_PRECISION,worker_id,MPI_work_packet,MPI_COMM_WORLD,ierr)
     if (ierr .ne. 0) call quit('Could not send work packet.')


end subroutine send_work

subroutine send_cease(worker_id)
     ! Send a message to worker that there is no more work to be done.
     use conf
     implicit none
     include 'mpif.h'
     integer :: worker_id, ierr

     real(kind=db) :: buffer(4)

     buffer = -1

     call MPI_SEND(buffer,4,MPI_DOUBLE_PRECISION,worker_id,MPI_work_packet,MPI_COMM_WORLD,ierr)
     if (ierr .ne. 0) call quit('Failed to send cease packet')

end subroutine send_cease




