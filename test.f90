program init
     use conf
     call conf_init()
     call test()
end program init
subroutine test
     ! TODO: List bound states + initial thermal ensemble
     ! independently of thermal threshold
     use conf
     use potentials
     implicit none
     real(kind=db) :: vmat(num_N,num_N,0:JTOTMAX,NR)
     complex(kind=exp_k) :: expvmatHalf(num_N,num_N,0:JTOTMAX,NR), expvmat(num_N,num_N,0:JTOTMAX,NR)
     integer :: ir
     complex(kind=db) :: k(NR) = (/( ir*DK, ir=1,NR )/)
     !complex(kind=db) :: expkin(NR)
     integer :: order(9)
     real(kind=db) :: array(9);
     integer :: i, n, num_states, jt
     !complex(kind=db) :: a(NR,num_N,0:JTOTMAX,num_trial_states,0:JTOTMAX)
     !real(kind=db), dimension(num_trial_states,0:JTOTMAX) :: E
     !complex(kind=db) :: dtC
     real(kind=db) :: sixjsymbol

     array = (/5,1,4,2,3,10,7,6,9/)

     order = (/( n, n=1,9)/)

     call mergesort(9,9,order,array);

     do n = 1,9
          print *, array(order(n))
     enddo


     ! Todo: find thermal ensemble

     print *,'6j symbols'
     print *, sixjsymbol(14, 16, 16, 78, 62, 76)
     print *, sixjsymbol(14, 16, 16, 78, 62, 76)

     call countUmat(N)

end subroutine test

subroutine countUmat(N)
     ! Calculate the matrix representation of cos^2 theta
     ! in the coupled basis
     use conf
     implicit none

     integer, intent(in) :: N
     integer :: j1, jt1, j2, jt2, l, jmin
     integer(kind=ll) :: i1, i2
     integer :: m
     real(kind=db) :: THREE0, THREE
     integer :: delta
     real(kind=db) :: prefac, fac, j1j22000, sqrtsqrt
     integer(kind=ll) :: used,notused,misses
     
     used = 0
     notused = 0
     misses = 0

     do l = 0, LMAX, LSTEP
     do jt2 = 0, JTOTMAX, JTOTSTEP
     do j2 = 0, jMAX, jSTEP
     i2 = Uind(j2,jt2,l); if (i2<0) cycle
     do jt1 = 0, JTOTMAX, JTOTSTEP
     do j1 = 0, jMAX, jSTEP
          i1 = Uind(j1,jt1,l); if (i1 < 0) cycle
          if (abs(j1-j2) > 2 .or. (j1+j2 < 2 .and. j1+j2 .ne. 0)) then
               notused = notused + 1
               cycle
          endif
          
          !jmin = min(j1,j2)

          !prefac = sqrt(dble((2*jt1+1)*(2*jt2+1)))
          !j1j22000 = THREE0(j1,j2,2)
          !sqrtsqrt = sqrt(dble((2*j1+1)*(2*j2+1)))
     
          !fac = 0
          !do m = -jmin,jmin
          !     if (abs(N-m) .gt. l) cycle
          !     fac = fac + (2*(-1)**m*sqrtsqrt*j1j22000*three(j1,j2,2,m,-m,0) +&
          !     delta(j1,j2))*three(j1,l,jt1,m,N-m,-N)*three(j2,l,jt2,m,N-m,-N)
          !enddo
          !if (abs(prefac*fac/3) .lt. 1d-10) misses = misses + 1
          used = used+1
     enddo
     enddo
     enddo
     enddo
     enddo

     print *, misses,notused,used,dble(notused)/dble(used)
     print *, misses/dble(num_jJ**2),notused/dble(num_jJ**2),used/dble(num_jJ**2),dble(notused)/dble(used)

end subroutine countUmat







