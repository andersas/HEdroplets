// This file is from the GSL library. Added here to test new way of
// calculating six j symbols. The GSL library overflowed for semi large J.
// I have changed it so it works with the logarithm of factorials instead
// of the raw factorials.

/* specfunc/coupling.c
 * 
 * Copyright (C) 1996, 1997, 1998, 1999, 2000, 2001, 2002 Gerard Jungman
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/* Author:  G. Jungman */
/* Edited by Anders A. Søndergaard to calculate 6j symbols for high j */

//#include <config.h>
#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_coupling.h>
#include <gsl/gsl_sf_exp.h>

#include "error.h"

inline
static
int locMax3(const int a, const int b, const int c)
{
  int d = GSL_MAX(a, b);
  return GSL_MAX(d, c);
}

inline
static
int locMin3(const int a, const int b, const int c)
{
  int d = GSL_MIN(a, b);
  return GSL_MIN(d, c);
}

inline
static
int locMin5(const int a, const int b, const int c, const int d, const int e)
{
  int f = GSL_MIN(a, b);
  int g = GSL_MIN(c, d);
  int h = GSL_MIN(f, g);
  return GSL_MIN(e, h);
}


/* See: [Thompson, Atlas for Computing Mathematical Functions] */

static
int
lndelta(int ta, int tb, int tc, gsl_sf_result * d)
{
  gsl_sf_result f1, f2, f3, f4;
  int status = 0;
  status += gsl_sf_lnfact_e((ta + tb - tc)/2, &f1);
  status += gsl_sf_lnfact_e((ta + tc - tb)/2, &f2);
  status += gsl_sf_lnfact_e((tb + tc - ta)/2, &f3);
  status += gsl_sf_lnfact_e((ta + tb + tc)/2 + 1, &f4);
  if(status != 0) {
    OVERFLOW_ERROR(d);
  }
  d->val = f1.val + f2.val + f3.val - f4.val;
  d->err = 0;
  return GSL_SUCCESS;
}


static
int
triangle_selection_fails(int two_ja, int two_jb, int two_jc)
{
  /*
   * enough to check the triangle condition for one spin vs. the other two
   */
  return ( (two_jb < abs(two_ja - two_jc)) || (two_jb > two_ja + two_jc) ||
           GSL_IS_ODD(two_ja + two_jb + two_jc) );
}


int /* Don't bother with error codes. */
gsl_sf_coupling_6j_e_anders(int two_ja, int two_jb, int two_jc,
                     int two_jd, int two_je, int two_jf,
                     gsl_sf_result * result)
{
  /* CHECK_POINTER(result) */

  if(   two_ja < 0 || two_jb < 0 || two_jc < 0
     || two_jd < 0 || two_je < 0 || two_jf < 0
     ) {
    DOMAIN_ERROR(result);
  }
  else if(   triangle_selection_fails(two_ja, two_jb, two_jc)
          || triangle_selection_fails(two_ja, two_je, two_jf)
          || triangle_selection_fails(two_jb, two_jd, two_jf)
          || triangle_selection_fails(two_je, two_jd, two_jc)
     ) {
    result->val = 0.0;
    result->err = 0.0;
    return GSL_SUCCESS;
  }
  else {
    gsl_sf_result n1;
    gsl_sf_result d1, d2, d3, d4, d5, d6;
    double lnnorm;
    int tk, tkmin, tkmax;
    double phase;
    double sum_pos = 0.0;
    double sum_neg = 0.0;
    int status = 0;
    status += lndelta(two_ja, two_jb, two_jc, &d1);
    status += lndelta(two_ja, two_je, two_jf, &d2);
    status += lndelta(two_jb, two_jd, two_jf, &d3);
    status += lndelta(two_je, two_jd, two_jc, &d4);
    if(status != GSL_SUCCESS) {
      OVERFLOW_ERROR(result);
    }
    // Dividing by two inside exp() is the same as sqrt() outside
    lnnorm = (d1.val + d2.val + d3.val + d4.val)/2;
    
    tkmin = locMax3(0,
                   two_ja + two_jd - two_jc - two_jf,
                   two_jb + two_je - two_jc - two_jf);

    tkmax = locMin5(two_ja + two_jb + two_je + two_jd + 2,
                    two_ja + two_jb - two_jc,
                    two_je + two_jd - two_jc,
                    two_ja + two_je - two_jf,
                    two_jb + two_jd - two_jf);

    phase = GSL_IS_ODD((two_ja + two_jb + two_je + two_jd + tkmin)/2)
            ? -1.0
            :  1.0;

    for(tk=tkmin; tk<=tkmax; tk += 2) {
      double term;
      gsl_sf_result den_1, den_2;
      gsl_sf_result d1_a, d1_b;
      status = 0;

      status += gsl_sf_lnfact_e((two_ja + two_jb + two_je + two_jd - tk)/2 + 1, &n1);
      status += gsl_sf_lnfact_e(tk/2, &d1_a);
      status += gsl_sf_lnfact_e((two_jc + two_jf - two_ja - two_jd + tk)/2, &d1_b);
      status += gsl_sf_lnfact_e((two_jc + two_jf - two_jb - two_je + tk)/2, &d2);
      status += gsl_sf_lnfact_e((two_ja + two_jb - two_jc - tk)/2, &d3);
      status += gsl_sf_lnfact_e((two_je + two_jd - two_jc - tk)/2, &d4);
      status += gsl_sf_lnfact_e((two_ja + two_je - two_jf - tk)/2, &d5);
      status += gsl_sf_lnfact_e((two_jb + two_jd - two_jf - tk)/2, &d6);

      if(status != GSL_SUCCESS) {
        OVERFLOW_ERROR(result);
      }

      d1.val = d1_a.val + d1_b.val;
      den_1.val = d1.val+d2.val+d3.val;
      den_2.val = d4.val+d5.val+d6.val;

      term = n1.val - den_1.val - den_2.val;

      if(phase >= 0.0) {
        sum_pos += phase*exp(lnnorm+term);
      }
      else {
        sum_neg -= phase*exp(lnnorm+term);
      }
      phase = -phase;
    }

    result->val  = sum_pos - sum_neg;
    result->err = 0;

    return GSL_SUCCESS;
  }
}


