module thermal ! Needs a module because we need an explicit interface
               ! for passing allocatable arrays and allocating them
               ! inside the function. At least according to my fortran book.
               ! I have not tried without an explicit interface.
contains
subroutine thermal_ensemble(T,percentile,ensemble_size,a_out,jt_out,z_out,E_out,jt_num_states,energy_eigenstates,E)
     use conf
     implicit none
     integer, intent(out) :: ensemble_size
     real(kind=db), intent(in) :: T, percentile
     complex(kind=db), intent(out), allocatable :: a_out(:,:,:)
     real(kind=db), intent(out), allocatable :: z_out(:), E_out(:)
     integer, intent(out), allocatable :: jt_out(:)
     real(kind=db), dimension(num_trial_states,0:imag_JTOTMAX),intent(in) :: E

     complex(kind=db) :: energy_eigenstates(NR,num_N,num_trial_states,0:imag_JTOTMAX)
     real(kind=db), dimension(num_trial_states,0:imag_JTOTMAX) :: zs
     real(kind=db), dimension(size(zs)) :: zs_flat
     integer, dimension(size(zs)) :: order
     integer :: jt_num_states(0:imag_JTOTMAX)
     integer :: n, jt, jt_state_index
     real(kind=db) :: Z,Z_part, abundance

     if (T .eq. 0) then
          ensemble_size = 1
          allocate(E_out(ensemble_size), z_out(ensemble_size), jt_out(ensemble_size), a_out(NR,num_N,ensemble_size))
          z_out(1) = 1
          jt_out(1) = 0
          a_out(:,:,1) = energy_eigenstates(:,:,1,0)
          E_out(1) = E(1,0)
          return 
     endif

     ! Resolve a percentile of the partition function:
     ! First find the partition function:

     zs = 0 ! default: 0 statistical weight
     Z = 0;
     do jt = 0, imag_JTOTMAX, JTOTSTEP
          if (mod(jt,2) .eq. 0) then ! This trick works in gas phase.
               abundance = even_abundance ! With the helium attached
          else                            ! it is questinable at best,
               abundance = odd_abundance  ! since H does not commute with j.
          end if                          ! This is an approximation!
          do n = 1,jt_num_states(jt)
               zs(n,jt) = (2*jt+1)*exp(-E(n,jt)/T)*abundance
               Z = Z + zs(n,jt) 
          enddo
     enddo

     ! Sort the states after statistical weight
     zs_flat = reshape(zs,shape(zs_flat))
     order = (/(n, n=1,size(zs_flat) )/)
     
     call mergesort(size(zs_flat),size(zs_flat),order,zs_flat)
     ! order(n) now indexes zs_flat in the sorted order
     
     ! Find number of states to resolve the percentile of the partition fcn:
     Z_part = 0
     do n = 1,size(zs_flat)
          Z_part = Z_part + zs_flat(order(n))
          if (Z_part/Z .gt. percentile) exit
     enddo
     ensemble_size = n

     ! Allocate the output arrays, and copy the most 99.9% most significant
     ! states into the allocated output arrays.
     allocate(E_out(ensemble_size), z_out(ensemble_size), jt_out(ensemble_size), a_out(NR,num_N,ensemble_size))

     do n=1,ensemble_size
          ! order(n) = jt*num_states + jt_state_index (see reshape)
          ! Note: integer division (no fractional part)
          jt = (order(n)-1)/num_trial_states
          jt_state_index = mod(order(n)-1,num_trial_states) + 1
          jt_out(n) = jt ! Jtot of state
          z_out(n) = zs_flat(order(n)) / Z ! probability of this state
          a_out(:,:,n) = energy_eigenstates(:,:,jt_state_index,jt) ! state
          E_out(n) = E(jt_state_index,jt) 
     enddo


end subroutine thermal_ensemble

end module thermal


subroutine energy_spectrum(jt_num_states,energy_eigenstates,E,expvmat,expkin,expkinHalf)
     use conf
     
     integer, intent(out) :: jt_num_states(0:imag_JTOTMAX)
     complex(kind=db), intent(out) :: energy_eigenstates(NR,num_N,num_trial_states,0:imag_JTOTMAX)
     real(kind=db), dimension(num_trial_states,0:imag_JTOTMAX), intent(out) :: E
     real(kind=db), intent(in) :: expvmat(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)
     complex(kind=db), intent(in) :: expkin(NR), expkinHalf(NR)
     
     integer :: jt

     ! Find all the relevant states in the partition function
     !$OMP PARALLEL DO SCHEDULE(GUIDED,1) DEFAULT(SHARED) PRIVATE(jt)
     do jt = 0,imag_JTOTMAX,JTOTSTEP
          jt_num_states(jt) = num_trial_states
          call J_spectrum(jt_num_states(jt),jt,energy_eigenstates(:,:,:,jt),E(:,jt),expvmat,expkin,expkinHalf)
          print *, 'Found first ',jt_num_states(jt),' stationary state(s) belonging to jt=',jt
     enddo
     !$OMP END PARALLEL DO

end subroutine energy_spectrum

subroutine J_spectrum(num_states,jt,a,E,expvmat,expkin,expkinHalf)
     ! Find the lowest num_states energy eigenstates for J = jt.
     ! (except the 2jt+1 degeneracy)
     ! If states of higher energy than some threshold are found, exit and set
     ! num_states to the number of negative energy states found.
     use conf
     implicit none
     
     integer, intent(in) :: jt
     integer, intent(inout) :: num_states
     complex(kind=db), intent(out) :: a(NR,num_N,num_states)
     real(kind=db), intent(out) :: E(num_states)
     real(kind=db), intent(in) :: expvmat(imag_num_N,imag_num_N,0:imag_JTOTMAX,NR)
     complex(kind=db), intent(in) :: expkin(NR), expkinHalf(NR)
     real(kind=db) :: s, norm_subspace
     integer :: n,m,i, ir, it
     real(kind=db) :: t
     real(kind=db), parameter :: r(NR) = (/( ir*DR, ir=1,NR )/)

     a = cmplx(0_db,0_db,kind=db)

     ! Initial guess for wave functions. Must be 0 at 0 and 0 at inf.
     ! Note: no jt dependence in state vector, because jt commutes
     ! with the Hamiltonian outside the laser pulse.
     ! This saves a lot of space.
     if (He .eq. 1) then
     forall (n=1:num_states,i=1:indOrder(jt))    &
          a(1:NR,i,n) = (/( cmplx( r(ir)*exp(-r(ir)/4d0)*(NR*DR-r(ir)),0d0,kind=db) , ir=1,NR )/)
     else
          a = 1
     endif
     ! Normalize:
     do n=1,num_states
          a(:,:,n) = a(:,:,n)/norm_subspace(a(:,:,n),jt)
     enddo
     
     ! Imaginary time propagation
     !open(77,file='imaginary_time_propagation.log',status='UNKNOWN',position='APPEND')
     do n = 1, num_states
          do it=0,NT1,NTblock_imag
               t = it*DT_imag ! Propagate NTblock_imag timesteps:
               call imag_timeblock_singleJ(jt,NTblock_imag,a(:,:,n),expvmat,expkin,expkinHalf)
               do m=1,n-1 ! Project out previously found states
                    call project_out_subspace(a(:,:,n),a(:,:,m),jt)
               enddo

               ! Find the energy only after projecting out previous states
               s = norm_subspace(a(:,:,n),jt)
               a(:,:,n) = a(:,:,n)/s

               E(n) = -log(s)/(DT_imag*NTblock_imag)

      !         write(77,*) jt, it, n, t, E(n)
          enddo
          if (E(n) .GT. thermal_energy_threshold) then
               num_states = n - 1
               exit
          endif
     enddo
     !close(77)
     
end subroutine J_spectrum




