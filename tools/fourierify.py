#!/usr/bin/python3

import numpy 
import numpy.fft
import matplotlib
import matplotlib.pyplot as plt
import scipy.constants

import argparse

parser = argparse.ArgumentParser('Fourier spectrum of alignment trace');

parser.add_argument('in',type=str,help='Alignment trace file')
parser.add_argument('out',type=str,help='Output figure file')

args = parser.parse_args();

trace = numpy.genfromtxt(getattr(args,"in"));

times = trace[150*3:,0];
cos2 = trace[150*3:,1];
cos2 = cos2 - numpy.mean(cos2)

power = abs(numpy.fft.fft(cos2))**2;
freq = numpy.fft.fftfreq(len(cos2),times[-1]-times[-2])

positive = freq>=0;

power = power[positive];
freq = freq[positive];

wavelength_meters = scipy.constants.speed_of_light/(freq*1e12)
wavenumber = 1/(wavelength_meters*100); # how many wavelengths in a cm


fig = plt.figure();
plt.plot(wavenumber,power);
plt.xlim([0,30])
plt.xlabel('Frequency [cm^-1]')
plt.ylabel('Intensity [arb. u.]')
fig = plt.figure();
plt.plot(times,cos2);
plt.show();


