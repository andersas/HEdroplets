#!/usr/bin/python3
import numpy

LMAX=32;
LSTEP=1;
jMAX=160;
jStep=2;
JTOTMAX=160;
JTOTSTEP=1;



ind = numpy.empty((LMAX+1,jMAX+1,JTOTMAX+1),dtype=numpy.int64,order='F');
Uind = numpy.empty((jMAX+1,JTOTMAX+1,LMAX+1),dtype=numpy.int64,order='F');
indOrder = numpy.zeros((JTOTMAX+1,),dtype=numpy.int64,order='F');
UindOrder = numpy.zeros((LMAX+1,),dtype=numpy.int64,order='F');


ind[:] = -1
Uind[:] = -1

for jtot in range(0,JTOTMAX+1,JTOTSTEP):
    i = 0
    for j in range(0,jMAX+1,jStep):
        for l in range(0,LMAX+1,LSTEP):
            if (abs(l-j)>jtot or l+j<jtot):
                continue;
            ind[l][j][jtot] = i;
            i = i + 1;
    indOrder[jtot] = i

for l in range(0,LMAX+1,LSTEP):
    i = 0
    for jtot in range(0,JTOTMAX+1,JTOTSTEP):
        for j in range(0,jMAX,jStep):
            if (abs(j-l) > jtot or j+l < jtot):
                continue;
            Uind[j,jtot,l] = i
            i = i + 1;
    UindOrder[l] = i


