#!/usr/bin/python3

import numpy 
import matplotlib.pyplot as plt

import argparse


def normalise(trace):
    res = trace-numpy.mean(trace,axis=0);
    res = res/numpy.max(res,axis=0);
    return res;




parser = argparse.ArgumentParser('Calculate cross correlation coefficients of two ensemble files');

parser.add_argument('f1',type=str,help='Ensemble file 1 (npy)')
parser.add_argument('f2',type=str,help='Ensemble file 2 (npy)')

args = parser.parse_args();

part1 = numpy.load(args.f1);
part2 = numpy.load(args.f2);

t1 = part1[:,0];
t2 = part2[:,0];


plt.plot(part1[455:,2]);
plt.figure();

plt.plot(t1[455:],normalise(part1[455:,1]));
plt.plot(t2[455:],normalise(part2[455:,1]));

print(numpy.corrcoef(normalise(part1[455:,1]),normalise(part2[455:,1])));

plt.show();






