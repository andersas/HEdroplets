#!/usr/bin/python3

import numpy

base = "/unsafe/tmp/single/"

vmat = numpy.memmap(base+"vmat.dat",dtype=numpy.single,mode='r')
vmat_diag = numpy.memmap(base+"vmat_diag.dat",dtype=numpy.double,mode='r')

nexti = numpy.memmap(base + "nexti.dat",dtype=numpy.int64,mode='r')
i = 0
while (nexti[i] != 0):
    i = i + 1
rotmat = numpy.memmap(base+"rotmat.dat",shape=(i,),dtype=numpy.single,mode='r')



print (numpy.max(rotmat)*numpy.nanmax(vmat[numpy.isinf(vmat) is False])*numpy.max(vmat_diag));





