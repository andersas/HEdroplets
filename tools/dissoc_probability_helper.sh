#!/bin/sh

# run this script with seq -f "%e" 1e10 1e10 1e11 | parallel -P N script  
# replace the intensity with INTENSITY_TEMPLATE and
# rename conf.f90 to conf.f90.template


mkdir -p dissoc_run/$1

(
flock -x  9  || exit 1

sed 's/INTENSITY_TEMPLATE/'$1'*1e4/' < conf.f90.template > conf.f90 
make || exit 2
cp conf.f90 coupchan dissoc_run/$1
) 9>/var/lock/dissoc_prob.lock

cd dissoc_run/$1 && mpirun -n 2 ./coupchan


