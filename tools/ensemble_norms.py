#!/usr/bin/python3

import numpy 
import os
import sys
import argparse

parser = argparse.ArgumentParser('Dump |psi|^2 as function of t of all ensemble members');

parser.add_argument('in',type=str,help='ensemble folder')

args = parser.parse_args();

directory = getattr(args,"in");
members = os.listdir(directory);

traces = [];
intensities = [];
for member in members:
    intensities.append(float(member.split(' ')[-1]));
    trace = numpy.genfromtxt(directory + '/' + member+'/norm.dat');
    t = trace[:,0];
    psi_sq = numpy.abs(trace[:,1])**2;
    traces.append((t,psi_sq));

numpy.savez('norms.npz',intensities=intensities,traces=traces);


