#!/usr/bin/python3

import numpy 
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation

import argparse

parser = argparse.ArgumentParser('hej');

parser.add_argument('in',type=str,help='file to convert')
parser.add_argument('out',type=str,help='movie file')

args = parser.parse_args();

scatter = numpy.genfromtxt(getattr(args,"in"));

times = numpy.unique(scatter[:,0]);

fig = plt.figure();
handle = plt.plot([],[]);

FFMpegWriter = manimation.writers['ffmpeg'];
metadata = dict(title='Animation', artist='Anders Søndergaard', comment='Simulation animation')
writer = FFMpegWriter(fps=3*15, codec="mpeg4",bitrate=64000, metadata=metadata)

dpi=100

plt.xlim(0,20);
plt.ylim(0,0.1);
#plt.xlabel('r [Ångstrøm]');
plt.xlabel('J');
plt.ylabel('Sandsynlighedstæthed [forkert enhed]')


annotation = plt.text(15,0.090,'')

with writer.saving(fig,args.out,dpi):
    for t in times:
        data = scatter[numpy.where(scatter[:,0]==t)];
        index = data[:,1];
        value = data[:,2];
        handle[0].set_data(index,value);
        annotation.set_text('t = {:0< 5.02f} ps'.format(float(int(t*100))/100.0));
        writer.grab_frame();
        print(t/times[-1]);

    
    



