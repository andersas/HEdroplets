#!/usr/bin/python3

import numpy 

import argparse

parser = argparse.ArgumentParser('Convert ensemble file to npy array');

parser.add_argument('in',type=str,help='Ensemble file')
parser.add_argument('out',type=str,help='Output npy file')

args = parser.parse_args();

ensemble = numpy.genfromtxt(getattr(args,"in"));

t = numpy.unique(ensemble[:,0]);

num_t = len(t);
population_levels = numpy.reshape(ensemble[:,2],(num_t,-1));

numpy.save(getattr(args,"out"),numpy.concatenate((numpy.reshape(t,(len(t),1)),population_levels),axis=1));


