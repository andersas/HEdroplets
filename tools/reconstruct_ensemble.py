#!/usr/bin/python3

import numpy 
import os
import sys
import argparse

parser = argparse.ArgumentParser('Reconstruct cos^2 theta trace from incomplete ensemble');

parser.add_argument('in',type=str,help='ensemble folder')

args = parser.parse_args();

directory = getattr(args,"in");
members = os.listdir(directory);

traces = [];
weightsum = 0;
for member in members:
    weight = float(member.split(' ')[0])
    weightsum += weight;
    trace = numpy.genfromtxt(directory + '/' + member+'/norm.dat');
    t = trace[:,0];
    cos2 = weight*trace[:,2];
    traces.append(cos2);

if (abs(weightsum-1)>0.001):
    print('Weightsum ', weightsum, 'not 1.',file=sys.stderr);

num_points = min([len(cos2) for cos2 in traces]);
cos2 = numpy.zeros(num_points);
for trace in traces:
    cos2 += trace[0:num_points];

table = numpy.vstack((t[0:num_points],cos2)).T;

numpy.savetxt(sys.stdout.buffer,table);



