! shm is for SHared Memory.
! The idea is to memory map a named, shared file. The memory mapped
! pages will be kept in the operating system filesystem cache,
! and data does not actually need to hit the harddrive ever,
! unless there is a high memory pressure.
!
! We make use of the POSIX mmap call for this purpose.
! It is most easily called from a C function (see shm_backend.c)
!
! To avoid two processes trying to diagonalize the same matrix,
! we need a mutex. This is implemented by using POSIX file locking
! via the flock system call.
! In this way, the shared arrays are only locked on a single node
! in the bigger MPI cluster. I.e. exactly one MPI process on each host
! will diagonalize the potentials.
!
! Also, since the flock syscall is blocking, the waiting processes will
! yield to the parallel calculation of the exponential matrices,
! regardless of the MPI implementation
module shm
     use conf
     
     !integer, parameter :: integer_size = STORAGE_SIZE(42)/8
     !integer, parameter :: double_size = STORAGE_SIZE(1.0_db)/8
     !integer, parameter :: double_complex_size = STORAGE_SIZE(cmplx(0d0,0d0,kind=db))/8

     interface
          function shm_get_handle(num_bytes) bind(c)
               use iso_c_binding
               type(c_ptr) :: shm_get_handle
               integer(kind=C_SIZE_T), value :: num_bytes
           end function shm_get_handle
     end interface

     interface
          ! Derives the pointer to the shared memory area
          ! from the handle
          function shm_get_pointer(shm_handle) bind(c)
               use iso_c_binding
               type(c_ptr), value :: shm_handle
               type(c_ptr) :: shm_get_pointer
           end function shm_get_pointer
     end interface

     interface
          function shm_allocate_raw(filename,shm_handle) bind(c,name='shm_allocate')
               use iso_c_binding
               type(c_ptr) :: shm_allocate_raw
               character(kind=C_CHAR) :: filename(*)
               type(c_ptr), value :: shm_handle
           end function shm_allocate_raw
     end interface

     interface ! This deallocates the shared memory, and also the handle(!)
          subroutine shm_deallocate(shm_handle) bind(c)
               use iso_c_binding
               type(c_ptr), value :: shm_handle
          end subroutine shm_deallocate
     end interface

     interface
          function shm_lock_shared(shm_handle) bind(c)
               use iso_c_binding
               integer(kind=C_INT) :: shm_lock_shared
               type(c_ptr), value :: shm_handle
          end function shm_lock_shared
     end interface

     interface
          function shm_lock_exclusive(shm_handle) bind(c)
               use iso_c_binding
               integer(kind=C_INT) :: shm_lock_exclusive
               type(c_ptr), value :: shm_handle
          end function shm_lock_exclusive
     end interface

     interface
          function shm_unlock(shm_handle) bind(c)
               use iso_c_binding
               integer(kind=C_INT) :: shm_unlock
               type(c_ptr), value :: shm_handle
          end function shm_unlock
     end interface

     interface
          function unlink_raw(filename) bind(c,name='unlink')
               use iso_c_binding
               integer(kind=C_INT) :: unlink_raw
               character(kind=C_CHAR) :: filename(*)
           end function unlink_raw
     end interface


     !type shm_handle
     !     use iso_c_binding
     !     type(c_ptr) :: handle
     !end type shm_handle

contains 

     ! allocate_shared and deallocate_shared are just here
     ! to mimic the intrinsic allocate and deallocate
     ! as much as possible.
     subroutine allocate_shared(filename,num_bytes,shm_handle,shm_pointer)
          use conf
          use iso_c_binding
          implicit none

          character(len=*), intent(in) :: filename
          integer(kind=C_SIZE_T), intent(in) :: num_bytes
          type(c_ptr), intent(out) :: shm_handle, shm_pointer

          shm_handle = shm_get_handle(num_bytes)
          shm_pointer = shm_allocate(filename,shm_handle)

          if (.not. C_ASSOCIATED(shm_pointer)) call quit('Failed to allocate shm array ' // filename);

     end subroutine allocate_shared

     subroutine deallocate_shared(shm_handle)
          use iso_c_binding
          implicit none
          type(c_ptr) :: shm_handle
          
          call shm_deallocate(shm_handle)

     end subroutine deallocate_shared


     !! Wrapper around shm_allocate that converts the filename from 
     !! a fortran string to a C string:
     function shm_allocate(filename,shm_handle)
         use iso_c_binding
         implicit none
         type(c_ptr) :: shm_allocate
         character(len=*) :: filename
         character(len=len_trim(filename)+1,kind=c_char) :: c_filename
         type(c_ptr) :: shm_handle

         c_filename = trim(filename)//C_NULL_CHAR

         shm_allocate = shm_allocate_raw(c_filename,shm_handle)

     end function shm_allocate
    

     ! Similarily with unlink:
     function unlink(filename)
         use iso_c_binding
         implicit none
         integer :: unlink
         character(len=*) :: filename
         character(len=len_trim(filename)+1,kind=c_char) :: c_filename

         c_filename = trim(filename)//C_NULL_CHAR

         unlink = unlink_raw(c_filename)

     end function unlink

end module shm



